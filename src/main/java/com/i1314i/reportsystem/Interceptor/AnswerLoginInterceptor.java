package com.i1314i.reportsystem.Interceptor;

import org.springframework.lang.Nullable;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author 平行时空
 * @created 2018-06-13 15:49
 **/
public class AnswerLoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String userurl=request.getRequestURI();
        if(userurl.indexOf("answerlogin")>0){
            return true;
        }
        HttpSession session=request.getSession();
        String userId= (String) session.getAttribute("userId");
        if(userId!=null){
            return true;
        }else{
            String path=request.getServletContext().getContextPath();
            response.sendRedirect(path+"/answerlogin");
        }
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {

    }
}
