package com.i1314i.reportsystem.Interceptor;


import com.i1314i.reportsystem.po.Problem;
import com.i1314i.reportsystem.po.Studentlogin;
import com.i1314i.reportsystem.service.ProblemService;
import com.i1314i.reportsystem.service.impl.ProblemServiceImpl;
import com.i1314i.reportsystem.utils.TemplateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author 平行时空
 * @created 2018-04-28 16:12
 **/

/**
 * 拦截器例子
 */

public class LoginInterceptor implements HandlerInterceptor{
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String userurl=request.getRequestURI();
        if(userurl.indexOf("/login")>0){
            return true;
        }

        if(userurl.indexOf("student/info")>0||userurl.indexOf("student/test")>0){

            Object examStatus= request.getSession().getAttribute("exam");
            if(examStatus!=null){
                String path=request.getServletContext().getContextPath();
                if(TemplateUtils.isWeChat(request)){
                    response.sendRedirect(path+"/student/questions");
                }else {
                    if(TemplateUtils.isMoblie(request)){
                        response.sendRedirect(path+"/student/questions");
                    }else {
                        response.sendRedirect(path+"/student/problem");
                    }

                }
                return false;
            }
        }

        HttpSession session=request.getSession();
        String userId= (String) session.getAttribute("userId");
        if(userId!=null){
            return true;

        }else{
            String path=request.getServletContext().getContextPath();
            response.sendRedirect(path+"/login");
        }
        return false;
    }


    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
