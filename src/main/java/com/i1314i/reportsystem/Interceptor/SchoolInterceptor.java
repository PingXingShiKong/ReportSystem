package com.i1314i.reportsystem.Interceptor;

import com.i1314i.reportsystem.po.Adminlogin;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 平行时空
 * @created 2018-06-18 13:59
 **/
public class SchoolInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String userurl=request.getRequestURI();

            Adminlogin adminlogin= (Adminlogin) request.getSession().getAttribute("admindata");
            if(adminlogin==null){
                String path=request.getServletContext().getContextPath();
                response.sendRedirect(path+"/login");
                return false;
            }else if(adminlogin.getType()==1){
                return true;
            }

        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {

    }
}
