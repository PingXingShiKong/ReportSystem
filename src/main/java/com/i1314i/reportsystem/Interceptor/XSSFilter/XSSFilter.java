package com.i1314i.reportsystem.Interceptor.XSSFilter;

import com.i1314i.reportsystem.utils.XSSUtil.XssHttpServletRequestWrapper;
import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author 平行时空
 * 防止sxx注入
 * @created 2018-07-06 13:57
 **/
//@WebFilter(filterName = "XSSFilter", urlPatterns = "/*")
@Order(value = 1)
public class XSSFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        System.out.println("xxs检测");
        XssHttpServletRequestWrapper xssRequest = new XssHttpServletRequestWrapper(
                (HttpServletRequest) request);
        chain.doFilter(xssRequest, response);
    }
    public void init(FilterConfig config) throws ServletException {

    }

}
