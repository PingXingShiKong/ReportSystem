package com.i1314i.reportsystem.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * @author 平行时空
 * @created 2018-08-10 15:02
 **/

@Configuration
@EnableRedisHttpSession
public class RedisSessionConfig {
}
