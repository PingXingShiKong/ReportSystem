package com.i1314i.reportsystem.config;

import com.i1314i.reportsystem.Interceptor.*;
import com.i1314i.reportsystem.Interceptor.XSSFilter.XSSFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.support.ErrorPageFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author 平行时空
 * @created 2018-04-28 16:11
 **/
@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor()).addPathPatterns("/student/**");
        registry.addInterceptor(new AnswerLoginInterceptor()).addPathPatterns("/answer/**");
        registry.addInterceptor(new CollegeInterceptor()).addPathPatterns("/xueyuan/**");
        registry.addInterceptor(new ColAndSchInterceptor()).addPathPatterns("/colandsch/**");
        registry.addInterceptor(new SchoolInterceptor()).addPathPatterns("/school/**"); //对来自/** 这个链接来的请求进行拦截
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        ///registry.addViewController("/").setViewName("forward:/login");
//        registry.addViewController("stu/login").setViewName("forward:/stu/login.html");
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
        super.addViewControllers(registry);
    }



    //    @Bean
    public FilterRegistrationBean testFilterRegistration() {
        //配置无需过滤的路径或者静态资源，如：css，imgage等
        StringBuffer excludedUriStr = new StringBuffer();
//        excludedUriStr.append("/login/*");
//        excludedUriStr.append(",");
        excludedUriStr.append("/favicon.ico");
        excludedUriStr.append("/css/*");
        excludedUriStr.append("/images/*");
        excludedUriStr.append("/js/*");
        excludedUriStr.append("/layui/*");
        excludedUriStr.append("/layui2/*");
        excludedUriStr.append("/xy/*");
        excludedUriStr.append("/stu/*");
        excludedUriStr.append("/json/*");

        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new XSSFilter());
        registration.addUrlPatterns("/*");
        registration.addInitParameter("excludedUri", excludedUriStr.toString());
        registration.setName("XSSFilter");
        registration.setOrder(1);
        return registration;
    }

    /**
     * 优化tomcat 图片流加载未完成客户端关闭的io异常
     * @return
     */

//    @Bean
    public ErrorPageFilter errorPageFilter() {
        return new ErrorPageFilter();
    }
//    @Bean
    public FilterRegistrationBean disableSpringBootErrorFilter(ErrorPageFilter filter) {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(filter);
        filterRegistrationBean.setEnabled(false);
        return filterRegistrationBean;
    }
}
