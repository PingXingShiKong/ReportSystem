package com.i1314i.reportsystem.controller;

import com.i1314i.reportsystem.mapper.BasicinfoMapper;
import com.i1314i.reportsystem.po.JsonResult;
import com.i1314i.reportsystem.po.SchoolRate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/weixin")
public class AdminSideController {
    @Autowired
    BasicinfoMapper basicinfoMapper;

    @RequestMapping("/schoolRate")
    public @ResponseBody JsonResult schoolRate(){
        JsonResult jsonResult = JsonResult.createJsonResult();
        int schoolNOReport = basicinfoMapper.schoolNOReport();
        int schoolISReport = basicinfoMapper.schoolISReport();
        int schoolSum = schoolISReport + schoolNOReport;
        double schoolRate = ((double)schoolISReport) / ((double)schoolSum) ;
        schoolRate = (double)Math.round(schoolRate * 1000)/1000;

        int northNOReport = basicinfoMapper.northlNOReport();
        int northISReport = basicinfoMapper.northISReport();
        int northSum = northNOReport + northISReport;
        double northRate = ((double)northISReport) / ((double)northSum) ;
        northRate = (double)Math.round(northRate * 1000)/1000;

        int eastNOReport = basicinfoMapper.eastNOReport();
        int eastISReport = basicinfoMapper.eastISReport();
        int eastSum = eastNOReport + eastISReport;
        double eastRate = ((double)eastISReport) / ((double)eastSum) ;
        eastRate = (double)Math.round(eastRate * 1000)/1000;

        int southNOReport = basicinfoMapper.southNOReport();
        int southISReport = basicinfoMapper.southISReport();
        int southSum = southNOReport + southISReport;
        double southRate = ((double)southISReport) / ((double)southSum) ;
        southRate = (double)Math.round(southRate * 1000)/1000;

        SchoolRate schoolRate1 = new SchoolRate(schoolISReport, schoolNOReport, schoolSum, schoolRate, northISReport, northNOReport, northSum, northRate, eastISReport, eastNOReport, eastSum, eastRate, southISReport, southNOReport, southSum, southRate);



        jsonResult.setSuccess(1);
        jsonResult.setMsg("统计成功");
        jsonResult.setData(schoolRate1);
        return jsonResult;
    }

    @RequestMapping("/allRate")
    public @ResponseBody JsonResult allRate(){
        JsonResult jsonResult = JsonResult.createJsonResult();
        int schoolNOReport = basicinfoMapper.schoolNOReport();
        int schoolISReport = basicinfoMapper.schoolISReport();
        int schoolSum = schoolISReport + schoolNOReport;
        double schoolRate = ((double)schoolISReport) / ((double)schoolSum) ;
        schoolRate = (double)Math.round(schoolRate * 1000)/1000;

        SchoolRate schoolRate1 = new SchoolRate();
        schoolRate1.setSchoolISReport(schoolISReport);
        schoolRate1.setSchoolNOReport(schoolNOReport);
        schoolRate1.setSchoolSum(schoolSum);
        schoolRate1.setSchoolRate(schoolRate);

        jsonResult.setSuccess(1);
        jsonResult.setMsg("统计成功");
        jsonResult.setData(schoolRate1);
        schoolRate1=null;
        return jsonResult;

    }

}
