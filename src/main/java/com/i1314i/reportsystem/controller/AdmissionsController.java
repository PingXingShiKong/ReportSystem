package com.i1314i.reportsystem.controller;


import com.i1314i.reportsystem.po.Basicinfo;
import com.i1314i.reportsystem.service.CollegeSideService;
import com.i1314i.reportsystem.service.exception.StudentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/weixin/admissions")
public class AdmissionsController {


    @Autowired
    CollegeSideService collegeSideService;

    @RequestMapping("/freshman")
    public String checkFreshman(HttpServletRequest request, ModelMap map){
        String userId = request.getParameter("userId");
        String responseMsg = "";
        if (request.getSession() == null){
            responseMsg = "获取你的信息是失败";
            map.put("responseMsg",responseMsg);
            return "weixin/fail";
        }
        String fromUserName = (String)request.getSession().getAttribute("userId");

        try {
            Basicinfo basicinfo = collegeSideService.checkFreshman(userId,fromUserName,false);
            map.put("basicinfo",basicinfo);
            return "weixin/freshmanInfo";
        }catch (StudentException e){
            responseMsg = e.getMessage();
            map.put("responseMsg",responseMsg);
        }
        return "weixin/fail";
    }


    @RequestMapping("/remark")
    public String remark (HttpServletRequest request, ModelMap map){
        String userId = request.getParameter("userId");
        String remarkText = request.getParameter("remark");
        Boolean noSpotRegister = false;
        String responseMsg = "";
        String fromUserName = "";
        if (request.getSession() == null){
            responseMsg = "获取你的信息是失败";
            map.put("resposeMsg",responseMsg);
            return "weixin/fail";
        }
        fromUserName = (String)request.getSession().getAttribute("userId");

        try {
            noSpotRegister = collegeSideService.noSpotRegister(userId,fromUserName,remarkText,false);
        } catch (StudentException e) {
            responseMsg = e.getMessage();
            map.put("responseMsg",responseMsg);
            return "weixin/fail";
        }
        if (noSpotRegister){
            responseMsg = "备注备注成功";
            map.put("responseMsg",responseMsg);
            return "weixin/success";
        }
        responseMsg = "坏了";
        map.put("responseMsg",responseMsg);
        return "weixin/fail";
    }

}
