package com.i1314i.reportsystem.controller;

import com.i1314i.reportsystem.po.JsonResult;
import com.i1314i.reportsystem.po.Problem;
import com.i1314i.reportsystem.service.ProblemService;
import com.i1314i.reportsystem.service.exception.ProblemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * @author 平行时空
 * @created 2018-06-13 15:52
 **/
@RestController
@RequestMapping(value = "/answer")
public class AnswerController {
    @Autowired
    private ProblemService problemService;

    @RequestMapping(value = "/getanswer",method = {RequestMethod.POST},produces="application/json;charset=utf-8;")
    public JsonResult getAnswers(HttpServletRequest request) throws IOException {
        JsonResult jsonResult=JsonResult.createJsonResult();
        String userId= (String) request.getSession().getAttribute("userId");

        try {
            List<Problem> problems=problemService. wechatGetAnswers(userId);
            jsonResult.setData(problems);
            problems=null;
            jsonResult.setSuccess(1);
            jsonResult.setMsg("获取试题答案成功");
        }catch (ProblemException e) {
            jsonResult.setMsg(e.getMessage());
            jsonResult.setSuccess(0);
        }
        return jsonResult;
    }
}
