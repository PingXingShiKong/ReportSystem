package com.i1314i.reportsystem.controller;


import com.i1314i.reportsystem.po.Basicinfo;
import com.i1314i.reportsystem.service.CollegeSideService;
import com.i1314i.reportsystem.service.ScanQRCodeService;
import com.i1314i.reportsystem.service.exception.StudentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
@RequestMapping("/weixin/college")
public class CollegeSideController {

    @Autowired
    ScanQRCodeService scanQRCodeService;
    @RequestMapping("/manualReports")
    public String manualReport(HttpServletRequest request, ModelMap map) throws IOException {
        String userId = request.getParameter("userId");
        String responseMsg = "";
        if (request.getSession().getAttribute("userId") != null){

            String fromUserName = (String)request.getSession().getAttribute("userId");
            String success = "报到成功，并给"+userId+"发送相关消息消息";
            responseMsg = scanQRCodeService.responseMsg(userId,"shoudng",fromUserName);
            if (success.trim().equalsIgnoreCase(responseMsg)){
                map.put("responseMsg",responseMsg);
                return "/weixin/success";
            }else{
                map.put("responseMsg",responseMsg);
                return "/weixin/fail";
            }
        }else{
            System.out.println("userId is null");
            responseMsg = "获取你的信息是失败";
        }
        map.put("responseMsg",responseMsg);
        return "weixin/fail";
    }


    @Autowired
    CollegeSideService collegeSideService;

    @RequestMapping("/remark")
    public String remark (HttpServletRequest request, ModelMap map){
        String userId = request.getParameter("userId");
        String remarkText = request.getParameter("remark");
        Boolean noSpotRegister = false;
        String responseMsg = "";
        String fromUserName = "";
        if (request.getSession() == null){
            responseMsg = "获取你的信息是失败";
            map.put("resposeMsg",responseMsg);
            return "weixin/fail";
        }
        fromUserName = (String)request.getSession().getAttribute("userId");

        try {
            noSpotRegister = collegeSideService.noSpotRegister(userId,fromUserName,remarkText,true);
        } catch (StudentException e) {
            responseMsg = e.getMessage();
            map.put("responseMsg",responseMsg);
            return "weixin/fail";
        }
        if (noSpotRegister){
            responseMsg = "备注备注成功";
            map.put("responseMsg",responseMsg);
            return "weixin/success";
        }
        responseMsg = "坏了";
        map.put("responseMsg",responseMsg);
        return "weixin/fail";
    }


    @RequestMapping("/freshman")
    public String checkFreshman(HttpServletRequest request,ModelMap map){
        String userId = request.getParameter("userId");
        String responseMsg = "";
        if (request.getSession() == null){
            responseMsg = "获取你的信息是失败";
            map.put("responseMsg",responseMsg);
            return "weixin/fail";
        }
        String fromUserName = (String)request.getSession().getAttribute("userId");

        try {
            Basicinfo basicinfo = collegeSideService.checkFreshman(userId,fromUserName,true);
            map.put("basicinfo",basicinfo);
            return "weixin/freshmanInfo";
        }catch (StudentException e){
            responseMsg = e.getMessage();
            map.put("responseMsg",responseMsg);
        }
        return "weixin/fail";
    }



}
