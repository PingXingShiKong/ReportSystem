package com.i1314i.reportsystem.controller;



import com.i1314i.reportsystem.mapper.ScoreMapper;
import com.i1314i.reportsystem.po.Adminlogin;
import com.i1314i.reportsystem.po.Problem;
import com.i1314i.reportsystem.po.ReportsRate;
import com.i1314i.reportsystem.po.Score;
import com.i1314i.reportsystem.service.CollegeSideService;
import com.i1314i.reportsystem.service.IdentityService;
import com.i1314i.reportsystem.service.StudentSideService;
import com.i1314i.reportsystem.service.admin.GetCollegeReportsRate;
import com.i1314i.reportsystem.service.exception.AdminException;
import com.i1314i.reportsystem.service.exception.StudentException;
import com.i1314i.reportsystem.utils.*;
import com.i1314i.reportsystem.utils.Jedis.IJedisClient;
import com.i1314i.reportsystem.utils.Jedis.JedisClusterClient;
import com.i1314i.reportsystem.utils.weixin.ParamesAPI.util.ParamesAPI;
import com.i1314i.reportsystem.utils.weixin.ParamesAPI.util.WeixinUtil;
import com.i1314i.reportsystem.utils.weixin.contacts.util.MPerson;
import com.i1314i.reportsystem.utils.weixin.oauth2.util.GOauth2Core;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import redis.clients.jedis.Jedis;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/weixin")
public class GetUserIdController {
    private static Logger logger = Logger.getLogger(GetUserIdController.class);


    @Autowired
    ScoreMapper scoreMapper;
    @Autowired
    CollegeSideService collegeSideService;
    @Autowired
    IdentityService identityService;
    @Autowired
    GetCollegeReportsRate getCollegeReportsRate;

    @Autowired
    private IJedisClient jedisClient;

    @Autowired
    StudentSideService studentSideService;



    @RequestMapping("/getUserId")
    public String getUserId(HttpServletRequest request, ModelMap map, HttpServletResponse response) throws IOException, AdminException {
        System.out.println("getUserId");
        HttpSession session = request.getSession();
        String code = request.getParameter("code");
        String stateString[]=request.getParameter("state").split("_");
        System.out.println("state: " + request.getParameter("state"));
        String state = stateString[0];
        System.out.println("state: "+ state);



        String userId = identityService.getUserId(code,state,request);

        if (userId == null && request.getSession().getAttribute("userId") == null) {
            System.out.println("------userId 和 session 为空-------");
            if (stateString.length == 1){
                try {
                    System.out.println("開始第二次跳转---");
                    String url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxa2a676446e900b62&redirect_uri=http://www.i1314i.com/weixin/getUserId&response_type=code&scope=snsapi_base&agentid=1000010&state="+state+"_12#wechat_redirect";
                    System.out.println(url);
                    response.setStatus(301);
                    response.setHeader("Location",url);
                    response.setHeader( "Connection", "close" );
                    response.sendRedirect(url);
                }catch (Exception e){

                }

            }
            //失败页面
            if (state.equals("answer")) {
                return "redirect:/answerlogin";
            }
            map.put("responseMsg", "获取ID失败，请稍后重试");
            return "weixin/fail";
        }



        if (!identityService.identity(state,userId,request)){
            if (state.equals("answer")) {
                return "redirect:/answerlogin";
            }
            map.put("responseMsg","您的权限不够");
            return "weixin/fail";
        }


        if(state.equals("sc")){

            /*
                考试题是否过关验证
             */
            String resourcepath="other.properties";
            Integer examisOpen= Integer.valueOf(TemplateUtils.getPropertiesdata(resourcepath,"ExamisOpen"));

            if(examisOpen==null){
                examisOpen=0;
            }

            if(examisOpen!=0) {
                //最小过关成绩
                Integer minScore = Integer.valueOf(TemplateUtils.getPropertiesdata(resourcepath, "ExamMinScore"));
                Score score = scoreMapper.selectByPrimaryKey(userId);



                if (score == null) {
                    score=new Score();
                    score.setSno(userId);
                    score.setScore(0.0);
                    scoreMapper.insert(score);
                    session.setAttribute("exam", 0);
                }

                if(score.getScore()==null){
                    score.setScore(0.0);
                }

                if (minScore > score.getScore()) {
                    session.setAttribute("exam", 0);
                }
            }
            //我的信息
            return "redirect:/student/test";
        }else if (state.equals("ws")){
            //完善信息
            return "weixin/tian";
        }else if (state.equals("qrcode")) {
            //生成二维码
            logger.info("prprprprprprprpr");

//            map.put("userId-token",userId+"-"+qrtoken);
            return "weixin/qrcode";
        }else if (state.equals("xx")){

            JSONObject jsonObject = studentSideService.getMPerson(userId,state);
            if (jsonObject.getInt("errcode") != 0){
                map.put("responseMsg","获取个人信息失败");
                return "weixin/fail";
            }
            String name=jsonObject.getString("name");
            String mobile=jsonObject.getString("mobile");
            session.setAttribute("name",name);
            request.setAttribute("name", name);
            request.setAttribute("mobile", mobile);
            request.setAttribute("userId", userId);

            return "weixin/phoneChange";
        } else if (state.equals("sdbd")){
            //手动报到
            return "weixin/manualReport";
        }else if (state.equals("xyreason")){
            //延迟报到备注
            return "weixin/remark";
        }else if (state.equals("lcx")){
            //学院端-新生信息
            return "weixin/checkFreshman";
        }else if (state.equals("xckbd")){
//            Adminlogin adminlogin = new Adminlogin();
//            adminlogin.setType(2);
//            session.setAttribute("admindata",adminlogin);
            //学院端-报道情况
//            try{
//                collegeSideService.setCollegeId(userId,request);
//            }catch (StudentException e){
//                request.setAttribute("responseMsg",e.getMessage());
//                return "weixin/fail";
//            }
            return "weixin/collegeRate";
        }else if (state.equals("xywbd")){

            //学院端-查询未报到学生
            return "weixin/unReport";
        }else if (state.equals("zsck")){
            //招生就业-查询新生信息
            return "weixin/admissionsFreshman";
        }else if (state.equals("zsreason")){
            //招生就业-备注原因
            return "weixin/admissionsRemark";
        }else if (state.equals("zsbdl")){
//            Adminlogin adminlogin = new Adminlogin();
//            adminlogin.setType(1);
//            session.setAttribute("admindata",adminlogin);
            //招生就业-报到率
            List<ReportsRate> list = getCollegeReportsRate.getCollegeReportsRateList();
            map.put("collegeList",list);
            return "weixin/adminRate";
        }else if (state.equals("answer")){
            return "redirect:/answer/answers";
        }
        return "weixin/fail";
    }


}