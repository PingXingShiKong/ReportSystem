package com.i1314i.reportsystem.controller;

import com.i1314i.reportsystem.po.Adminlogin;
import com.i1314i.reportsystem.po.JsonResult;
import com.i1314i.reportsystem.po.Studentlogin;
import com.i1314i.reportsystem.service.LoginService;
import com.i1314i.reportsystem.service.exception.LoginException;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author 平行时空
 * @created 2018-05-12 15:43
 **/
@RestController
@RequestMapping(value = "/login")
public class LoginController {
    private final static Logger logger= LoggerFactory.getLogger(LoginController.class);
    @Autowired
    LoginService loginService;
    @RequestMapping(value = "/studentlogincheck",method = {RequestMethod.POST},produces="application/json;charset=utf-8;",consumes = MediaType.APPLICATION_JSON_VALUE)
    public JsonResult studentlogincheck(@RequestBody Studentlogin studentlogin, HttpServletRequest request) throws Exception {
        JsonResult result=JsonResult.createJsonResult();
        try {
            if(studentDataCheck(studentlogin)){
                Integer loginstatus=loginService.isStudentByNo(studentlogin,request.getSession(),request);
                if(loginstatus==1){
                    result.setSuccess(1);
                    result.setMsg("student success");
                }else if(loginstatus==2){
                    result.setSuccess(2);
                    result.setMsg("college admin success");
                }else if(loginstatus==3){
                    result.setSuccess(3);
                    result.setMsg("all admin success");
                }else {
                    result.setSuccess(0);
                    result.setMsg("error");
                }
            }
        } catch (LoginException e) {
            result.setSuccess(0);
            result.setMsg(e.getMessage());

        }
        return result;
    }


    @RequestMapping(value = "/studentloginout",method = {RequestMethod.POST},produces="application/json;charset=utf-8;")
    public JsonResult studentLoginout(HttpServletRequest request)throws Exception{
        HttpSession session=request.getSession();
        session.removeAttribute("userId");
        JsonResult jsonResult=JsonResult.createJsonResult();
        jsonResult.setSuccess(1);
        jsonResult.setMsg("退出成功");
        return jsonResult;
    }
    /**
     * 学生登陆信息校验
     * @param studentlogin
     * @return
     * @throws LoginException
     */
    public boolean studentDataCheck(Studentlogin studentlogin) throws LoginException {
        if(studentlogin==null){
            logger.info("数据传输错误");
            throw new LoginException("数据传输错误");
        }else if(studentlogin.getSno().trim().equals("")||studentlogin.getSno()==null){

            throw new LoginException("学号不能为空");
        }else if(studentlogin.getPassword().trim().equals("")||studentlogin.getPassword()==null){

            throw new LoginException("密码不能为空");
        }else if(studentlogin.getCode().trim().equals("")||studentlogin.getCode()==null){
            throw new LoginException("验证码不能为空");
        }else {
            return true;
        }
    }

    /**
     * 用于登陆页面获取用户登陆状态
     * @param session
     * @return 0 1  2  3  无登陆 学生 学院管理员 超级管理员
     * @throws Exception
     */
    @RequestMapping("/getSession")
    public Integer getSession(HttpSession session)throws Exception{
        String userId= (String) session.getAttribute("userId");
        Adminlogin adminlogin= (Adminlogin) session.getAttribute("admindata");
        if(userId!=null){
            return 1;
        }else if(adminlogin!=null){
            if(adminlogin.getType()==1){
                return 3;
            }else if(adminlogin.getType()==2) {
                return 2;
            }

        }
        return 0;

    }
}
