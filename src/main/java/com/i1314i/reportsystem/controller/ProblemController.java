package com.i1314i.reportsystem.controller;

import com.i1314i.reportsystem.po.JsonResult;
import com.i1314i.reportsystem.po.Problem;
import com.i1314i.reportsystem.po.pageHelper.PageBean;
import com.i1314i.reportsystem.service.ProblemService;
import com.i1314i.reportsystem.service.exception.ProblemException;
import com.i1314i.reportsystem.utils.CacheUtils;
import com.i1314i.reportsystem.utils.Jedis.JedisClusterClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author 平行时空
 * @created 2018-06-06 8:05
 **/
@RestController
@RequestMapping("/student")
public class ProblemController {
    @Autowired
    private ProblemService problemService;

    /**
     * 获取考试试题
     * @param request
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/getExam",method = {RequestMethod.POST},produces="application/json;charset=utf-8;")
    public JsonResult getExam(HttpServletRequest request) throws IOException {
        JsonResult jsonResult=JsonResult.createJsonResult();
        String userId= (String) request.getSession().getAttribute("userId");


        try {
            List<Problem>problems=problemService.getProblems(userId);
            jsonResult.setData(problems);
            problems=null;
            jsonResult.setSuccess(0);
            jsonResult.setMsg("获取试题成功");
        } catch (ProblemException e) {
            jsonResult.setMsg(e.getMessage());
            jsonResult.setSuccess(1);
        }
        return jsonResult;
    }

    /**
     * 提交试卷获取成绩
     * @param request
     * @param map
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/updateExam",method = {RequestMethod.POST},produces="application/json;charset=utf-8;")
    public JsonResult updateExamData(HttpServletRequest request, @RequestBody Map<Integer, String> map) throws IOException {
        JsonResult jsonResult=JsonResult.createJsonResult();
        String userId= (String) request.getSession().getAttribute("userId");
        double[]answ=null;
        try {
            answ=problemService.getScore(map,userId);
        } catch (ProblemException e) {
           jsonResult.setSuccess(0);
           jsonResult.setMsg(e.getMessage());
        }

        if(answ[0]==1){
            jsonResult.setData(answ[0]);
            request.getSession().removeAttribute("exam");
            jsonResult.setMsg("恭喜您测验过关,请您查询您的报道信息");
            jsonResult.setSuccess(1);
        }else{
            request.getSession().setAttribute("exam",0);
            jsonResult.setMsg("您的考试成绩不合格，请再次考试");
            jsonResult.setSuccess(0);
        }
        jsonResult.setData(answ[1]);

        answ=null;
        return jsonResult;
    }


    /**
     * 获取考试状态
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getexamstatus",method = {RequestMethod.POST},produces="application/json;charset=utf-8;")
    public JsonResult isjump(HttpServletRequest request)throws Exception{
        String userId= (String) request.getSession().getAttribute("userId");
        JsonResult jsonResult=JsonResult.createJsonResult();
        boolean status=problemService.isJump(userId);
        if(status){
            jsonResult.setSuccess(1);
            request.getSession().removeAttribute("exam");
            jsonResult.setMsg("考试过关");
        }else {
            jsonResult.setSuccess(0);
            request.getSession().setAttribute("exam",0);
            jsonResult.setMsg("您的考试未过关,请考试");
        }
        return jsonResult;
    }



}
