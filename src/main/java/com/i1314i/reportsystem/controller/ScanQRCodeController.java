package com.i1314i.reportsystem.controller;


import com.i1314i.reportsystem.service.ScanQRCodeService;
import com.i1314i.reportsystem.utils.weixin.ParamesAPI.util.ParamesAPI;
import com.i1314i.reportsystem.utils.weixin.encryption.AesException;
import com.i1314i.reportsystem.utils.weixin.encryption.WXBizMsgCrypt;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

@Controller
@RequestMapping("/ScanQRCode")
public class ScanQRCodeController {

    @RequestMapping(value = "/ScanQRCodeGet" , method={RequestMethod.GET})
    public void QRCodeGet(HttpServletRequest request, HttpServletResponse response)  {
        String a = request.getMethod();
        System.out.println("method"+a);
        System.out.println("get");
        // 微信加密签名
        String msg_signature = request.getParameter("msg_signature");
        // 时间戳
        String timestamp = request.getParameter("timestamp");
        // 随机数
        String nonce = request.getParameter("nonce");
        // 随机字符串
        String echostr = request.getParameter("echostr");
        // 打印请求地址
        System.out.println("request=" + request.getRequestURL());
        // 流
        PrintWriter out = null;
        try {
            out = response.getWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 通过检验signature对请求进行校验，若校验成功则原样返回echostr，表示接入成功，否则接入失败
        String result = null;
        try {
            WXBizMsgCrypt wxcpt = new WXBizMsgCrypt(ParamesAPI.token,ParamesAPI.encodingAESKey,ParamesAPI.corpId);
            // 验证URL函数
            result = wxcpt.VerifyURL(msg_signature, timestamp, nonce, echostr);
        } catch (AesException e) {
            e.printStackTrace();
        }
        if (result == null) {
            // result为空，赋予token
            result = ParamesAPI.token;
        }
        // 拼接请求参数
        String str = msg_signature+" "+timestamp+" "+nonce+" "+echostr;
        // 打印参数+地址+result
        System.out.println("Exception:"+result+" "+ request.getRequestURL()+" "+"FourParames:"+str);
        out.print(result);
        out.close();
    }

    @Autowired
    ScanQRCodeService scanQRCodeService;

    @RequestMapping(value = "/ScanQRCodeGet" , method = {RequestMethod.POST})
    public void QRCodePost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //将请求、响应的编码均设置为UTF-8（防止中文乱码）
        System.out.println("post");
        try {
            request.setCharacterEncoding("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        response.setCharacterEncoding("UTF-8");

        // 微信加密签名
        String msg_signature = request.getParameter("msg_signature");
        // 时间戳
        String timestamp = request.getParameter("timestamp");
        // 随机数
        String nonce = request.getParameter("nonce");

        //从请求中读取整个post数据
        InputStream inputStream = request.getInputStream();
        //commons.io.jar 方法
        String Post = IOUtils.toString(inputStream, "UTF-8");
        // Post打印结果
        System.out.println(Post);

        String Msg = "";
        WXBizMsgCrypt wxcpt = null;
        try {
            wxcpt = new WXBizMsgCrypt(ParamesAPI.token,ParamesAPI.encodingAESKey,ParamesAPI.corpId);
            //解密消息
            Msg = wxcpt.DecryptMsg(msg_signature, timestamp, nonce, Post);
        } catch (AesException e) {
            e.printStackTrace();
        }
        // Msg打印结果
        System.out.println("Msg打印结果：" + Msg);

        // 调用核心业务类接收消息、处理消息
        String respMessage = scanQRCodeService.processRequest(Msg);

        // respMessage打印结果
        System.out.println("respMessage打印结果：" + respMessage);
        String encryptMsg = "";
        try {
            //加密回复消息
            encryptMsg = wxcpt.EncryptMsg(respMessage, timestamp, nonce);
        } catch (AesException e) {
            e.printStackTrace();
        }

        // 响应消息
        PrintWriter out = response.getWriter();
        out.print(encryptMsg);
        out.close();
    }

}
