package com.i1314i.reportsystem.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.i1314i.reportsystem.po.*;

import com.i1314i.reportsystem.service.LoginService;
import com.i1314i.reportsystem.service.ProblemService;
import com.i1314i.reportsystem.service.StudentService;
import com.i1314i.reportsystem.service.StudentSideService;
import com.i1314i.reportsystem.service.exception.StudentException;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author 平行时空
 * @created 2018-05-12 15:43
 **/
@RestController
@RequestMapping("/student")
public class StudentController {
    private final static Logger logger= LoggerFactory.getLogger(StudentController.class);
    @Autowired
    StudentService studentService;
    @Autowired
    LoginService loginService;
    @Autowired
    ProblemService problemService;

    @Autowired
    StudentSideService studentSideService;

    /**
     * 用于登录后获取学生信息
     * @param request
     * @return JsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/getstudentdatas")
    public JsonResult getStudentData(HttpServletRequest request) throws Exception {
        HttpSession session=request.getSession();
        JsonResult jsonResult=JsonResult.createJsonResult();
        String userId= (String) session.getAttribute("userId");
        String forurl="";
        StudentInfo data=new StudentInfo();
        if(userId!=null){
//            Stcompleteinfo sdata=studentService.findInfoBySno(userId);
            Basicinfo basicinfo=studentService.findStudentDataBySno(userId);
            data.setBasicinfo(basicinfo);
//            data.setStcompleteinfo(sdata);
            jsonResult.setSuccess(1);
            jsonResult.setMsg("success");
            jsonResult.setData(data);

            basicinfo=null;
            data=null;
          //  forurl="stu/index";
        }else{
            jsonResult.setSuccess(0);
            jsonResult.setMsg("您未登录，未查询到您的信息");
        }


        return jsonResult;
    }



    /**
     * 获取更新信息
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getStcompleteinfo")
    public JsonResult getStudentStcompleteinfo(HttpServletRequest request, Model model)throws Exception{
        JsonResult jsonResult=null;
        try {
             jsonResult=studentService.getInfoBySnoResult(request);
        } catch (StudentException e) {
            jsonResult=JsonResult.createJsonResult(0,e.getMessage());
        }

        return jsonResult;
    }
    /**
     * 用户上传信息
     * @param stcompleteinfo
     * @param request
     * @return
     */

    @RequestMapping(value = "/updateStcompleteinfo",method = {RequestMethod.POST},produces="application/json;charset=utf-8;")
    public JsonResult updateStcompleteinfo(@RequestBody Stcompleteinfo stcompleteinfo,HttpServletRequest request) throws JsonProcessingException {
        JsonResult jsonResult=null;
        try {
            if(checkStcompleteinfo(stcompleteinfo)){
                jsonResult=studentService.updateStcompleteinfo(stcompleteinfo,request);
            }
        } catch (StudentException e) {
            jsonResult=JsonResult.createJsonResult(0,e.getMessage());
        }
        return jsonResult;
    }
    /**
     * 用户上传校验
     * @param stcompleteinfo
     * @return
     * @throws StudentException
     */
    public boolean checkStcompleteinfo(Stcompleteinfo stcompleteinfo) throws StudentException {
        boolean stutas=false;
        if(stcompleteinfo.getPhonenum()==null||stcompleteinfo.getPhonenum().trim().equals("")){
            logger.info(stcompleteinfo.getSno()+":联系方式不能为空");
            throw new StudentException("联系方式不能为空");
        }

        if(stcompleteinfo.getMilitaryclothing()==null||stcompleteinfo.getMilitaryclothing().trim().equals("")){
            logger.info(stcompleteinfo.getSno()+":是否购买军训服不能为空");
            throw new StudentException("是否购买军训服不能为空");
        }else {
            if(stcompleteinfo.getMilitaryclothing().trim().equals("购买")){
                if(stcompleteinfo.getShoenum()==null||stcompleteinfo.getShoenum()==0){
                    logger.info(stcompleteinfo.getSno()+":鞋号不能为空或者0");
                    throw new StudentException("鞋号不能为空或者0");
                }else if(stcompleteinfo.getHeight()==null||stcompleteinfo.getHeight()==0){
                    logger.info(stcompleteinfo.getSno()+":身高不能为空或者0");
                    throw new StudentException("身高不能为空或者0");
                }else if(stcompleteinfo.getWeight()==null||stcompleteinfo.getWeight()==0){
                    logger.info(stcompleteinfo.getSno()+":体重不能为空或者0");
                    throw new StudentException("体重不能为空或者0");
                }
            }
        }

        if(stcompleteinfo.getParentname()==null||stcompleteinfo.getParentname().trim().equals("")){
            logger.info(stcompleteinfo.getSno()+":父母名字不能为空");
            throw new StudentException("父母名字不能为空");
        }else if(stcompleteinfo.getParentphonenum()==null||stcompleteinfo.getParentphonenum().trim().equals("")){
            logger.info(stcompleteinfo.getSno()+":父母联系方式不能为空");
            throw new StudentException("父母联系方式不能为空");
        }else if(stcompleteinfo.getRegistertime()==null||stcompleteinfo.getRegistertime().trim().equals("")){
            logger.info(stcompleteinfo.getSno()+":预计报到时间不能为空");
            throw new StudentException("预计报到时间不能为空");
        }else if(stcompleteinfo.getCompany()==null){
            logger.info(stcompleteinfo.getSno()+":同行人数不能为空");
            throw new StudentException("同行人数不能为空");
        }

        stutas=true;
        return stutas;

    }


    /**
     * 获取考试结果
     * @return
     * @throws Exception
     */
    @RequestMapping("/studentgrade")
    public JsonResult getGrade(List<Problem>problems)throws Exception{
        return null;
    }

    @RequestMapping(value = "/changestudentlogin",method = {RequestMethod.POST},produces="application/json;charset=utf-8;")
    public JsonResult changeStudentLoginInfo(@RequestBody ChangeStudentLogin changeStudentLogin,HttpServletRequest request){
        String sno= (String) request.getSession().getAttribute("userId");
        changeStudentLogin.setSno(sno);
        return studentService.changelogininfo(changeStudentLogin);
    }


    @RequestMapping(value = "/changephones",method = {RequestMethod.POST},produces="application/json;charset=utf-8;")
    public JsonResult changephone(@RequestBody Studentlogin studentlogin,HttpSession session)throws Exception{
        String userid=(String) session.getAttribute("userId");
        JSONObject jsonObject=studentSideService.getMPerson(userid,"xx");
        JsonResult jsonResult=JsonResult.createJsonResult();
        if (jsonObject.getInt("errcode") != 0){
            jsonResult.setMsg("获取个人信息失败");
            jsonResult.setSuccess(0);
        }else {
            String name=jsonObject.getString("name");
            String mobile=jsonObject.getString("mobile");
            String data=studentSideService.phoneChange(userid,studentlogin.getNewphone(),"xx");
            String []datess=data.split("-");
            if(datess[0].trim().equals("0")){
                jsonResult.setSuccess(1);
                jsonResult.setMsg("修改手机号成功");
            }else {
                jsonResult.setSuccess(0);
                jsonResult.setMsg("修改手机号失败");
            }
        }
        return jsonResult;
    }

}
