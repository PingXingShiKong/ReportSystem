package com.i1314i.reportsystem.controller;

import com.i1314i.reportsystem.po.Basicinfo;
import com.i1314i.reportsystem.po.JsonResult;
import com.i1314i.reportsystem.po.News;
import com.i1314i.reportsystem.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @author 平行时空
 * @created 2018-06-19 8:57
 **/
@RestController
public class StudentSelectController {
    @Autowired
    StudentService studentService;
    @RequestMapping(value = "/getStudentluqu",method = {RequestMethod.POST},produces="application/json;charset=utf-8;")
    public JsonResult getStudentSc(@RequestBody Basicinfo basicinfo) throws IOException {
        JsonResult jsonResult=studentService.selectStudentIsGetSchool(basicinfo);
        return jsonResult;
    }

        @RequestMapping(value = "/getpublicinfo",method = {RequestMethod.POST},produces="application/json;charset=utf-8;")
    public JsonResult getPublicInfo(@RequestBody News news){
                JsonResult jsonResult=studentService.studentPublicInfo(news.getType());
               return  jsonResult;
           }
}
