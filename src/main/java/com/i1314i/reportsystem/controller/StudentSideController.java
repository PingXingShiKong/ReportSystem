package com.i1314i.reportsystem.controller;

import com.i1314i.reportsystem.service.StudentSideService;
import com.i1314i.reportsystem.utils.CacheGetKeyUtils;
import com.i1314i.reportsystem.utils.CacheUtils;
import com.i1314i.reportsystem.utils.Jedis.IJedisClient;
import com.i1314i.reportsystem.utils.Jedis.JedisClusterClient;
import com.i1314i.reportsystem.utils.TemplateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
@RequestMapping("/weixin")
public class StudentSideController {
    private static Logger logger = Logger.getLogger(StudentSideController.class);
    @Autowired
    StudentSideService studentSideService;
    @Autowired
    private IJedisClient jedisClient;

    @RequestMapping("/phoneChange")
    public String phoneChange(HttpServletRequest request)throws IOException{
        String userId = request.getSession().getAttribute("userId").toString();
        String name = request.getSession().getAttribute("name").toString();
        String mobile = request.getParameter("newtel");
        String state = "xx";

        //String change_post = studentSideService.phoneChange(userId,name,mobile,state);

        String change_post = studentSideService.phoneChange(userId,mobile,state);

        String change = change_post.split("-")[0];
        String post = change_post.split("-")[1];
        if (change.equals("0")){
            request.setAttribute("responseMsg","修改成功");
            return "weixin/success";
        }
        request.setAttribute("responseMsg","修改失败");
        return "weixin/fail";
    }

    @RequestMapping("/qrCode")
    public String qrCode(HttpServletRequest request) throws IOException {
        String userId = (String) request.getSession().getAttribute("userId");
        String resourcepath="other.properties";
        //缓存类型
        String cacheType=TemplateUtils.getPropertiesdata(resourcepath,"cache.type");

        boolean cacheStatus=cacheType.trim().equalsIgnoreCase("redis");

        boolean status=false;
        String key=CacheGetKeyUtils.getQrKey(userId);
        System.out.println(key);
//            Jedis jedis=null;
        String qrtoken=TemplateUtils.uuid();

        if(cacheStatus){
//                jedis=JedisUtils.getJedis();
            //key不存在
            logger.info("刷新二维码校验token(redis)："+qrtoken);
//                    jedis.set(key,qrtoken);
            jedisClient.set(key,qrtoken,60*60*24*7);
            //设置缓存时间
//                    jedis.expire(key,60*60*24*7);
//                    jedis.close();
        }else {
            //加入缓存
            CacheUtils.qrconcurrentHashMap.put(key,qrtoken);
            logger.info("刷新二维码校验token(concurrentHashMap)："+qrtoken);
        }
        return "redirect:/images/getqrs?content=" + userId +"-"+ qrtoken;
    }
}
