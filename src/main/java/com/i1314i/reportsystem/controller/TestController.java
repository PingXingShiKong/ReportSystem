package com.i1314i.reportsystem.controller;

import com.i1314i.reportsystem.mapper.StudentloginMapper;
import com.i1314i.reportsystem.po.Adminlogin;
import com.i1314i.reportsystem.po.Studentlogin;
import com.i1314i.reportsystem.po.StudentloginExample;
import com.i1314i.reportsystem.utils.JavaPoiUtil;
import com.i1314i.reportsystem.utils.TemplateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 平行时空
 * @created 2018-05-09 21:31
 **/
@Controller
public class TestController {
    @Autowired
    StudentloginMapper studentloginMapper;

    @RequestMapping(value = "/index")
    public String index() throws Exception {
        return "index";
    }

    @RequestMapping(value = "/poi")
    public String poi(HttpServletResponse response) throws Exception {
        response.reset();
        response.setContentType("application/msexcel");
        response.setHeader("Content-Disposition", "attachment;filename=" + JavaPoiUtil.getExcelName("用户表单", 2003));
        StudentloginExample example = new StudentloginExample();


        List<Studentlogin> studentlogins = studentloginMapper.selectByExample(example);
        String[] keys = {"sno", "password"};//要写出的字段名
        String columnNames[] = {"学号", "密码"};//列名字
        OutputStream outputStream = response.getOutputStream();
        JavaPoiUtil.createWorkBooks(Studentlogin.class,studentlogins, keys, columnNames, 2003).write(outputStream);



        //        JavaPoiUtil.createWorkBooks(studentlogins, columnNames, 2003).write(outputStream);

        return null;
    }
    @RequestMapping(value = "/pois")
    public String pois(HttpServletResponse response,@RequestParam(name = "file") MultipartFile file)throws Exception{
        // 获取文件名
        String fileName = file.getOriginalFilename();
        System.out.println(fileName);
        // 获取文件后缀
        String prefix=fileName.substring(fileName.lastIndexOf("."),fileName.length());
        // 用uuid作为文件名，防止生成的临时文件重复
         File excelFile = File.createTempFile(TemplateUtils.uuid(), prefix);
        // MultipartFile to File
        file.transferTo(excelFile);


        //你的业务逻辑

        //程序结束时，删除临时文件

//
//        List<Studentlogin> studentlogins=JavaPoiUtil.getExcelList(Studentlogin.class,excelFile,null,2003);
//            for(int i=0;i<studentlogins.size();i++){
//            Studentlogin studentlogin=studentlogins.get(i);
//            System.out.println(studentlogin.toString());
//        }
        deleteFile(excelFile);
        return null;
    }


    /**
     * 删除
     *
     * @param files
     */
    private void deleteFile(File... files) {
        for (File file : files) {
            if (file.exists()) {
                file.delete();
            }
        }
    }

    /**
     * 方法二 自己生成List<map>不推荐
     * @param studentlogins
     * @return
     */
    private List<Map<String, Object>> createExcelRecord(List<Studentlogin> studentlogins) {
        List<Map<String, Object>> listmap = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sheetName", "sheet1");
        listmap.add(map);
        Studentlogin studentlogin = null;
        for (int i = 0; i < studentlogins.size(); i++) {
            studentlogin = studentlogins.get(i);
            Map<String, Object> mapValue = new HashMap<>();
            mapValue.put("sno", studentlogin.getSno());
            mapValue.put("password", studentlogin.getPassword());
            listmap.add(mapValue);
        }
        return listmap;
    }






//        if (values.size() > 0) {
//            for (int i = 0; i < values.size(); i++) {
//                //通过反射创建一个其他类的对象
////                bean = clazz.newInstance();
////                Field[] f = bean.getClass().getFields();
//
//                value = new HashMap<>();
//                T data = values.get(i);
//                for (int j = 0; j < names.length; j++) {
//                    Field newage = data.getClass().getDeclaredField(names[j]);
//                    System.out.println(newage);
//                    newage.setAccessible(true);
//                    value.put(names[j], newage);
//                }
//                result.add(value);
//            }
//        }


}