package com.i1314i.reportsystem.controller;

import com.i1314i.reportsystem.mapper.CollegeMapper;
import com.i1314i.reportsystem.mapper.MajorMapper;
import com.i1314i.reportsystem.po.College;
import com.i1314i.reportsystem.po.Major;
import com.i1314i.reportsystem.utils.CacheGetKeyUtils;
import com.i1314i.reportsystem.utils.JavaPoiUtil;
import com.i1314i.reportsystem.utils.Jedis.IJedisClient;
import com.i1314i.reportsystem.utils.Jedis.JedisClusterClient;
import com.i1314i.reportsystem.utils.QRUtil.QRCodeUtil;
import com.i1314i.reportsystem.utils.TemplateUtils;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.*;

/**
 * @author 平行时空
 * @created 2018-08-26 21:02
 **/
@RestController
public class TestControllers {
    @Autowired
    private IJedisClient jedisClient;
    @Autowired
    private MajorMapper majorMapper;
    @Autowired
    CollegeMapper collegeMapper;
//    @RequestMapping(value = "/imdddd")
        public void shengchebg()throws Exception{
            File file=new File("C:\\img\\测试数据导入.xls");
            List <List<Object>> a= JavaPoiUtil.getExcelList(file);

            Map<String,String> list=new HashMap<>();

            Map<String,String> maps=new HashMap<>();
            for(int i=0;i<a.size();i++){
                List<Object> b=a.get(i);
                maps.put((String)b.get(0),(String)b.get(6));
                //  maps.put((String) b.get(0),(String) b.get(1));
                // System.out.println((String) b.get(0)+"——"+(String) b.get(6));
                File file1=new File("C:\\img\\test\\"+(String) b.get(6));

                Boolean aa=file1.mkdir();// true


                System.out.println(aa);
            }

            souts(maps);
        }



     

        @RequestMapping(value = "/flush")
        public Set<String> flushall()throws Exception{
            //jedis.allkeys(CacheGetKeyUtils.getStudentbasicinfo("*"));
        return  jedisClient.allkeys(CacheGetKeyUtils.getStudentbasicinfo("*"));
        }

    @RequestMapping(value = "/flushall")
        public String flushinfo()throws Exception{
        jedisClient.deleteRedisKeyStartWith(CacheGetKeyUtils.getStudentbasicinfo("*"));
            return "success";
        }

    public void souts( Map<String,String> map) throws Exception {


        for (String sno : map.keySet()) {

            //  System.out.println(key);
            String key= CacheGetKeyUtils.getQrKey(sno);
            System.out.println(key);

            String qrtoken= TemplateUtils.uuid();
            System.out.println(qrtoken);
            // cluster.set(key,qrtoken);
//            cluster.expire(key,60*60*24*7);


            jedisClient.set(key,qrtoken,60*60*24*7);

               System.out.println("ssss-------"+jedisClient.get(key)+"success");
              QRCodeUtil.encode(sno +"-"+ qrtoken,"C:\\img\\qrlogo.jpg", "C:\\img\\test\\"+map.get(sno),true,sno);

        }
    }
    @RequestMapping("/getCollegetByName")
    public Major test(HttpServletRequest request){
     return majorMapper.selectByCollegeIdAndMajorName(Integer.valueOf(request.getParameter("collegeId")),request.getParameter("major"));
    }

    public static void main(String[] args) {
        System.out.println(TemplateUtils.getPropertiesdata("other.properties","isFirstImport"));

    }
}
