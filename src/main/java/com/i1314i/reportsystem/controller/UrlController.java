package com.i1314i.reportsystem.controller;

import com.i1314i.reportsystem.po.ReportsRate;
import com.i1314i.reportsystem.service.admin.GetCollegeReportsRate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author 平行时空
 * @created 2018-05-13 16:45
 **/
@Controller
public class UrlController {


//
//    @RequestMapping(value = "/student/test")
//    public String test()throws Exception{
//        return "weixin/test";
//    }

    @RequestMapping(value = "/login")
    public String studentlogin()throws Exception{
        return "student/login";
    }

    /**
     * 微信获取答案登陆
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/answerlogin")
    public String wechatanswerlogin()throws Exception{
        return "student/answerlogin";
    }

    @RequestMapping(value = "/answer/answers")
    public String getanswer()throws Exception{
        return "student/answers";
    }
    @RequestMapping(value = "/")
    public String studentlogins()throws Exception{
        return "student/login";
    }
    @RequestMapping(value = "/student/index")
    public String studentindex()throws Exception{
        return "student/index";
    }

    @RequestMapping(value = "/student/info")
    public String studentinfo()throws Exception{
        return "student/studentinfo";
    }

    @RequestMapping(value = "/student/changeinfo")
    public String studentchangeinfo()throws Exception{
        return "student/studentinfochange";
    }

    @RequestMapping(value = "/student/changephone")
    public String studentchangewechatphone()throws Exception{
        return "student/studentphonechange";
    }


    @RequestMapping(value = "/student/uploadself")
    public String studentupload()throws Exception{
        return "student/studentinfoupload";
    }

    @RequestMapping(value = "/student/problem")
    public String studentTest()throws Exception{
        return "student/problem";
    }

    @RequestMapping(value = "/student/questions")
    public String question()throws Exception{
        return "weixin/questions";
    }
    @RequestMapping(value = "/student/points")
    public String weixinTest()throws Exception{
        return "weixin/points";
    }
    @RequestMapping(value = "/weixin/collegeRate")
    public String collegeRate()throws Exception{
        return "weixin/collegeRate";
    }
    @RequestMapping(value = "/student/test")
    public String unReport()throws Exception{
        return "weixin/test";
    }
    @RequestMapping(value = "/weixin/success")
    public String success()throws Exception{
        return "weixin/success";
    }
    @RequestMapping(value = "/weixin/fail")
    public String fail()throws Exception{
        return "weixin/fail";
    }
    @RequestMapping(value = "/weixin/classRate")
    public String classRates(){
        return "weixin/classRate";
    }

    @RequestMapping(value = "/colandsch/bjbaodaolvs")
    public String bjbaodaolvs(){
        return "xueyuan/bjbaodaolv";
    }

    @RequestMapping(value = "/colandsch/baodaolvs")
    public String baodaolvss(){
        return "xueyuan/baodaolv";
    }

    @RequestMapping(value = "/selectself")
    public String studentselect(){
        return "student/studentselect";
    }

    @RequestMapping(value = "/loginouts")
    public String loginout(HttpServletRequest request){
        request.getSession().removeAttribute("admindata");
        return "redirect:/login";
    }

    //学生报道
    @RequestMapping(value = "/wechatstudentbd")
    public void wechatbao(HttpServletResponse response) throws IOException {
        String url="https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxa2a676446e900b62&redirect_uri=http://www.i1314i.com/weixin/getUserId&response_type=code&scope=snsapi_base&agentid=1000010&state=qrcode#wechat_redirect";
        url301(url,response);
    }

    //学生查看信息
    @RequestMapping(value = "/wechatstudentselectinfo")
    public void wechatselectinfo(HttpServletResponse response) throws IOException {
        String url="https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxa2a676446e900b62&redirect_uri=http://www.i1314i.com/weixin/getUserId&response_type=code&scope=snsapi_base&agentid=1000010&state=sc#wechat_redirect";
        url301(url,response);
    }

    //学生完善信息
    @RequestMapping(value = "/wechatstudentupdatinfo")
    public void wechatupdateinfo(HttpServletResponse response) throws IOException {
        String url="https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxa2a676446e900b62&redirect_uri=http://www.i1314i.com/weixin/getUserId&response_type=code&scope=snsapi_base&agentid=1000010&state=ws#wechat_redirect";
        url301(url,response);
    }

    //学生修改手机号
    @RequestMapping(value = "/wechatstudentchangephone")
    public void wechatchangephone(HttpServletResponse response) throws IOException {
        String url="https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxa2a676446e900b62&redirect_uri=http://www.i1314i.com/weixin/getUserId&response_type=code&scope=snsapi_base&agentid=1000010&state=xx#wechat_redirect";
        url301(url,response);
    }

    public static void url301(String url, HttpServletResponse response) throws IOException {
        response.setStatus(301);
        response.setHeader("Location",url);
        response.setHeader( "Connection", "close" );
        response.sendRedirect(url);
    }
}
