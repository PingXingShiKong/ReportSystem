package com.i1314i.reportsystem.controller.admin;

import com.alibaba.fastjson.JSONObject;
import com.i1314i.reportsystem.mapper.CollegeMapper;
import com.i1314i.reportsystem.po.Adminlogin;
import com.i1314i.reportsystem.po.College;
import com.i1314i.reportsystem.po.JsonResult;
import com.i1314i.reportsystem.service.admin.JudgeAdm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 添加学院管理员controller
 */
@RestController
@RequestMapping("/school")
public class AddAdmController {
    @Autowired
    JudgeAdm judgeAdm;
    @Autowired
    CollegeMapper collegeMapper;

    @RequestMapping("/addadm")
    public JSONObject addAdministratro(HttpServletRequest request) {
        String result;
        try {
            Integer id= Integer.valueOf(request.getParameter("id"));
            String college =request.getParameter("college");//存到数据库的是学院id
            String password = request.getParameter("password");
            String adminname = request.getParameter("name");
//            Integer type = Integer.parseInt(request.getParameter("type"));
            Adminlogin adminlogin = new Adminlogin();
            adminlogin.setCollege(college);
            adminlogin.setId(id);
            adminlogin.setPassword(password);
            adminlogin.setType(2);
            adminlogin.setAdminname(adminname);
            result = judgeAdm.judegAdministratoreexist(adminlogin);
        } catch (Exception e) {
            result = "您输入的信息有误请核对后重新输入";
        }
        JSONObject jObject = new JSONObject();
        jObject.put("msg", result);
        return jObject;
    }

    @RequestMapping(value = "/getAllCollege",method = {RequestMethod.GET})
    public JsonResult getAllCollege(){
        JsonResult jsonResult=JsonResult.createJsonResult();
        try {
            List<College>collegeList=collegeMapper.selectAllColleges();

            jsonResult.setData(collegeList);
            jsonResult.setMsg("success");
            jsonResult.setSuccess(1);
        }catch (Exception e){
            jsonResult.setMsg("fail");
            jsonResult.setSuccess(0);
        }

        return jsonResult;
    }
}