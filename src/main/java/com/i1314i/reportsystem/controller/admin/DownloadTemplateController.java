package com.i1314i.reportsystem.controller.admin;

import com.i1314i.reportsystem.service.admin.DownloadTemplateService;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

@Controller
@RequestMapping("/school")
public class DownloadTemplateController {
    @Autowired
    DownloadTemplateService downloadTemplateService;
    @RequestMapping("/downbasicinfo")
    public void downloadBasicInformation(HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename=student.xls");
        OutputStream ouputStream = response.getOutputStream();
        String[] informatin={"学号","姓名","性别","考生号","身份证号","校区","学院","录取专业","班级","宿舍","银行卡号","缴费情况","学院联系方式","学校联系方式","辅导员","辅导员联系方式","班主任","班主任联系方式","班助","班助联系方式"};
        HSSFWorkbook wb=downloadTemplateService.export(informatin);
        wb.write(ouputStream);
        ouputStream.flush();
        ouputStream.close();
    }

    @RequestMapping("/downbuchong")
    public void downloadBuchongInformation(HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename=student.xls");
        OutputStream ouputStream = response.getOutputStream();
        String[] informatin={"学号","班级","宿舍","辅导员","辅导员联系方式","班主任","班主任联系方式","班助","班助联系方式"};
        HSSFWorkbook wb=downloadTemplateService.export(informatin);
        wb.write(ouputStream);
        ouputStream.flush();
        ouputStream.close();
    }

    @RequestMapping("/downpaysta")
    public void downloadPaysta(HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename=student.xls");
        OutputStream ouputStream = response.getOutputStream();
        String[] informatin={"学号","缴费情况"};
        HSSFWorkbook wb=downloadTemplateService.export(informatin);
        wb.write(ouputStream);
        ouputStream.flush();
        ouputStream.close();
    }
}
