package com.i1314i.reportsystem.controller.admin;

import com.i1314i.reportsystem.mapper.BasicinfoMapper;
import com.i1314i.reportsystem.mapper.ExportpoMapper;
import com.i1314i.reportsystem.po.Basicinfo;
import com.i1314i.reportsystem.po.Studentlogin;
import com.i1314i.reportsystem.po.StudentloginExample;
import com.i1314i.reportsystem.service.admin.Exportpo;
import com.i1314i.reportsystem.service.admin.ExporyInformationService;
import com.i1314i.reportsystem.service.admin.ExporyService;
import com.i1314i.reportsystem.utils.JavaPoiUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/school")
public class ExportController {
    @Autowired
    BasicinfoMapper basicinfoMapper;
    @Autowired
    ExporyService exporyService;
    @Autowired
    ExportpoMapper exportpoMapper;
    @Autowired
    ExporyInformationService exporyInformationService;

    //导出报道信息
    @RequestMapping("/export")
    public void exportExcel(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename=student.xls");
        OutputStream ouputStream = response.getOutputStream();
        List<Basicinfo> basicinfoList=basicinfoMapper.selectAll();
        HSSFWorkbook wb=exporyService.export(basicinfoList);
        wb.write(ouputStream);
        ouputStream.flush();
        ouputStream.close();
    }
    @ResponseBody
    @RequestMapping("/exportinformation")
    public void exportInformation(Exportpo exportpo,HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename=student.xls");
        OutputStream ouputStream = response.getOutputStream();
        List<Exportpo> exportpoList=exportpoMapper.selectAll();
        HSSFWorkbook wb=exporyInformationService.export(exportpo,exportpoList);
        wb.write(ouputStream);
        ouputStream.flush();
        ouputStream.close();
    }



}
