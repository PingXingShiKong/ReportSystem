package com.i1314i.reportsystem.controller.admin;

import com.i1314i.reportsystem.mapper.*;
import com.i1314i.reportsystem.po.*;
import com.i1314i.reportsystem.service.admin.Exportpo;
import com.i1314i.reportsystem.service.admin.GetPasswordService;
import com.i1314i.reportsystem.service.admin.InsertService;
import com.i1314i.reportsystem.service.admin.JudgeExist;
import com.i1314i.reportsystem.utils.CacheGetKeyUtils;
import com.i1314i.reportsystem.utils.ExcelImportUtil;
import com.i1314i.reportsystem.utils.JavaPoiUtil;
import com.i1314i.reportsystem.utils.Jedis.IJedisClient;
import com.i1314i.reportsystem.utils.TemplateUtils;
import com.i1314i.reportsystem.utils.file.FileUtil;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.i1314i.reportsystem.utils.TemplateUtils.uuid;

/**
 * 导入excel到数据库
 */
@Controller
@RequestMapping("/school")
public class ImportController {
    @Autowired
    ExportpoMapper exportpoMapper;
    @Autowired
    InsertService insertService;
    @Autowired
    BasicinfoMapper basicinfoMapper;
    @Autowired
    JudgeExist judgeExist;
    @Autowired
    GetPasswordService getPasswordService;
    @Autowired
    StudentloginMapper studentloginMapper;
    @Autowired
    StcompleteinfoMapper stcompleteinfoMapper;

    @Autowired
    MajorMapper majorMapper;

    @Autowired
    CollegeMapper collegeMapper;

    @Autowired
    private IJedisClient jedisClient;


    private final static Logger logger = LoggerFactory.getLogger(ImportController.class);

    @ResponseBody
    @RequestMapping(value = "/testa")
    public List<Exportpo> test() {
        List<Exportpo> exportpoList = exportpoMapper.selectAll();
        return exportpoList;
    }

    @RequestMapping(value = "/clearCache")
    public @ResponseBody String  clearCache(){
        jedisClient.deleteRedisKeyStartWith(CacheGetKeyUtils.getStudentbasicinfo("*"));
//        jedisClient.deleteRedisKeyStartWith(CacheGetKeyUtils.getProblemKey("*"));
        jedisClient.deleteRedisKeyStartWith(CacheGetKeyUtils.getStudentstcompleteinfo("*"));
        jedisClient.deleteRedisKeyStartWith("listbaseinfo_*");
        jedisClient.deleteRedisKeyStartWith(CacheGetKeyUtils.getListbaseinfobyname("*"));
        return "success";
    }

    public void clearCaches(){
        jedisClient.deleteRedisKeyStartWith(CacheGetKeyUtils.getStudentbasicinfo("*"));
//        jedisClient.deleteRedisKeyStartWith(CacheGetKeyUtils.getProblemKey("*"));
        jedisClient.deleteRedisKeyStartWith(CacheGetKeyUtils.getStudentstcompleteinfo("*"));
        jedisClient.deleteRedisKeyStartWith("listbaseinfo_*");
        jedisClient.deleteRedisKeyStartWith(CacheGetKeyUtils.getListbaseinfobyname("*"));
    }

    //导入基础信息
    @RequestMapping(value = "/basicinfo")
    @ResponseBody
    public Map upload(@RequestParam(value = "file", required = false) MultipartFile file) throws IOException, ClassNotFoundException {
        List<Map<Integer, String>> list = ExcelImportUtil.readExcel(file);
        int addnum = 0;
        int failnum = 0;
        int updatenum = 0;
        for (int i = 0; i < list.size(); i++) {
            String password = getPasswordService.getPassword(list.get(i).get(4));
            Studentlogin studentlogin = new Studentlogin();//在studentlogin表里设置学生密码为身份证后六位
            studentlogin.setSno(list.get(i).get(0));
            studentlogin.setPassword(password);

            Basicinfo basicinfo = new Basicinfo();
            basicinfo.setSno(list.get(i).get(0));
            basicinfo.setName(list.get(i).get(1));
            basicinfo.setSex(list.get(i).get(2));
            basicinfo.setExamnum(list.get(i).get(3));
            basicinfo.setIdnum(list.get(i).get(4));
            basicinfo.setCampus(list.get(i).get(5));
            basicinfo.setCollege(list.get(i).get(6));
            basicinfo.setMajor(list.get(i).get(7));
            basicinfo.setClassnum(list.get(i).get(8));
            basicinfo.setDormnum(list.get(i).get(9));
            basicinfo.setBankcardid(list.get(i).get(10));
            basicinfo.setPaysta(list.get(i).get(11));
            basicinfo.setCollegetel(list.get(i).get(12));
            basicinfo.setSchooltel(list.get(i).get(13));
            basicinfo.setMastername(list.get(i).get(14));
            basicinfo.setMastertel(list.get(i).get(15));
            basicinfo.setBrother(list.get(i).get(16));
            basicinfo.setBrothertel(list.get(i).get(17));
            basicinfo.setBrother(list.get(i).get(18));
            basicinfo.setBrothertel(list.get(i).get(19));


            if (judgeExist.judgeExist(basicinfo.getSno())) {
                basicinfoMapper.updateByPrimaryKeySelective(basicinfo);
                updatenum++;
            } else {
                insertService.insertBasicinfo(basicinfo);
                //导基本数据的时候  需要把stcompleteinfo 表插入这些人的学号
                Stcompleteinfo stcompleteinfo = new Stcompleteinfo();
                stcompleteinfo.setSno(basicinfo.getSno());
                stcompleteinfoMapper.insertSelective(stcompleteinfo);
                addnum++;
            }
            //判断Studentlogin表中是否有要插入的该学生，如果有更新密码为身份证后六位，若没有则插入该学生，密码为身份证后六位
            if (judgeExist.judgeStudentloginExist(basicinfo.getSno())) {
                studentloginMapper.updateByPrimaryKeySelective(studentlogin);
            } else {
                studentloginMapper.insertSelective(studentlogin);

            }
        }
        Map<String, String> map = new HashMap<>();
        String msg = "成功更新" + updatenum + "条," + "成功插入" + addnum + "条";
        map.put("msg", msg);
        clearCaches();
        return map;
    }

    //导入补充信息
    @ResponseBody
    @RequestMapping(value = "/buchong")
    public Map uploadbc(@RequestParam(value = "file", required = false) MultipartFile file, HttpServletRequest request) throws Exception {

        String msg = "";
        int failnum = 0;
        int updatenum = 0;
        Integer all = 0;

        String pa = TemplateUtils.uuid();
        Map<String, String> map = new HashMap<>();
        String path = pa + file.getOriginalFilename();
        Adminlogin adminlogin = (Adminlogin) request.getSession().getAttribute("admindata");
        if (path.indexOf(".xls") <= 0 && path.indexOf(".xlsx") <= 0) {
            logger.info(adminlogin.getAdminname() + ":更新数据失败，文件格式不正确");
            msg = "更新数据失败，请上传正确格式的文件";
        } else {

            InputStream is = file.getInputStream();

            List<List<Object>> lists = JavaPoiUtil.getExcelList(is, path);
            all = lists.size();
            for (int i = 0; i < lists.size(); i++) {
                List<Object> list = lists.get(i);
                Basicinfo basicinfo = new Basicinfo();
                basicinfo.setSno(String.valueOf(list.get(0)));
                basicinfo.setClassnum(String.valueOf(list.get(1)));
                basicinfo.setDormnum(String.valueOf(list.get(2)));
                basicinfo.setMastername(String.valueOf(list.get(3)));
                basicinfo.setMastertel(String.valueOf(list.get(4)));
                basicinfo.setBrother(String.valueOf(list.get(5)));
                basicinfo.setBrothertel(String.valueOf(list.get(6)));
                basicinfo.setHelper(String.valueOf(list.get(7)));
                basicinfo.setHelpertel(String.valueOf(list.get(8)));
                if (judgeExist.judgeExist(basicinfo.getSno())) {
                    basicinfoMapper.updateByPrimaryKeySelective(basicinfo);
                    updatenum++;
                } else {
                    failnum++;
                }

                msg = "成功更新" + updatenum + "条," + "更新失败" + failnum + "条";
            }
        }
        map.put("msg", msg);
        clearCaches();
        return map;

    }

    //导入缴费信息
    @ResponseBody
    @RequestMapping(value = "/paysta")
    public Map uploadPaysta(@RequestParam(value = "file", required = false) MultipartFile file, HttpServletRequest request) throws Exception {
        int failnum = 0;
        int updatenum = 0;
        Integer all = 0;
        String msg = "";
        String pa = TemplateUtils.uuid();
        Map<String, String> map = new HashMap<>();
        String path = pa + file.getOriginalFilename();
        Adminlogin adminlogin = (Adminlogin) request.getSession().getAttribute("admindata");

        if (path.indexOf(".xls") <= 0 && path.indexOf(".xlsx") <= 0) {
            logger.info(adminlogin.getAdminname() + ":更新数据失败，文件格式不正确");
            msg = "更新数据失败，请上传正确格式的文件";
        } else {
            InputStream is = file.getInputStream();

            List<List<Object>> lists = JavaPoiUtil.getExcelList(is, path);
            all = lists.size();

            for (int i = 0; i < lists.size(); i++) {
                List<Object> list = lists.get(i);
                Basicinfo basicinfo = new Basicinfo();
                basicinfo.setSno(String.valueOf(list.get(0)));
                basicinfo.setPaysta(String.valueOf(list.get(1)));

                if (judgeExist.judgeExist(String.valueOf(list.get(0)))) {
                    basicinfoMapper.updateByPrimaryKeySelective(basicinfo);
                    updatenum++;
                } else {
                    failnum++;
                }

            }
            msg = "成功更新" + updatenum + "条," + "更新失败" + failnum + "条";
        }
        map.put("msg", msg);
        clearCaches();
        return map;
    }

    //新导入基础信息
    @RequestMapping(value = "/updateBaseinfoexcel")
    @ResponseBody
    public Map getExcel(@RequestParam("file") MultipartFile file, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setCharacterEncoding("utf-8");
        String msg = "";
        Integer num = 0;
        Integer upnum = 0;
        Integer all = 0;
        String pa = TemplateUtils.uuid();
        Map<String, String> map = new HashMap<>();
        String path = pa + file.getOriginalFilename();
        Adminlogin adminlogin = (Adminlogin) request.getSession().getAttribute("admindata");
        if (path.indexOf(".xls") <= 0 && path.indexOf(".xlsx") <= 0) {
            logger.info(adminlogin.getAdminname() + ":更新数据失败，文件格式不正确");
            msg = "更新数据失败，请上传正确格式的文件";
        } else {
            //判断是否第一次导入，如果第一次导入删除数据库学生表信息

            String isFirstImport = TemplateUtils.getPropertiesdata("other.properties", "isFirstImport");
           boolean isfirst= FileUtil.isExists(isFirstImport);
            if (!isfirst) {
             //   TemplateUtils.writeData("other.properties", "isFirstImport", "false");
                if(FileUtil.createFile(isFirstImport)){
                    System.out.println("创建成功");
                }else {
                    System.out.println("创建失败");
                }
                basicinfoMapper.deleteAll();
                collegeMapper.deleteAll();
                majorMapper.deleteAll();
                stcompleteinfoMapper.delegeAll();
                studentloginMapper.deleteAll();

            }
            InputStream is = file.getInputStream();

            List<List<Object>> lists = JavaPoiUtil.getExcelList(is, path);
            all = lists.size();
            for (int i = 0; i < lists.size(); i++) {
                List<Object> list = lists.get(i);
                Basicinfo basicinfo = new Basicinfo();
                basicinfo.setSno(String.valueOf(list.get(0)));
                basicinfo.setName(String.valueOf(list.get(1)));
                basicinfo.setSex(String.valueOf(list.get(2)));
                basicinfo.setExamnum(String.valueOf(list.get(3)));
                basicinfo.setIdnum(String.valueOf(list.get(4)));
                basicinfo.setCampus(String.valueOf(list.get(5)));
                basicinfo.setCollege(String.valueOf(list.get(6)));
                basicinfo.setMajor(String.valueOf(list.get(7)));
                basicinfo.setClassnum(String.valueOf(list.get(8)));
                basicinfo.setDormnum(String.valueOf(list.get(9)));
                basicinfo.setBankcardid(String.valueOf(list.get(10)));
                basicinfo.setPaysta(String.valueOf(list.get(11)));
                basicinfo.setCollegetel(String.valueOf(list.get(12)));
                basicinfo.setSchooltel(String.valueOf(list.get(13)));
                basicinfo.setMastername(String.valueOf(list.get(14)));
                basicinfo.setMastertel(String.valueOf(list.get(15)));
                basicinfo.setBrother(String.valueOf(list.get(16)));
                basicinfo.setBrothertel(String.valueOf(list.get(17)));
                basicinfo.setHelper(String.valueOf(list.get(18)));
                basicinfo.setHelpertel(String.valueOf(list.get(19)));


                if (basicinfoMapper.selectByPrimaryKey(String.valueOf(list.get(0))) == null) {
                    College college = collegeMapper.selectByName(basicinfo.getCollege());//如果学院不存在  插入学院
                    if (college == null) {
                        college = new College();
                        college.setCollegename(basicinfo.getCollege());
                        collegeMapper.insert(college);
                    }
                    college = collegeMapper.selectByName(basicinfo.getCollege());
                    Major major = majorMapper.selectByCollegeIdAndMajorName(college.getCollegeid(), basicinfo.getMajor());//专业不存在插入专业
                    if (major == null) {
                        major = new Major();
                        major.setCollegeid(college.getCollegeid());
                        major.setMajorname(basicinfo.getMajor());
                        majorMapper.insert(major);
                    }
                    basicinfoMapper.insert(basicinfo);
                    num++;
                } else {
                    basicinfoMapper.updateByPrimaryKeySelective(basicinfo);
                    upnum++;
                }

                Stcompleteinfo stcompleteinfo = new Stcompleteinfo();
                stcompleteinfo.setSno(basicinfo.getSno());
                Stcompleteinfo stcompleteinfosql = stcompleteinfoMapper.selectByPrimaryKey(stcompleteinfo.getSno());

                if (stcompleteinfosql == null) {
                    stcompleteinfoMapper.insertSelective(stcompleteinfo);
                }

                String password = getPasswordService.getPassword(basicinfo.getIdnum());
                Studentlogin studentlogin = new Studentlogin();//在studentlogin表里设置学生密码为身份证后六位
                studentlogin.setSno(basicinfo.getSno());
                studentlogin.setPassword(password);
                //判断Studentlogin表中是否有要插入的该学生，如果有更新密码为身份证后六位，若没有则插入该学生，密码为身份证后六位
                if (judgeExist.judgeStudentloginExist(basicinfo.getSno())) {
                    studentloginMapper.updateByPrimaryKeySelective(studentlogin);
                } else {
                    studentloginMapper.insertSelective(studentlogin);
                }

            }
            int fail = all - upnum - num;
            if (fail < 0) {
                fail = 0;
            }


            logger.info(adminlogin.getAdminname() + " :成功更新" + upnum + "条," + "成功插入" + num + "条" + " 失败：" + fail + "条");
            msg = "成功更新" + upnum + "条," + "成功插入" + num + "条" + " 失败：" + fail + "条";

        }


        map.put("msg", msg);
        clearCaches();
        return map;
    }


    @RequestMapping("/getbaodaostudent")
    @ResponseBody
    public void getdatas(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.reset();
        response.setContentType("application/msexcel");
        response.setHeader("Content-Disposition", "attachment;filename=" + JavaPoiUtil.getExcelName("导出数据", 2007));


        BasicinfoExample basicinfoExample = new BasicinfoExample();
        BasicinfoExample.Criteria criteria = basicinfoExample.createCriteria();
        criteria.andSpotregisterEqualTo("已报到");


        String[] keys = {"sno", "name"};//要写出的字段名
        String columnNames[] = {"学号", "姓名"};//列名字

        List<Basicinfo> basicinfoics = basicinfoMapper.selectByExample(basicinfoExample);

        System.out.println(basicinfoics.size());
        OutputStream outputStream = response.getOutputStream();
        JavaPoiUtil.createWorkBooks(Basicinfo.class, basicinfoics, keys, columnNames, 2007).write(outputStream);
    }
}



