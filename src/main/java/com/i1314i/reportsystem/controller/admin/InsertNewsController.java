package com.i1314i.reportsystem.controller.admin;

import com.i1314i.reportsystem.mapper.NewsMapper;
import com.i1314i.reportsystem.po.News;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/school")
public class InsertNewsController {
    @Autowired
    NewsMapper newsMapper;
    @ResponseBody
    @RequestMapping("/insertnewsctl")
    public Map<String,String> insertNews(HttpServletRequest request){
        String title=request.getParameter("title");
        String content=request.getParameter("content");
        String url=request.getParameter("url");
        String description=request.getParameter("description");
        Map<String,String> map=new HashMap<>();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        News news=new News();
        news.setTitle(title);
        news.setContent(content);
        news.setUrl(url);
        news.setDescription(description);
        news.setInputtime(df.format(new Date()));
        news.setType("1");
        String msg=null;
        try {
            newsMapper.insertSelective(news);
            msg="插入成功";
        }
        catch (Exception e){
            msg="插入失败，请重新输入";
        }

        map.put("msg",msg);
        return map;
    }

    //前端跳到该方法，查询到最后一条信息后放入request在跳到更新信息页面
    @RequestMapping("/getfinal")
    public String getFinalNews(HttpServletRequest request){
        News news=newsMapper.seletctByInsertTime("1");
        if(news==null){
            news=new News();
        }
        request.setAttribute("news",news);
        return "page/admin/insertnews";
    }
}
