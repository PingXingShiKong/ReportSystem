package com.i1314i.reportsystem.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/school")
public class QueryMajorController {
    @RequestMapping("/querymajor")
    public String QueryMajor(HttpServletRequest request, HttpSession session) {
        int  type=Integer.parseInt(request.getParameter("type"));
        String collegeid = request.getParameter("collegeid");
        session.setAttribute("collegeid", collegeid);
        if (type==1){
            return "/xueyuan/baodaolv.html";
        }
        else {
            return "/weixin/collegeRate";
        }

    }
}
