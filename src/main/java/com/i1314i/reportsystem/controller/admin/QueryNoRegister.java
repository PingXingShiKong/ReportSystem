package com.i1314i.reportsystem.controller.admin;

import com.i1314i.reportsystem.mapper.BasicinfoMapper;
import com.i1314i.reportsystem.po.Basicinfo;
import com.i1314i.reportsystem.po.Basicinfolayui;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/school")
public class QueryNoRegister {
    @Autowired
    BasicinfoMapper basicinfoMapper;

    @ResponseBody
    @RequestMapping("/getnoregister")
    public List<Basicinfo> getnoregister() {
//        Integer limit1=(page-1)*10;
//        Integer limit2=limit;
        List<Basicinfo> basicinfos = basicinfoMapper.getNoRegister();
        return basicinfos;
    }

    @ResponseBody
    @RequestMapping("/addreason")
    public String getInformation(HttpServletRequest request) {

        String id = request.getParameter("sno");
        String context = request.getParameter("context");
        String result;
        Basicinfo basicinfo = basicinfoMapper.selectByPrimaryKey(id);
        if(basicinfo==null){
            result="未找到该学生";
            return result;
        }
        basicinfo.setReason(context);
        int judge=basicinfoMapper.updateByPrimaryKeySelective(basicinfo);
        if(judge>0){
            result="备注成功";
        }
        else {
            result="备注失败";
        }
        return result;
    }

    //获得未报到新生、分页
    @ResponseBody
    @RequestMapping("/getnoregisterlimit")
    public Basicinfolayui getnoregisterlimit(HttpServletRequest request){
        Integer limit= Integer.valueOf(request.getParameter("limit"));
        Integer page=Integer.valueOf(request.getParameter("page"));
        Integer limit1=10 * (page - 1);
        List<Basicinfo> basicinfoList=basicinfoMapper.getNoRegisterLimit(limit1,limit);
        Basicinfolayui basicinfolayui=new Basicinfolayui();
        basicinfolayui.setCount(basicinfoMapper.selectCount());
        basicinfolayui.setCode(0);
        basicinfolayui.setMsg("");
        basicinfolayui.setData(basicinfoList);
        return basicinfolayui;

    }
}