package com.i1314i.reportsystem.controller.admin;

import com.i1314i.reportsystem.mapper.BasicinfoMapper;
import com.i1314i.reportsystem.mapper.StudentloginMapper;
import com.i1314i.reportsystem.po.Basicinfo;
import com.i1314i.reportsystem.po.Studentlogin;
import com.i1314i.reportsystem.service.admin.GetPasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RequestMapping("/school")
@RestController
public class ResetStudentPwdController {
    @Autowired
    BasicinfoMapper basicinfoMapper;
    @Autowired
    StudentloginMapper studentloginMapper;
    @Autowired
    GetPasswordService getPasswordService;
    //重置学生密码
    @RequestMapping("/resetpwdctl")
    public String resetStudentPasswork(HttpServletRequest request){
        String sno=request.getParameter("sno");
        String result;
        Basicinfo basicinfo=basicinfoMapper.selectByPrimaryKey(sno);
        if (basicinfo==null){
            result="未找到该学生";
            return result;
        }
        try{
            String idnum=basicinfo.getIdnum();
            Studentlogin studentlogin=new Studentlogin();
            studentlogin.setSno(sno);
            studentlogin.setPassword(getPasswordService.getPassword(idnum));

            int temp=studentloginMapper.updateByPrimaryKeySelective(studentlogin);


            if (temp>0){
                result="重置成功";
            }
            else {
                result="重置失败";
            }
        }catch (Exception e){
            result="重置失败";
        }
        return result;
    }
}
