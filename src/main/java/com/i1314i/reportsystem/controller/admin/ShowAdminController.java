package com.i1314i.reportsystem.controller.admin;

import com.i1314i.reportsystem.mapper.AdminloginMapper;
import com.i1314i.reportsystem.mapper.CollegeMapper;
import com.i1314i.reportsystem.po.Adminlogin;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * 展示学院管理员、修改管理员密码、删除管理员
 */
@Controller
@RequestMapping("/school")
public class ShowAdminController {
    @Autowired
    AdminloginMapper adminloginMapper;
    @Autowired
    CollegeMapper collegeMapper;

    @RequestMapping("/showadmin")
    public String showAdmin(HttpServletRequest request) {
        List<Adminlogin> adminlogins = adminloginMapper.selectAll();
        for (Adminlogin temp : adminlogins) {
            temp.setCollege(collegeMapper.selectByPrimaryKey(Integer.valueOf(temp.getCollege())).getCollegename());//数据库adminlogin和college表对应
        }
        request.setAttribute("admins", adminlogins);
        return "/page/admin/showadmin.html";
    }

    //修改学院管理员
    @ResponseBody
    @RequestMapping("/updateadminpassword")
    public String updataAdminPassword(@Param("adminname") String adminname, @Param("password") String password) {
        String result;
        Adminlogin adminlogin = adminloginMapper.selectByadminname(adminname);
        if (adminlogin == null)
            result = "未找到您输入的管理员，请重新输入";
        else {
            adminloginMapper.updatePasswordByadminname(adminname, password);
            result = "修改成功";
        }
        return result;
    }
    //删除管理员
    @ResponseBody
    @RequestMapping("/deleteadminpassword")
    public String deleteAdmin(@Param("adminname") String adminname) {
        String result;
        Adminlogin adminlogin = adminloginMapper.selectByadminname(adminname);
        if (adminlogin == null)
            result = "未找到您输入的管理员，请重新输入";
        else {
            adminloginMapper.deleteByAdminnam(adminname);
            result = "删除成功<br/>1秒钟后自动跳转 <script> function jumpurl(){ location='/school/showadmin'; } setTimeout('jumpurl()',1000);  </script>";
        }
        return result;
    }

    @RequestMapping("/update")
    public String updatepass(HttpServletRequest request) {
        request.setAttribute("adminname", request.getParameter("adminname"));
        return "/page/admin/updatepassword.html";
    }
    @RequestMapping("/delete")
    public String deleteadmin(HttpServletRequest request){
        request.setAttribute("adminname",request.getParameter("adminname"));
        return "/page/admin/deleteadmin.html";
    }

    //修改总管理员
    @ResponseBody
    @RequestMapping("/changepwd")
    public String changePwd(HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException {
        request.setCharacterEncoding("UTF-8");
        Adminlogin adminlogin = (Adminlogin) session.getAttribute("admindata");
        String password = request.getParameter("password");
        int i=0;
        if(password!=null&&!password.trim().equals("")){
            adminlogin.setPassword(password);
            i=adminloginMapper.updateByPrimaryKey(adminlogin);
        }

        String msg;

        if(i>0){
            msg = "修改成功<br/>1秒钟后自动跳转 <script> function jumpurl(){ location='/school/changepassword'; } setTimeout('jumpurl()',1000);  </script>";
        }else {
            msg = "修改失败<br/>1秒钟后自动跳转 <script> function jumpurl(){ location='/school/changepassword'; } setTimeout('jumpurl()',1000);  </script>";
        }

        return msg;
    }
}