package com.i1314i.reportsystem.controller.admin;

import com.i1314i.reportsystem.mapper.ReportsRateMapper;
import com.i1314i.reportsystem.po.ReportsRate;
import com.i1314i.reportsystem.service.admin.GetCollegeReportsRate;
import com.i1314i.reportsystem.service.exception.AdminException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 查看学院报到率Controller
 */
@Controller
@RequestMapping("/school")
public class ShowCollegeReportsRate {
    @Autowired
    GetCollegeReportsRate getCollegeReportsRate;

    @RequestMapping("/collegereportsrate")
    public String showCollegeReportsRate(HttpServletRequest request) throws AdminException {
        List<ReportsRate> reportsRateList = getCollegeReportsRate.getCollegeReportsRateList();
        request.setAttribute("collegeList", reportsRateList);
        return "page/admin/showrate.html";

    }
}
