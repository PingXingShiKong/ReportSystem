package com.i1314i.reportsystem.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/school")
public class TranspondController {
    @RequestMapping("/transpondshow")
    public String transpondShow(){
        return "page/admin/showstudent";
    }
    @RequestMapping("/transpondnoregister")
    public String transpondNoregister(){
        return "page/admin/noregister";
    }
    @RequestMapping("/transpondfile")
    public String transpondFile(){
        return "page/admin/file";
    }

    @RequestMapping("/transpondaddadmin")
    public String transpondAddadmin(){
        return "page/admin/addadministrator";
    }
    @RequestMapping("/transpondimport")
    public String transpondImport(){
        return "page/admin/import";
    }
    @RequestMapping("/index")
    public String transpondIndex(){
        return "index";
    }
    @RequestMapping("/changepassword")
    public String transpondChangePwd(){
        return "page/user/changePwd";
    }

    @RequestMapping(value = "/unselect")
    public String unselects(){
        return "xueyuan/unselect";
    }
    @RequestMapping("/innews")
    public String transpondInsertnews(){
        return "page/admin/insertnews";
    }
    @RequestMapping("/resetpassword")
    public String transpondResetpassword(){
        return "/page/admin/resetpwd";
    }
    @RequestMapping("/transpondremarkreson")
    public String transpondRemarkReason(){
        return "/page/admin/remarkreason";
    }


}
