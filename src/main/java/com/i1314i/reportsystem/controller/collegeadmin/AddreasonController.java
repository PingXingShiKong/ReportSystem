package com.i1314i.reportsystem.controller.collegeadmin;

import com.i1314i.reportsystem.mapper.AdminOtherMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
@Controller
@RequestMapping("/xueyuan")
public class AddreasonController {
    @Autowired
    AdminOtherMapper adminOtherMapper;

    @RequestMapping("/addreason")
    public String getInformation(HttpServletRequest request, Model model) {

        String id = request.getParameter("id");
        String context = request.getParameter("context");
        String f = request.getParameter("f");
        Map p = new HashMap();
        p.put("context", context);
        p.put("id", id);
        int x = adminOtherMapper.updatereason(p);
        if (f != null) {
            if (f.equals("1")) {
                model.addAttribute("sno", id);
                return "xueyuan/unselect";
            }
        }
        return "xueyuan/unreported";
    }
}