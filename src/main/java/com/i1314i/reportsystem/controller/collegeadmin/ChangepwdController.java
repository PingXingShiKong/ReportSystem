package com.i1314i.reportsystem.controller.collegeadmin;

import com.i1314i.reportsystem.mapper.AdminOtherMapper;
import com.i1314i.reportsystem.po.Adminlogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/xueyuan")
public class ChangepwdController {
    @Autowired
    AdminOtherMapper adminOtherMapper;

    @RequestMapping("/pwdchange")
    public String getInformation(HttpServletRequest request, HttpSession session) {
        Adminlogin adminlogin= (Adminlogin) session.getAttribute("admindata");
        String name="";
        if(adminlogin.getType()==2){
             name=adminlogin.getAdminname();
        }

        String np= request.getParameter("np");
        Map p = new HashMap();
        p.put("newpwd", np);
        p.put("name", name);
        int x = adminOtherMapper.updatePwd(p);

       // return "login";
        return "xueyuan/changePwd";
    }
}

