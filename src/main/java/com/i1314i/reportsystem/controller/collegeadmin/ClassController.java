package com.i1314i.reportsystem.controller.collegeadmin;

import com.i1314i.reportsystem.mapper.AdminOtherMapper;
import com.i1314i.reportsystem.po.vo.ClassRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/colandsch")
public class ClassController {
    @Autowired
    AdminOtherMapper adminOtherMapper;

    @RequestMapping("/bjbaodaolv")
    public List<ClassRegister> getInformation(HttpServletRequest request, HttpSession session) {
        String id = request.getParameter("id");
        List<ClassRegister> classRegisters = adminOtherMapper.getclassnum(id);

        for(int i=0;i<classRegisters.size();i++) {
            Map p=new HashMap();
            p.put("classnum",classRegisters.get(i).getClassNum());
            p.put("majorid",id);
            Integer x = adminOtherMapper.getheadcount(p);
            classRegisters.get(i).setHeadcount(x);
            Integer y = adminOtherMapper.getclassregister(p);
            classRegisters.get(i).setBdcount(y);
            double s=(double)y/x;
            s=(double)((int)(s*10000))/100;
            classRegisters.get(i).setRegister(s);
        }
        return classRegisters;
    }
}