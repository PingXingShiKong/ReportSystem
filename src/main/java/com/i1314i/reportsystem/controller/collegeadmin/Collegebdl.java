package com.i1314i.reportsystem.controller.collegeadmin;

import com.i1314i.reportsystem.mapper.AdminOtherMapper;
import com.i1314i.reportsystem.po.Adminlogin;
import com.i1314i.reportsystem.po.Major;
import com.i1314i.reportsystem.po.vo.CollegeRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/colandsch")
public class Collegebdl {
    @Autowired
    AdminOtherMapper adminOtherMapper;
    @RequestMapping("/baodaolv")
    public List<CollegeRegister> getInformation( HttpSession session) {
        String collegeid=null;
        collegeid=(String)session.getAttribute("collegeid");

        if(collegeid==""||collegeid==null) {
            Adminlogin adminlogin= (Adminlogin) session.getAttribute("admindata");
            if(adminlogin.getType()==2){
                collegeid=adminlogin.getCollege();
            }
        }

        List<CollegeRegister>collegeRegisters=new ArrayList<>();

        List<Major> majors=adminOtherMapper.getmajorbycollege(Integer.parseInt(collegeid));
        for(int i=0;i<majors.size();i++){
            CollegeRegister collegeRegister=new CollegeRegister();
            collegeRegister.setMajorid(majors.get(i).getMajorid());
            collegeRegister.setMajorname(majors.get(i).getMajorname());
            Integer x=adminOtherMapper.getnumsbymajor(collegeRegister.getMajorname());
            Integer y=adminOtherMapper.spotRegisternum(collegeRegister.getMajorname());
            collegeRegister.setBdcount(y);
            collegeRegister.setHeadcount(x);
            double s=(double)y/x;
            s=(double)((int)(s*10000))/100;
            collegeRegister.setRegister(s);
            collegeRegisters.add(collegeRegister);
        }
        return collegeRegisters;
    }
}