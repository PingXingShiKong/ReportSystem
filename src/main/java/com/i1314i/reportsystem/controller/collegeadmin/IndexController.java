package com.i1314i.reportsystem.controller.collegeadmin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping(value = "/xueyuan")
public class IndexController {

@RequestMapping("/collegeindex")
    public String index() {
        return "collegeindex";
    }
    @RequestMapping("/main")
    public String mains(){
        return "xueyuan/main";
    }

    @RequestMapping(value = "/selectcolleges")
    public String selectcollege(){
        return "xueyuan/selectcollege";
    }
    @RequestMapping(value = "/unreporteds")
    public  String unreported(){
        return "xueyuan/unreported";
    }
    @RequestMapping(value = "/baodaolvs")
    public String baodaolv(){
        return "xueyuan/baodaolv";
    }
    @RequestMapping(value = "/unselects")
    public String unselects(){
        return "xueyuan/unselect";
    }
    @RequestMapping(value = "/pwdchanges")
    public String pwdchange(){
        return "xueyuan/changePwd";
    }


}
