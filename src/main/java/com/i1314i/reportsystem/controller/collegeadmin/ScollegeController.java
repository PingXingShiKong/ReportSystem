package com.i1314i.reportsystem.controller.collegeadmin;

        import com.i1314i.reportsystem.mapper.BasicinfoMapper;
        import com.i1314i.reportsystem.po.Adminlogin;
        import com.i1314i.reportsystem.po.Basicinfo;
        import com.i1314i.reportsystem.po.Basicinfolayui;
        import org.apache.ibatis.annotations.Param;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Controller;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RequestParam;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.web.bind.annotation.RestController;

        import javax.servlet.http.HttpServletRequest;
        import javax.servlet.http.HttpSession;
        import java.util.ArrayList;
        import java.util.List;

/**
 * 查询学生信息Controller
 */
@Controller
@RequestMapping("/xueyuan")
public class ScollegeController {
    @Autowired
    BasicinfoMapper basicinfoMapper;

    Basicinfolayui basicinfolayui=new Basicinfolayui();

    @ResponseBody
    @RequestMapping("/all")
    public Basicinfolayui getInformationAll(HttpSession session,HttpServletRequest request, @Param("limit") Integer limit, @Param("page") Integer page) {
        Adminlogin adminlogin= (Adminlogin) session.getAttribute("admindata");
        String collegeid="";
        if(adminlogin.getType()==2){
             collegeid=adminlogin.getCollege();
        }
        String type = request.getParameter("type");
        String value = request.getParameter("value");
        if (type == null) {
            int pianyi = 10 * (page - 1);
            List<Basicinfo> basicinfoList = basicinfoMapper.selectcollegeLimit(Integer.valueOf(collegeid),pianyi, limit);
            basicinfolayui.setCode(0);
            basicinfolayui.setCount(basicinfoMapper.selectcollegeCount(Integer.valueOf(collegeid)));
            basicinfolayui.setMsg("");
            basicinfolayui.setData(basicinfoList);
        } else {
            if (type.equals("name")) {
                List<Basicinfo> basicinfoList = basicinfoMapper.selectcollegeName(Integer.valueOf(collegeid),value);
                basicinfolayui.setCode(0);
                basicinfolayui.setCount(basicinfoList.size());
                basicinfolayui.setMsg("");
                basicinfolayui.setData(basicinfoList);
            }
            else if (type.equals("sno")) {
                List<Basicinfo> basicinfoList = new ArrayList<>();
                Basicinfo basicinfo = basicinfoMapper.selectByPrimaryKey(value);
                basicinfoList.add(basicinfo);
                basicinfolayui.setCode(0);
                basicinfolayui.setCount(basicinfoList.size());
                basicinfolayui.setMsg("");
                basicinfolayui.setData(basicinfoList);
            }
            else if (type.equals("examnum")) {
                List<Basicinfo> basicinfoList = basicinfoMapper.selectcollegeExamnum(Integer.valueOf(collegeid),value);
                basicinfolayui.setCode(0);
                basicinfolayui.setCount(basicinfoList.size());
                basicinfolayui.setMsg("");
                basicinfolayui.setData(basicinfoList);
            }

        }
        return basicinfolayui;
    }
}

