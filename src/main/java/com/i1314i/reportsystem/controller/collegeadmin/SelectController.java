package com.i1314i.reportsystem.controller.collegeadmin;

import com.i1314i.reportsystem.mapper.AdminOtherMapper;
import com.i1314i.reportsystem.po.Adminlogin;
import com.i1314i.reportsystem.po.Basicinfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/xueyuan")
public class SelectController {
    @Autowired
    AdminOtherMapper adminOtherMapper;

    @RequestMapping("/unselect")
    public List<Basicinfo> getInformation(HttpServletRequest request, HttpSession session, Model model) {

        String ss=request.getParameter("sno");
        List<Basicinfo> list= adminOtherMapper.getUnselect(ss);
        return list;
    }
}

