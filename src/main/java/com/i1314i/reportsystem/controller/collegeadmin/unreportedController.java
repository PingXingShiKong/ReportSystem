package com.i1314i.reportsystem.controller.collegeadmin;

import com.i1314i.reportsystem.mapper.AdminOtherMapper;
import com.i1314i.reportsystem.po.Adminlogin;
import com.i1314i.reportsystem.po.Basicinfo;
import com.i1314i.reportsystem.utils.TemplateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/colandsch")
public class unreportedController {
    @Autowired
    AdminOtherMapper adminOtherMapper;

    @RequestMapping("/unreported")
    public List<Basicinfo> getInformation(HttpSession session, HttpServletRequest request) {
        System.out.println("unreported----");
        String collegeid="";
        Adminlogin adminlogin= (Adminlogin) session.getAttribute("admindata");
        if(TemplateUtils.isWeChat(request)){
            collegeid=adminlogin.getCollege();
            System.out.println("---------weixin-------" + collegeid);
        }else{
            System.out.println("------college--");
            if(adminlogin.getType()==2) {
                System.out.println("---------college------2---");
                collegeid = adminlogin.getCollege();
            }
        }

        List<Basicinfo> list= adminOtherMapper.getUnreported(Integer.parseInt(collegeid));
        return list;
    }
}