package com.i1314i.reportsystem.controller.problem;

import com.i1314i.reportsystem.po.JsonResult;
import com.i1314i.reportsystem.po.Problem;
import com.i1314i.reportsystem.po.pageHelper.PageBean;
import com.i1314i.reportsystem.service.ProblemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author 平行时空
 * @created 2019-04-28 17:38
 **/
@RestController
public class ProblemMangerController {
    @Autowired
    private ProblemService problemService;



    @RequestMapping(value = "/getAllexams",method = {RequestMethod.POST,RequestMethod.GET},produces="application/json;charset=utf-8;")
    public JsonResult getAllProblems(HttpServletRequest request, @RequestBody PageBean pageBean)throws  Exception{



        JsonResult jsonResult=JsonResult.createJsonResult();
        jsonResult.setData(problemService.getAllProblems(pageBean));


        return jsonResult;
    }


}
