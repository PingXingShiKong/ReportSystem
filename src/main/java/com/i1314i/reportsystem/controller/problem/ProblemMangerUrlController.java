package com.i1314i.reportsystem.controller.problem;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 平行时空
 * @created 2019-05-12 17:40
 **/
@Controller
public class ProblemMangerUrlController {
    @RequestMapping(value = "/problemList")
    public String problemList(HttpServletRequest request){
        return "problem/problemList";
    }
}
