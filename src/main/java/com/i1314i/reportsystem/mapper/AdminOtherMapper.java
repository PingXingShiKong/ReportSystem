package com.i1314i.reportsystem.mapper;


import com.i1314i.reportsystem.po.Basicinfo;
import com.i1314i.reportsystem.po.Major;
import com.i1314i.reportsystem.po.vo.ClassRegister;
import com.i1314i.reportsystem.po.vo.CollegeRegister;

import java.util.List;
import java.util.Map;

public interface AdminOtherMapper {
    List<Major> getmajorbycollege(Integer collageid);
    Integer getnumsbymajor(String majorid);
    Integer spotRegisternum(String majorname);
    List<ClassRegister> getclassnum(String majorname);
    Integer getheadcount(Map p);
    Integer getclassregister(Map p);
    List<Basicinfo> getUnreported(Integer collageid);
    List<Basicinfo> getUnselect(String sno);
    int updatePwd(Map p);
    int  updatereason(Map p);
}
