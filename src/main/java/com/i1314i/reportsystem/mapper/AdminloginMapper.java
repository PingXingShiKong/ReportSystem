package com.i1314i.reportsystem.mapper;

import com.i1314i.reportsystem.po.Adminlogin;
import com.i1314i.reportsystem.po.AdminloginExample;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface AdminloginMapper {
    long countByExample(AdminloginExample example);

    int deleteByExample(AdminloginExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Adminlogin record);

    int insertSelective(Adminlogin record);

    List<Adminlogin> selectByExample(AdminloginExample example);

    Adminlogin selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Adminlogin record, @Param("example") AdminloginExample example);

    int updateByExample(@Param("record") Adminlogin record, @Param("example") AdminloginExample example);

    int updateByPrimaryKeySelective(Adminlogin record);

    int updateByPrimaryKey(Adminlogin record);
    @Select("select * from adminlogin where adminname=#{name}")
    Adminlogin selectByadminname(@Param("name") String name);

    @Select("select * from adminlogin")
    List<Adminlogin> selectAll();

    @Update("update adminlogin set password=#{password} where adminname=#{adminname}")
    int updatePasswordByadminname(@Param("adminname")String adminnane,@Param("password")String password);

    @Delete("delete from adminlogin where adminname=#{adminname}")
    int deleteByAdminnam(@Param("adminname")String adminname);

}