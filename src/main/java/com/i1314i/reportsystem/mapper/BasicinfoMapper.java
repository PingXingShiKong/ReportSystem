package com.i1314i.reportsystem.mapper;

import com.i1314i.reportsystem.po.Basicinfo;
import com.i1314i.reportsystem.po.BasicinfoExample;
import java.util.List;

import com.i1314i.reportsystem.po.Major;
import com.i1314i.reportsystem.po.vo.CollegeRegister;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

@Component
public interface BasicinfoMapper {
    long countByExample(BasicinfoExample example);

    int deleteByExample(BasicinfoExample example);

    int deleteByPrimaryKey(String sno);

    int insert(Basicinfo record);

    int insertSelective(Basicinfo record);

    List<Basicinfo> selectByExample(BasicinfoExample example);

    Basicinfo selectByPrimaryKey(String sno);

    int updateByExampleSelective(@Param("record") Basicinfo record, @Param("example") BasicinfoExample example);

    int updateByExample(@Param("record") Basicinfo record, @Param("example") BasicinfoExample example);

    int updateByPrimaryKeySelective(Basicinfo record);

    int updateByPrimaryKey(Basicinfo record);
    @Select("select * from basicinfo where name=#{name}")
    List<Basicinfo> selectByName(String name);

    @Select("select * from basicinfo where IDNum=#{IDNum}")
    List<Basicinfo> selectByIdnum(String IDNum);

    @Select("select * from basicinfo where schoolTel=#{schoolTel}")
    Basicinfo selectBySchooltel(Integer schoolTel);

    @Select("select * from basicinfo")
    List<Basicinfo> selectAll();

    @Select("select * from basicinfo limit #{limit1},#{limit2}")
    List<Basicinfo> selectAllLimit(@Param("limit1") Integer limit1, @Param("limit2") Integer limit2);


    @Select("select * from basicinfo where examNum=#{examNum}")
    List<Basicinfo> selectByExamnum(@Param("examNum") String examNum);


    @Select("select count(*) from basicinfo")
    int selectCount();

    @Select("select * from basicinfo where spotRegister!='已报到'")
    List<Basicinfo> getNoRegister( );


    @Select("select * from college c, basicinfo b where c.collegeid=#{cc} and c.collegename=b.college and b.name=#{name}")
    List<Basicinfo> selectcollegeName(@Param("cc")Integer cc,@Param("name")String name);



    @Select("select * from college c, basicinfo b where c.collegeid=#{cc} and c.collegename=b.college limit #{limit1},#{limit2} ")
    List<Basicinfo> selectcollegeLimit(@Param("cc")Integer cc, @Param("limit1") Integer limit1, @Param("limit2") Integer limit2);


    @Select("select * from college c, basicinfo b where c.collegeid=#{cc} and c.collegename=b.college and b.examNum=#{examNum}")
    List<Basicinfo> selectcollegeExamnum(@Param("cc")Integer cc,@Param("examNum") String examNum);



    @Select("select count(*) from college c, basicinfo b where c.collegeid=#{cc} and c.collegename=b.college ")
    int selectcollegeCount(@Param("cc")Integer cc);

    @Select("select * from basicinfo where spotRegister!='已报到' limit #{limit1},#{limit2}")
    List<Basicinfo> getNoRegisterLimit(@Param("limit1")Integer limit1,@Param("limit2")Integer limit2);

    @Select("select count(*) from basicinfo where spotregister != '已报到' or spotregister is null or spotregister = '' ")
    int schoolNOReport();

    @Select("select count(*) from basicinfo where spotregister = '已报到' ")
    int schoolISReport();

    @Select("select count(*) from basicinfo where (spotregister != '已报到' or spotregister is null or spotregister = '') and campus = '北校区' ")
    int northlNOReport();

    @Select("select count(*) from basicinfo where spotregister = '已报到' and campus = '北校区' ")
    int northISReport();

    @Select("select count(*) from basicinfo where (spotregister != '已报到' or spotregister is null or spotregister = '') and campus = '东校区' ")
    int eastNOReport();

    @Select("select count(*) from basicinfo where spotregister = '已报到' and campus = '东校区' ")
    int eastISReport();

    @Select("select count(*) from basicinfo where (spotregister != '已报到' or spotregister is null or spotregister = '') and campus = '南校区' ")
    int southNOReport();

    @Select("select count(*) from basicinfo where spotregister = '已报到' and campus = '南校区'")
    int southISReport();

    void deleteAll();
}