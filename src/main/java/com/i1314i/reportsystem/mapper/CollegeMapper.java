package com.i1314i.reportsystem.mapper;

import com.i1314i.reportsystem.po.College;
import com.i1314i.reportsystem.po.CollegeExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

@Component
public interface CollegeMapper {
    long countByExample(CollegeExample example);

    int deleteByExample(CollegeExample example);

    int deleteByPrimaryKey(Integer collegeid);

    int insert(College record);

    int insertSelective(College record);

    List<College> selectByExample(CollegeExample example);

    College selectByPrimaryKey(Integer collegeid);

    int updateByExampleSelective(@Param("record") College record, @Param("example") CollegeExample example);

    int updateByExample(@Param("record") College record, @Param("example") CollegeExample example);

    int updateByPrimaryKeySelective(College record);

    int updateByPrimaryKey(College record);

    @Select("select collegename from college where collegeid=#{id}")
    String selectCollegeByCollegeId(Integer id);

    List<College>selectAllColleges();

    College selectByName(String collegeName);

    void deleteAll();
}