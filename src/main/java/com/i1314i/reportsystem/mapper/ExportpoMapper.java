package com.i1314i.reportsystem.mapper;

import com.i1314i.reportsystem.service.admin.Exportpo;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ExportpoMapper {
    public List<Exportpo> selectAll();
}
