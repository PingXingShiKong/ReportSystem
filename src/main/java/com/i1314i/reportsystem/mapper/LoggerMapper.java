package com.i1314i.reportsystem.mapper;

import com.i1314i.reportsystem.po.Logger;
import com.i1314i.reportsystem.po.LoggerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LoggerMapper {
    long countByExample(LoggerExample example);

    int deleteByExample(LoggerExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Logger record);

    int insertSelective(Logger record);

    List<Logger> selectByExampleWithBLOBs(LoggerExample example);

    List<Logger> selectByExample(LoggerExample example);

    Logger selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Logger record, @Param("example") LoggerExample example);

    int updateByExampleWithBLOBs(@Param("record") Logger record, @Param("example") LoggerExample example);

    int updateByExample(@Param("record") Logger record, @Param("example") LoggerExample example);

    int updateByPrimaryKeySelective(Logger record);

    int updateByPrimaryKeyWithBLOBs(Logger record);

    int updateByPrimaryKey(Logger record);
}