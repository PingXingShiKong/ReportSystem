package com.i1314i.reportsystem.mapper;

import com.i1314i.reportsystem.po.Major;
import com.i1314i.reportsystem.po.MajorExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MajorMapper {
    long countByExample(MajorExample example);

    int deleteByExample(MajorExample example);

    int deleteByPrimaryKey(Integer majorid);

    int insert(Major record);

    int insertSelective(Major record);

    List<Major> selectByExample(MajorExample example);

    Major selectByPrimaryKey(Integer majorid);

    Major selectByCollegeid(String collegeid);

    int updateByExampleSelective(@Param("record") Major record, @Param("example") MajorExample example);

    int updateByExample(@Param("record") Major record, @Param("example") MajorExample example);

    int updateByPrimaryKeySelective(Major record);

    int updateByPrimaryKey(Major record);

    Major selectByCollegeIdAndMajorName(@Param("collegeid") Integer collegeid, @Param("major")String major);

    void deleteAll();
}