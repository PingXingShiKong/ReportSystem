package com.i1314i.reportsystem.mapper;

import com.i1314i.reportsystem.po.News;
import com.i1314i.reportsystem.po.NewsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

@Component
public interface NewsMapper {
    long countByExample(NewsExample example);

    int deleteByExample(NewsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(News record);

    int insertSelective(News record);

    List<News> selectByExampleWithBLOBs(NewsExample example);

    List<News> selectByExample(NewsExample example);

    News selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") News record, @Param("example") NewsExample example);

    int updateByExampleWithBLOBs(@Param("record") News record, @Param("example") NewsExample example);

    int updateByExample(@Param("record") News record, @Param("example") NewsExample example);

    int updateByPrimaryKeySelective(News record);

    int updateByPrimaryKeyWithBLOBs(News record);

    int updateByPrimaryKey(News record);

    @Select("SELECT * FROM news WHERE TYPE=#{type} ORDER BY id desc LIMIT 0,1")
    News seletctByInsertTime(@Param("type") String type);

    @Select("SELECT * FROM  news WHERE TYPE=#{type} ORDER BY id desc LIMIT 0,1")
    News selectPublicInfo(@Param("type") String type);
}