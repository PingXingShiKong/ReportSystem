package com.i1314i.reportsystem.mapper;

import com.github.pagehelper.PageHelper;
import com.i1314i.reportsystem.po.Problem;
import com.i1314i.reportsystem.po.ProblemExample;
import java.util.List;

import com.i1314i.reportsystem.po.pageHelper.PageBean;
import org.apache.ibatis.annotations.Param;

public interface ProblemMapper {
    long countByExample(ProblemExample example);

    int deleteByExample(ProblemExample example);

    int deleteByPrimaryKey(Integer problemid);

    int insert(Problem record);

    int insertSelective(Problem record);

    List<Problem> selectByExampleWithBLOBs(ProblemExample example);

    List<Problem> selectByExample(ProblemExample example);

    Problem selectByPrimaryKey(Integer problemid);

    int updateByExampleSelective(@Param("record") Problem record, @Param("example") ProblemExample example);

    int updateByExampleWithBLOBs(@Param("record") Problem record, @Param("example") ProblemExample example);

    int updateByExample(@Param("record") Problem record, @Param("example") ProblemExample example);

    int updateByPrimaryKeySelective(Problem record);

    int updateByPrimaryKeyWithBLOBs(Problem record);

    int updateByPrimaryKey(Problem record);
    List<Problem>selectProblemsbyRand(Integer num);

    /**
     * 获取所有试题
     * @return
     */
    List<Problem>selectAllProblems(PageBean pageBean);
}