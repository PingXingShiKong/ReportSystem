package com.i1314i.reportsystem.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

/**
 * 查询学院报道率
 */
@Component
public interface ReportsRateMapper {
    //查询未报道人数
    @Select("select count(*) from reportsystem.basicinfo,reportsystem.college where basicinfo.college=college.collegename and college.collegeid=#{collegeid} and basicinfo.spotRegister!='已报到'")
    public int selectNoReport(@Param("collegeid") Integer collegeid);
    //查询总人数
    @Select(" select count(*) from reportsystem.basicinfo,reportsystem.college where basicinfo.college=college.collegename and college.collegeid=#{collegeid}")
    public int selectTotalNum(@Param("collegeid") Integer collegeid);
    //查询一报道人数
    @Select("select count(*) from reportsystem.basicinfo,reportsystem.college where basicinfo.college=college.collegename and college.collegeid=#{collegeid} and basicinfo.spotRegister='已报到'")
    public int selectReport(@Param("collegeid") Integer collegeid);
}