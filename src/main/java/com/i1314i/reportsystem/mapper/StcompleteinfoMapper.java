package com.i1314i.reportsystem.mapper;

import com.i1314i.reportsystem.po.Stcompleteinfo;
import com.i1314i.reportsystem.po.StcompleteinfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Component
public interface StcompleteinfoMapper {
    long countByExample(StcompleteinfoExample example);

    int deleteByExample(StcompleteinfoExample example);

    int deleteByPrimaryKey(String sno);

    int insert(Stcompleteinfo record);

    int insertSelective(Stcompleteinfo record);

    List<Stcompleteinfo> selectByExample(StcompleteinfoExample example);

    Stcompleteinfo selectByPrimaryKey(String sno);

    int updateByExampleSelective(@Param("record") Stcompleteinfo record, @Param("example") StcompleteinfoExample example);

    int updateByExample(@Param("record") Stcompleteinfo record, @Param("example") StcompleteinfoExample example);

    int updateByPrimaryKeySelective(Stcompleteinfo record);

    int updateByPrimaryKey(Stcompleteinfo record);

    void delegeAll();
}