package com.i1314i.reportsystem.mapper;

import com.i1314i.reportsystem.po.Studentlogin;
import com.i1314i.reportsystem.po.StudentloginExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Component
public interface StudentloginMapper {
    long countByExample(StudentloginExample example);

    int deleteByExample(StudentloginExample example);

    int deleteByPrimaryKey(String sno);

    int insert(Studentlogin record);

    int insertSelective(Studentlogin record);

    List<Studentlogin> selectByExample(StudentloginExample example);

    Studentlogin selectByPrimaryKey(String sno);

    int updateByExampleSelective(@Param("record") Studentlogin record, @Param("example") StudentloginExample example);

    int updateByExample(@Param("record") Studentlogin record, @Param("example") StudentloginExample example);

    int updateByPrimaryKeySelective(Studentlogin record);

    int updateByPrimaryKey(Studentlogin record);

    void deleteAll();
}