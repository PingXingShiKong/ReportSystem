package com.i1314i.reportsystem.po;

import java.io.Serializable;

public class Basicinfo implements Serializable {
    private String sno;

    private String name;

    private String sex;

    private String examnum;

    private String idnum;

    private String campus;

    private String college;

    private String major;

    private String classnum;

    private String dormnum;

    private String bankcardid;

    private String paysta;

    private String collegetel;

    private String schooltel;

    private String mastername;

    private String mastertel;

    private String brother;

    private String brothertel;

    private String helper;

    private String helpertel;

    private String spotregister;

    private String reason;

    private String remarkOne;

    private String remarkTwo;

    private Integer collegeid;

    public Integer getCollegeid() {
        return collegeid;
    }

    public void setCollegeid(Integer collegeid) {
        this.collegeid = collegeid;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno == null ? null : sno.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getExamnum() {
        return examnum;
    }

    public void setExamnum(String examnum) {
        this.examnum = examnum == null ? null : examnum.trim();
    }

    public String getIdnum() {
        return idnum;
    }

    public void setIdnum(String idnum) {
        this.idnum = idnum == null ? null : idnum.trim();
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus == null ? null : campus.trim();
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college == null ? null : college.trim();
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major == null ? null : major.trim();
    }

    public String getClassnum() {
        return classnum;
    }

    public void setClassnum(String classnum) {
        this.classnum = classnum == null ? null : classnum.trim();
    }

    public String getDormnum() {
        return dormnum;
    }

    public void setDormnum(String dormnum) {
        this.dormnum = dormnum == null ? null : dormnum.trim();
    }

    public String getBankcardid() {
        return bankcardid;
    }

    public void setBankcardid(String bankcardid) {
        this.bankcardid = bankcardid == null ? null : bankcardid.trim();
    }

    public String getPaysta() {
        return paysta;
    }

    public void setPaysta(String paysta) {
        this.paysta = paysta == null ? null : paysta.trim();
    }

    public String getCollegetel() {
        return collegetel;
    }

    public void setCollegetel(String collegetel) {
        this.collegetel = collegetel == null ? null : collegetel.trim();
    }

    public String getSchooltel() {
        return schooltel;
    }

    public void setSchooltel(String schooltel) {
        this.schooltel = schooltel == null ? null : schooltel.trim();
    }

    public String getMastername() {
        return mastername;
    }

    public void setMastername(String mastername) {
        this.mastername = mastername == null ? null : mastername.trim();
    }

    public String getMastertel() {
        return mastertel;
    }

    public void setMastertel(String mastertel) {
        this.mastertel = mastertel == null ? null : mastertel.trim();
    }

    public String getBrother() {
        return brother;
    }

    public void setBrother(String brother) {
        this.brother = brother == null ? null : brother.trim();
    }

    public String getBrothertel() {
        return brothertel;
    }

    public void setBrothertel(String brothertel) {
        this.brothertel = brothertel == null ? null : brothertel.trim();
    }

    public String getHelper() {
        return helper;
    }

    public void setHelper(String helper) {
        this.helper = helper == null ? null : helper.trim();
    }

    public String getHelpertel() {
        return helpertel;
    }

    public void setHelpertel(String helpertel) {
        this.helpertel = helpertel == null ? null : helpertel.trim();
    }

    public String getSpotregister() {
        return spotregister;
    }

    public void setSpotregister(String spotregister) {
        this.spotregister = spotregister == null ? null : spotregister.trim();
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason == null ? null : reason.trim();
    }

    public String getRemarkOne() {
        return remarkOne;
    }

    public void setRemarkOne(String remarkOne) {
        this.remarkOne = remarkOne == null ? null : remarkOne.trim();
    }

    public String getRemarkTwo() {
        return remarkTwo;
    }

    public void setRemarkTwo(String remarkTwo) {
        this.remarkTwo = remarkTwo == null ? null : remarkTwo.trim();
    }
}