package com.i1314i.reportsystem.po;

import java.util.ArrayList;
import java.util.List;

public class BasicinfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BasicinfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andSnoIsNull() {
            addCriterion("sno is null");
            return (Criteria) this;
        }

        public Criteria andSnoIsNotNull() {
            addCriterion("sno is not null");
            return (Criteria) this;
        }

        public Criteria andSnoEqualTo(String value) {
            addCriterion("sno =", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoNotEqualTo(String value) {
            addCriterion("sno <>", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoGreaterThan(String value) {
            addCriterion("sno >", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoGreaterThanOrEqualTo(String value) {
            addCriterion("sno >=", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoLessThan(String value) {
            addCriterion("sno <", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoLessThanOrEqualTo(String value) {
            addCriterion("sno <=", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoLike(String value) {
            addCriterion("sno like", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoNotLike(String value) {
            addCriterion("sno not like", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoIn(List<String> values) {
            addCriterion("sno in", values, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoNotIn(List<String> values) {
            addCriterion("sno not in", values, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoBetween(String value1, String value2) {
            addCriterion("sno between", value1, value2, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoNotBetween(String value1, String value2) {
            addCriterion("sno not between", value1, value2, "sno");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andSexIsNull() {
            addCriterion("sex is null");
            return (Criteria) this;
        }

        public Criteria andSexIsNotNull() {
            addCriterion("sex is not null");
            return (Criteria) this;
        }

        public Criteria andSexEqualTo(String value) {
            addCriterion("sex =", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotEqualTo(String value) {
            addCriterion("sex <>", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThan(String value) {
            addCriterion("sex >", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThanOrEqualTo(String value) {
            addCriterion("sex >=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThan(String value) {
            addCriterion("sex <", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThanOrEqualTo(String value) {
            addCriterion("sex <=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLike(String value) {
            addCriterion("sex like", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotLike(String value) {
            addCriterion("sex not like", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexIn(List<String> values) {
            addCriterion("sex in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotIn(List<String> values) {
            addCriterion("sex not in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexBetween(String value1, String value2) {
            addCriterion("sex between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotBetween(String value1, String value2) {
            addCriterion("sex not between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andExamnumIsNull() {
            addCriterion("examNum is null");
            return (Criteria) this;
        }

        public Criteria andExamnumIsNotNull() {
            addCriterion("examNum is not null");
            return (Criteria) this;
        }

        public Criteria andExamnumEqualTo(String value) {
            addCriterion("examNum =", value, "examnum");
            return (Criteria) this;
        }

        public Criteria andExamnumNotEqualTo(String value) {
            addCriterion("examNum <>", value, "examnum");
            return (Criteria) this;
        }

        public Criteria andExamnumGreaterThan(String value) {
            addCriterion("examNum >", value, "examnum");
            return (Criteria) this;
        }

        public Criteria andExamnumGreaterThanOrEqualTo(String value) {
            addCriterion("examNum >=", value, "examnum");
            return (Criteria) this;
        }

        public Criteria andExamnumLessThan(String value) {
            addCriterion("examNum <", value, "examnum");
            return (Criteria) this;
        }

        public Criteria andExamnumLessThanOrEqualTo(String value) {
            addCriterion("examNum <=", value, "examnum");
            return (Criteria) this;
        }

        public Criteria andExamnumLike(String value) {
            addCriterion("examNum like", value, "examnum");
            return (Criteria) this;
        }

        public Criteria andExamnumNotLike(String value) {
            addCriterion("examNum not like", value, "examnum");
            return (Criteria) this;
        }

        public Criteria andExamnumIn(List<String> values) {
            addCriterion("examNum in", values, "examnum");
            return (Criteria) this;
        }

        public Criteria andExamnumNotIn(List<String> values) {
            addCriterion("examNum not in", values, "examnum");
            return (Criteria) this;
        }

        public Criteria andExamnumBetween(String value1, String value2) {
            addCriterion("examNum between", value1, value2, "examnum");
            return (Criteria) this;
        }

        public Criteria andExamnumNotBetween(String value1, String value2) {
            addCriterion("examNum not between", value1, value2, "examnum");
            return (Criteria) this;
        }

        public Criteria andIdnumIsNull() {
            addCriterion("IDNum is null");
            return (Criteria) this;
        }

        public Criteria andIdnumIsNotNull() {
            addCriterion("IDNum is not null");
            return (Criteria) this;
        }

        public Criteria andIdnumEqualTo(String value) {
            addCriterion("IDNum =", value, "idnum");
            return (Criteria) this;
        }

        public Criteria andIdnumNotEqualTo(String value) {
            addCriterion("IDNum <>", value, "idnum");
            return (Criteria) this;
        }

        public Criteria andIdnumGreaterThan(String value) {
            addCriterion("IDNum >", value, "idnum");
            return (Criteria) this;
        }

        public Criteria andIdnumGreaterThanOrEqualTo(String value) {
            addCriterion("IDNum >=", value, "idnum");
            return (Criteria) this;
        }

        public Criteria andIdnumLessThan(String value) {
            addCriterion("IDNum <", value, "idnum");
            return (Criteria) this;
        }

        public Criteria andIdnumLessThanOrEqualTo(String value) {
            addCriterion("IDNum <=", value, "idnum");
            return (Criteria) this;
        }

        public Criteria andIdnumLike(String value) {
            addCriterion("IDNum like", value, "idnum");
            return (Criteria) this;
        }

        public Criteria andIdnumNotLike(String value) {
            addCriterion("IDNum not like", value, "idnum");
            return (Criteria) this;
        }

        public Criteria andIdnumIn(List<String> values) {
            addCriterion("IDNum in", values, "idnum");
            return (Criteria) this;
        }

        public Criteria andIdnumNotIn(List<String> values) {
            addCriterion("IDNum not in", values, "idnum");
            return (Criteria) this;
        }

        public Criteria andIdnumBetween(String value1, String value2) {
            addCriterion("IDNum between", value1, value2, "idnum");
            return (Criteria) this;
        }

        public Criteria andIdnumNotBetween(String value1, String value2) {
            addCriterion("IDNum not between", value1, value2, "idnum");
            return (Criteria) this;
        }

        public Criteria andCampusIsNull() {
            addCriterion("campus is null");
            return (Criteria) this;
        }

        public Criteria andCampusIsNotNull() {
            addCriterion("campus is not null");
            return (Criteria) this;
        }

        public Criteria andCampusEqualTo(String value) {
            addCriterion("campus =", value, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusNotEqualTo(String value) {
            addCriterion("campus <>", value, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusGreaterThan(String value) {
            addCriterion("campus >", value, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusGreaterThanOrEqualTo(String value) {
            addCriterion("campus >=", value, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusLessThan(String value) {
            addCriterion("campus <", value, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusLessThanOrEqualTo(String value) {
            addCriterion("campus <=", value, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusLike(String value) {
            addCriterion("campus like", value, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusNotLike(String value) {
            addCriterion("campus not like", value, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusIn(List<String> values) {
            addCriterion("campus in", values, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusNotIn(List<String> values) {
            addCriterion("campus not in", values, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusBetween(String value1, String value2) {
            addCriterion("campus between", value1, value2, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusNotBetween(String value1, String value2) {
            addCriterion("campus not between", value1, value2, "campus");
            return (Criteria) this;
        }

        public Criteria andCollegeIsNull() {
            addCriterion("college is null");
            return (Criteria) this;
        }

        public Criteria andCollegeIsNotNull() {
            addCriterion("college is not null");
            return (Criteria) this;
        }

        public Criteria andCollegeEqualTo(String value) {
            addCriterion("college =", value, "college");
            return (Criteria) this;
        }

        public Criteria andCollegeNotEqualTo(String value) {
            addCriterion("college <>", value, "college");
            return (Criteria) this;
        }

        public Criteria andCollegeGreaterThan(String value) {
            addCriterion("college >", value, "college");
            return (Criteria) this;
        }

        public Criteria andCollegeGreaterThanOrEqualTo(String value) {
            addCriterion("college >=", value, "college");
            return (Criteria) this;
        }

        public Criteria andCollegeLessThan(String value) {
            addCriterion("college <", value, "college");
            return (Criteria) this;
        }

        public Criteria andCollegeLessThanOrEqualTo(String value) {
            addCriterion("college <=", value, "college");
            return (Criteria) this;
        }

        public Criteria andCollegeLike(String value) {
            addCriterion("college like", value, "college");
            return (Criteria) this;
        }

        public Criteria andCollegeNotLike(String value) {
            addCriterion("college not like", value, "college");
            return (Criteria) this;
        }

        public Criteria andCollegeIn(List<String> values) {
            addCriterion("college in", values, "college");
            return (Criteria) this;
        }

        public Criteria andCollegeNotIn(List<String> values) {
            addCriterion("college not in", values, "college");
            return (Criteria) this;
        }

        public Criteria andCollegeBetween(String value1, String value2) {
            addCriterion("college between", value1, value2, "college");
            return (Criteria) this;
        }

        public Criteria andCollegeNotBetween(String value1, String value2) {
            addCriterion("college not between", value1, value2, "college");
            return (Criteria) this;
        }

        public Criteria andMajorIsNull() {
            addCriterion("major is null");
            return (Criteria) this;
        }

        public Criteria andMajorIsNotNull() {
            addCriterion("major is not null");
            return (Criteria) this;
        }

        public Criteria andMajorEqualTo(String value) {
            addCriterion("major =", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorNotEqualTo(String value) {
            addCriterion("major <>", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorGreaterThan(String value) {
            addCriterion("major >", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorGreaterThanOrEqualTo(String value) {
            addCriterion("major >=", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorLessThan(String value) {
            addCriterion("major <", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorLessThanOrEqualTo(String value) {
            addCriterion("major <=", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorLike(String value) {
            addCriterion("major like", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorNotLike(String value) {
            addCriterion("major not like", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorIn(List<String> values) {
            addCriterion("major in", values, "major");
            return (Criteria) this;
        }

        public Criteria andMajorNotIn(List<String> values) {
            addCriterion("major not in", values, "major");
            return (Criteria) this;
        }

        public Criteria andMajorBetween(String value1, String value2) {
            addCriterion("major between", value1, value2, "major");
            return (Criteria) this;
        }

        public Criteria andMajorNotBetween(String value1, String value2) {
            addCriterion("major not between", value1, value2, "major");
            return (Criteria) this;
        }

        public Criteria andClassnumIsNull() {
            addCriterion("classNum is null");
            return (Criteria) this;
        }

        public Criteria andClassnumIsNotNull() {
            addCriterion("classNum is not null");
            return (Criteria) this;
        }

        public Criteria andClassnumEqualTo(String value) {
            addCriterion("classNum =", value, "classnum");
            return (Criteria) this;
        }

        public Criteria andClassnumNotEqualTo(String value) {
            addCriterion("classNum <>", value, "classnum");
            return (Criteria) this;
        }

        public Criteria andClassnumGreaterThan(String value) {
            addCriterion("classNum >", value, "classnum");
            return (Criteria) this;
        }

        public Criteria andClassnumGreaterThanOrEqualTo(String value) {
            addCriterion("classNum >=", value, "classnum");
            return (Criteria) this;
        }

        public Criteria andClassnumLessThan(String value) {
            addCriterion("classNum <", value, "classnum");
            return (Criteria) this;
        }

        public Criteria andClassnumLessThanOrEqualTo(String value) {
            addCriterion("classNum <=", value, "classnum");
            return (Criteria) this;
        }

        public Criteria andClassnumLike(String value) {
            addCriterion("classNum like", value, "classnum");
            return (Criteria) this;
        }

        public Criteria andClassnumNotLike(String value) {
            addCriterion("classNum not like", value, "classnum");
            return (Criteria) this;
        }

        public Criteria andClassnumIn(List<String> values) {
            addCriterion("classNum in", values, "classnum");
            return (Criteria) this;
        }

        public Criteria andClassnumNotIn(List<String> values) {
            addCriterion("classNum not in", values, "classnum");
            return (Criteria) this;
        }

        public Criteria andClassnumBetween(String value1, String value2) {
            addCriterion("classNum between", value1, value2, "classnum");
            return (Criteria) this;
        }

        public Criteria andClassnumNotBetween(String value1, String value2) {
            addCriterion("classNum not between", value1, value2, "classnum");
            return (Criteria) this;
        }

        public Criteria andDormnumIsNull() {
            addCriterion("dormNum is null");
            return (Criteria) this;
        }

        public Criteria andDormnumIsNotNull() {
            addCriterion("dormNum is not null");
            return (Criteria) this;
        }

        public Criteria andDormnumEqualTo(String value) {
            addCriterion("dormNum =", value, "dormnum");
            return (Criteria) this;
        }

        public Criteria andDormnumNotEqualTo(String value) {
            addCriterion("dormNum <>", value, "dormnum");
            return (Criteria) this;
        }

        public Criteria andDormnumGreaterThan(String value) {
            addCriterion("dormNum >", value, "dormnum");
            return (Criteria) this;
        }

        public Criteria andDormnumGreaterThanOrEqualTo(String value) {
            addCriterion("dormNum >=", value, "dormnum");
            return (Criteria) this;
        }

        public Criteria andDormnumLessThan(String value) {
            addCriterion("dormNum <", value, "dormnum");
            return (Criteria) this;
        }

        public Criteria andDormnumLessThanOrEqualTo(String value) {
            addCriterion("dormNum <=", value, "dormnum");
            return (Criteria) this;
        }

        public Criteria andDormnumLike(String value) {
            addCriterion("dormNum like", value, "dormnum");
            return (Criteria) this;
        }

        public Criteria andDormnumNotLike(String value) {
            addCriterion("dormNum not like", value, "dormnum");
            return (Criteria) this;
        }

        public Criteria andDormnumIn(List<String> values) {
            addCriterion("dormNum in", values, "dormnum");
            return (Criteria) this;
        }

        public Criteria andDormnumNotIn(List<String> values) {
            addCriterion("dormNum not in", values, "dormnum");
            return (Criteria) this;
        }

        public Criteria andDormnumBetween(String value1, String value2) {
            addCriterion("dormNum between", value1, value2, "dormnum");
            return (Criteria) this;
        }

        public Criteria andDormnumNotBetween(String value1, String value2) {
            addCriterion("dormNum not between", value1, value2, "dormnum");
            return (Criteria) this;
        }

        public Criteria andBankcardidIsNull() {
            addCriterion("bankCardID is null");
            return (Criteria) this;
        }

        public Criteria andBankcardidIsNotNull() {
            addCriterion("bankCardID is not null");
            return (Criteria) this;
        }

        public Criteria andBankcardidEqualTo(String value) {
            addCriterion("bankCardID =", value, "bankcardid");
            return (Criteria) this;
        }

        public Criteria andBankcardidNotEqualTo(String value) {
            addCriterion("bankCardID <>", value, "bankcardid");
            return (Criteria) this;
        }

        public Criteria andBankcardidGreaterThan(String value) {
            addCriterion("bankCardID >", value, "bankcardid");
            return (Criteria) this;
        }

        public Criteria andBankcardidGreaterThanOrEqualTo(String value) {
            addCriterion("bankCardID >=", value, "bankcardid");
            return (Criteria) this;
        }

        public Criteria andBankcardidLessThan(String value) {
            addCriterion("bankCardID <", value, "bankcardid");
            return (Criteria) this;
        }

        public Criteria andBankcardidLessThanOrEqualTo(String value) {
            addCriterion("bankCardID <=", value, "bankcardid");
            return (Criteria) this;
        }

        public Criteria andBankcardidLike(String value) {
            addCriterion("bankCardID like", value, "bankcardid");
            return (Criteria) this;
        }

        public Criteria andBankcardidNotLike(String value) {
            addCriterion("bankCardID not like", value, "bankcardid");
            return (Criteria) this;
        }

        public Criteria andBankcardidIn(List<String> values) {
            addCriterion("bankCardID in", values, "bankcardid");
            return (Criteria) this;
        }

        public Criteria andBankcardidNotIn(List<String> values) {
            addCriterion("bankCardID not in", values, "bankcardid");
            return (Criteria) this;
        }

        public Criteria andBankcardidBetween(String value1, String value2) {
            addCriterion("bankCardID between", value1, value2, "bankcardid");
            return (Criteria) this;
        }

        public Criteria andBankcardidNotBetween(String value1, String value2) {
            addCriterion("bankCardID not between", value1, value2, "bankcardid");
            return (Criteria) this;
        }

        public Criteria andPaystaIsNull() {
            addCriterion("paySta is null");
            return (Criteria) this;
        }

        public Criteria andPaystaIsNotNull() {
            addCriterion("paySta is not null");
            return (Criteria) this;
        }

        public Criteria andPaystaEqualTo(String value) {
            addCriterion("paySta =", value, "paysta");
            return (Criteria) this;
        }

        public Criteria andPaystaNotEqualTo(String value) {
            addCriterion("paySta <>", value, "paysta");
            return (Criteria) this;
        }

        public Criteria andPaystaGreaterThan(String value) {
            addCriterion("paySta >", value, "paysta");
            return (Criteria) this;
        }

        public Criteria andPaystaGreaterThanOrEqualTo(String value) {
            addCriterion("paySta >=", value, "paysta");
            return (Criteria) this;
        }

        public Criteria andPaystaLessThan(String value) {
            addCriterion("paySta <", value, "paysta");
            return (Criteria) this;
        }

        public Criteria andPaystaLessThanOrEqualTo(String value) {
            addCriterion("paySta <=", value, "paysta");
            return (Criteria) this;
        }

        public Criteria andPaystaLike(String value) {
            addCriterion("paySta like", value, "paysta");
            return (Criteria) this;
        }

        public Criteria andPaystaNotLike(String value) {
            addCriterion("paySta not like", value, "paysta");
            return (Criteria) this;
        }

        public Criteria andPaystaIn(List<String> values) {
            addCriterion("paySta in", values, "paysta");
            return (Criteria) this;
        }

        public Criteria andPaystaNotIn(List<String> values) {
            addCriterion("paySta not in", values, "paysta");
            return (Criteria) this;
        }

        public Criteria andPaystaBetween(String value1, String value2) {
            addCriterion("paySta between", value1, value2, "paysta");
            return (Criteria) this;
        }

        public Criteria andPaystaNotBetween(String value1, String value2) {
            addCriterion("paySta not between", value1, value2, "paysta");
            return (Criteria) this;
        }

        public Criteria andCollegetelIsNull() {
            addCriterion("collegeTel is null");
            return (Criteria) this;
        }

        public Criteria andCollegetelIsNotNull() {
            addCriterion("collegeTel is not null");
            return (Criteria) this;
        }

        public Criteria andCollegetelEqualTo(String value) {
            addCriterion("collegeTel =", value, "collegetel");
            return (Criteria) this;
        }

        public Criteria andCollegetelNotEqualTo(String value) {
            addCriterion("collegeTel <>", value, "collegetel");
            return (Criteria) this;
        }

        public Criteria andCollegetelGreaterThan(String value) {
            addCriterion("collegeTel >", value, "collegetel");
            return (Criteria) this;
        }

        public Criteria andCollegetelGreaterThanOrEqualTo(String value) {
            addCriterion("collegeTel >=", value, "collegetel");
            return (Criteria) this;
        }

        public Criteria andCollegetelLessThan(String value) {
            addCriterion("collegeTel <", value, "collegetel");
            return (Criteria) this;
        }

        public Criteria andCollegetelLessThanOrEqualTo(String value) {
            addCriterion("collegeTel <=", value, "collegetel");
            return (Criteria) this;
        }

        public Criteria andCollegetelLike(String value) {
            addCriterion("collegeTel like", value, "collegetel");
            return (Criteria) this;
        }

        public Criteria andCollegetelNotLike(String value) {
            addCriterion("collegeTel not like", value, "collegetel");
            return (Criteria) this;
        }

        public Criteria andCollegetelIn(List<String> values) {
            addCriterion("collegeTel in", values, "collegetel");
            return (Criteria) this;
        }

        public Criteria andCollegetelNotIn(List<String> values) {
            addCriterion("collegeTel not in", values, "collegetel");
            return (Criteria) this;
        }

        public Criteria andCollegetelBetween(String value1, String value2) {
            addCriterion("collegeTel between", value1, value2, "collegetel");
            return (Criteria) this;
        }

        public Criteria andCollegetelNotBetween(String value1, String value2) {
            addCriterion("collegeTel not between", value1, value2, "collegetel");
            return (Criteria) this;
        }

        public Criteria andSchooltelIsNull() {
            addCriterion("schoolTel is null");
            return (Criteria) this;
        }

        public Criteria andSchooltelIsNotNull() {
            addCriterion("schoolTel is not null");
            return (Criteria) this;
        }

        public Criteria andSchooltelEqualTo(String value) {
            addCriterion("schoolTel =", value, "schooltel");
            return (Criteria) this;
        }

        public Criteria andSchooltelNotEqualTo(String value) {
            addCriterion("schoolTel <>", value, "schooltel");
            return (Criteria) this;
        }

        public Criteria andSchooltelGreaterThan(String value) {
            addCriterion("schoolTel >", value, "schooltel");
            return (Criteria) this;
        }

        public Criteria andSchooltelGreaterThanOrEqualTo(String value) {
            addCriterion("schoolTel >=", value, "schooltel");
            return (Criteria) this;
        }

        public Criteria andSchooltelLessThan(String value) {
            addCriterion("schoolTel <", value, "schooltel");
            return (Criteria) this;
        }

        public Criteria andSchooltelLessThanOrEqualTo(String value) {
            addCriterion("schoolTel <=", value, "schooltel");
            return (Criteria) this;
        }

        public Criteria andSchooltelLike(String value) {
            addCriterion("schoolTel like", value, "schooltel");
            return (Criteria) this;
        }

        public Criteria andSchooltelNotLike(String value) {
            addCriterion("schoolTel not like", value, "schooltel");
            return (Criteria) this;
        }

        public Criteria andSchooltelIn(List<String> values) {
            addCriterion("schoolTel in", values, "schooltel");
            return (Criteria) this;
        }

        public Criteria andSchooltelNotIn(List<String> values) {
            addCriterion("schoolTel not in", values, "schooltel");
            return (Criteria) this;
        }

        public Criteria andSchooltelBetween(String value1, String value2) {
            addCriterion("schoolTel between", value1, value2, "schooltel");
            return (Criteria) this;
        }

        public Criteria andSchooltelNotBetween(String value1, String value2) {
            addCriterion("schoolTel not between", value1, value2, "schooltel");
            return (Criteria) this;
        }

        public Criteria andMasternameIsNull() {
            addCriterion("masterName is null");
            return (Criteria) this;
        }

        public Criteria andMasternameIsNotNull() {
            addCriterion("masterName is not null");
            return (Criteria) this;
        }

        public Criteria andMasternameEqualTo(String value) {
            addCriterion("masterName =", value, "mastername");
            return (Criteria) this;
        }

        public Criteria andMasternameNotEqualTo(String value) {
            addCriterion("masterName <>", value, "mastername");
            return (Criteria) this;
        }

        public Criteria andMasternameGreaterThan(String value) {
            addCriterion("masterName >", value, "mastername");
            return (Criteria) this;
        }

        public Criteria andMasternameGreaterThanOrEqualTo(String value) {
            addCriterion("masterName >=", value, "mastername");
            return (Criteria) this;
        }

        public Criteria andMasternameLessThan(String value) {
            addCriterion("masterName <", value, "mastername");
            return (Criteria) this;
        }

        public Criteria andMasternameLessThanOrEqualTo(String value) {
            addCriterion("masterName <=", value, "mastername");
            return (Criteria) this;
        }

        public Criteria andMasternameLike(String value) {
            addCriterion("masterName like", value, "mastername");
            return (Criteria) this;
        }

        public Criteria andMasternameNotLike(String value) {
            addCriterion("masterName not like", value, "mastername");
            return (Criteria) this;
        }

        public Criteria andMasternameIn(List<String> values) {
            addCriterion("masterName in", values, "mastername");
            return (Criteria) this;
        }

        public Criteria andMasternameNotIn(List<String> values) {
            addCriterion("masterName not in", values, "mastername");
            return (Criteria) this;
        }

        public Criteria andMasternameBetween(String value1, String value2) {
            addCriterion("masterName between", value1, value2, "mastername");
            return (Criteria) this;
        }

        public Criteria andMasternameNotBetween(String value1, String value2) {
            addCriterion("masterName not between", value1, value2, "mastername");
            return (Criteria) this;
        }

        public Criteria andMastertelIsNull() {
            addCriterion("masterTel is null");
            return (Criteria) this;
        }

        public Criteria andMastertelIsNotNull() {
            addCriterion("masterTel is not null");
            return (Criteria) this;
        }

        public Criteria andMastertelEqualTo(String value) {
            addCriterion("masterTel =", value, "mastertel");
            return (Criteria) this;
        }

        public Criteria andMastertelNotEqualTo(String value) {
            addCriterion("masterTel <>", value, "mastertel");
            return (Criteria) this;
        }

        public Criteria andMastertelGreaterThan(String value) {
            addCriterion("masterTel >", value, "mastertel");
            return (Criteria) this;
        }

        public Criteria andMastertelGreaterThanOrEqualTo(String value) {
            addCriterion("masterTel >=", value, "mastertel");
            return (Criteria) this;
        }

        public Criteria andMastertelLessThan(String value) {
            addCriterion("masterTel <", value, "mastertel");
            return (Criteria) this;
        }

        public Criteria andMastertelLessThanOrEqualTo(String value) {
            addCriterion("masterTel <=", value, "mastertel");
            return (Criteria) this;
        }

        public Criteria andMastertelLike(String value) {
            addCriterion("masterTel like", value, "mastertel");
            return (Criteria) this;
        }

        public Criteria andMastertelNotLike(String value) {
            addCriterion("masterTel not like", value, "mastertel");
            return (Criteria) this;
        }

        public Criteria andMastertelIn(List<String> values) {
            addCriterion("masterTel in", values, "mastertel");
            return (Criteria) this;
        }

        public Criteria andMastertelNotIn(List<String> values) {
            addCriterion("masterTel not in", values, "mastertel");
            return (Criteria) this;
        }

        public Criteria andMastertelBetween(String value1, String value2) {
            addCriterion("masterTel between", value1, value2, "mastertel");
            return (Criteria) this;
        }

        public Criteria andMastertelNotBetween(String value1, String value2) {
            addCriterion("masterTel not between", value1, value2, "mastertel");
            return (Criteria) this;
        }

        public Criteria andBrotherIsNull() {
            addCriterion("brother is null");
            return (Criteria) this;
        }

        public Criteria andBrotherIsNotNull() {
            addCriterion("brother is not null");
            return (Criteria) this;
        }

        public Criteria andBrotherEqualTo(String value) {
            addCriterion("brother =", value, "brother");
            return (Criteria) this;
        }

        public Criteria andBrotherNotEqualTo(String value) {
            addCriterion("brother <>", value, "brother");
            return (Criteria) this;
        }

        public Criteria andBrotherGreaterThan(String value) {
            addCriterion("brother >", value, "brother");
            return (Criteria) this;
        }

        public Criteria andBrotherGreaterThanOrEqualTo(String value) {
            addCriterion("brother >=", value, "brother");
            return (Criteria) this;
        }

        public Criteria andBrotherLessThan(String value) {
            addCriterion("brother <", value, "brother");
            return (Criteria) this;
        }

        public Criteria andBrotherLessThanOrEqualTo(String value) {
            addCriterion("brother <=", value, "brother");
            return (Criteria) this;
        }

        public Criteria andBrotherLike(String value) {
            addCriterion("brother like", value, "brother");
            return (Criteria) this;
        }

        public Criteria andBrotherNotLike(String value) {
            addCriterion("brother not like", value, "brother");
            return (Criteria) this;
        }

        public Criteria andBrotherIn(List<String> values) {
            addCriterion("brother in", values, "brother");
            return (Criteria) this;
        }

        public Criteria andBrotherNotIn(List<String> values) {
            addCriterion("brother not in", values, "brother");
            return (Criteria) this;
        }

        public Criteria andBrotherBetween(String value1, String value2) {
            addCriterion("brother between", value1, value2, "brother");
            return (Criteria) this;
        }

        public Criteria andBrotherNotBetween(String value1, String value2) {
            addCriterion("brother not between", value1, value2, "brother");
            return (Criteria) this;
        }

        public Criteria andBrothertelIsNull() {
            addCriterion("brothertel is null");
            return (Criteria) this;
        }

        public Criteria andBrothertelIsNotNull() {
            addCriterion("brothertel is not null");
            return (Criteria) this;
        }

        public Criteria andBrothertelEqualTo(String value) {
            addCriterion("brothertel =", value, "brothertel");
            return (Criteria) this;
        }

        public Criteria andBrothertelNotEqualTo(String value) {
            addCriterion("brothertel <>", value, "brothertel");
            return (Criteria) this;
        }

        public Criteria andBrothertelGreaterThan(String value) {
            addCriterion("brothertel >", value, "brothertel");
            return (Criteria) this;
        }

        public Criteria andBrothertelGreaterThanOrEqualTo(String value) {
            addCriterion("brothertel >=", value, "brothertel");
            return (Criteria) this;
        }

        public Criteria andBrothertelLessThan(String value) {
            addCriterion("brothertel <", value, "brothertel");
            return (Criteria) this;
        }

        public Criteria andBrothertelLessThanOrEqualTo(String value) {
            addCriterion("brothertel <=", value, "brothertel");
            return (Criteria) this;
        }

        public Criteria andBrothertelLike(String value) {
            addCriterion("brothertel like", value, "brothertel");
            return (Criteria) this;
        }

        public Criteria andBrothertelNotLike(String value) {
            addCriterion("brothertel not like", value, "brothertel");
            return (Criteria) this;
        }

        public Criteria andBrothertelIn(List<String> values) {
            addCriterion("brothertel in", values, "brothertel");
            return (Criteria) this;
        }

        public Criteria andBrothertelNotIn(List<String> values) {
            addCriterion("brothertel not in", values, "brothertel");
            return (Criteria) this;
        }

        public Criteria andBrothertelBetween(String value1, String value2) {
            addCriterion("brothertel between", value1, value2, "brothertel");
            return (Criteria) this;
        }

        public Criteria andBrothertelNotBetween(String value1, String value2) {
            addCriterion("brothertel not between", value1, value2, "brothertel");
            return (Criteria) this;
        }

        public Criteria andHelperIsNull() {
            addCriterion("helper is null");
            return (Criteria) this;
        }

        public Criteria andHelperIsNotNull() {
            addCriterion("helper is not null");
            return (Criteria) this;
        }

        public Criteria andHelperEqualTo(String value) {
            addCriterion("helper =", value, "helper");
            return (Criteria) this;
        }

        public Criteria andHelperNotEqualTo(String value) {
            addCriterion("helper <>", value, "helper");
            return (Criteria) this;
        }

        public Criteria andHelperGreaterThan(String value) {
            addCriterion("helper >", value, "helper");
            return (Criteria) this;
        }

        public Criteria andHelperGreaterThanOrEqualTo(String value) {
            addCriterion("helper >=", value, "helper");
            return (Criteria) this;
        }

        public Criteria andHelperLessThan(String value) {
            addCriterion("helper <", value, "helper");
            return (Criteria) this;
        }

        public Criteria andHelperLessThanOrEqualTo(String value) {
            addCriterion("helper <=", value, "helper");
            return (Criteria) this;
        }

        public Criteria andHelperLike(String value) {
            addCriterion("helper like", value, "helper");
            return (Criteria) this;
        }

        public Criteria andHelperNotLike(String value) {
            addCriterion("helper not like", value, "helper");
            return (Criteria) this;
        }

        public Criteria andHelperIn(List<String> values) {
            addCriterion("helper in", values, "helper");
            return (Criteria) this;
        }

        public Criteria andHelperNotIn(List<String> values) {
            addCriterion("helper not in", values, "helper");
            return (Criteria) this;
        }

        public Criteria andHelperBetween(String value1, String value2) {
            addCriterion("helper between", value1, value2, "helper");
            return (Criteria) this;
        }

        public Criteria andHelperNotBetween(String value1, String value2) {
            addCriterion("helper not between", value1, value2, "helper");
            return (Criteria) this;
        }

        public Criteria andHelpertelIsNull() {
            addCriterion("helpertel is null");
            return (Criteria) this;
        }

        public Criteria andHelpertelIsNotNull() {
            addCriterion("helpertel is not null");
            return (Criteria) this;
        }

        public Criteria andHelpertelEqualTo(String value) {
            addCriterion("helpertel =", value, "helpertel");
            return (Criteria) this;
        }

        public Criteria andHelpertelNotEqualTo(String value) {
            addCriterion("helpertel <>", value, "helpertel");
            return (Criteria) this;
        }

        public Criteria andHelpertelGreaterThan(String value) {
            addCriterion("helpertel >", value, "helpertel");
            return (Criteria) this;
        }

        public Criteria andHelpertelGreaterThanOrEqualTo(String value) {
            addCriterion("helpertel >=", value, "helpertel");
            return (Criteria) this;
        }

        public Criteria andHelpertelLessThan(String value) {
            addCriterion("helpertel <", value, "helpertel");
            return (Criteria) this;
        }

        public Criteria andHelpertelLessThanOrEqualTo(String value) {
            addCriterion("helpertel <=", value, "helpertel");
            return (Criteria) this;
        }

        public Criteria andHelpertelLike(String value) {
            addCriterion("helpertel like", value, "helpertel");
            return (Criteria) this;
        }

        public Criteria andHelpertelNotLike(String value) {
            addCriterion("helpertel not like", value, "helpertel");
            return (Criteria) this;
        }

        public Criteria andHelpertelIn(List<String> values) {
            addCriterion("helpertel in", values, "helpertel");
            return (Criteria) this;
        }

        public Criteria andHelpertelNotIn(List<String> values) {
            addCriterion("helpertel not in", values, "helpertel");
            return (Criteria) this;
        }

        public Criteria andHelpertelBetween(String value1, String value2) {
            addCriterion("helpertel between", value1, value2, "helpertel");
            return (Criteria) this;
        }

        public Criteria andHelpertelNotBetween(String value1, String value2) {
            addCriterion("helpertel not between", value1, value2, "helpertel");
            return (Criteria) this;
        }

        public Criteria andSpotregisterIsNull() {
            addCriterion("spotRegister is null");
            return (Criteria) this;
        }

        public Criteria andSpotregisterIsNotNull() {
            addCriterion("spotRegister is not null");
            return (Criteria) this;
        }

        public Criteria andSpotregisterEqualTo(String value) {
            addCriterion("spotRegister =", value, "spotregister");
            return (Criteria) this;
        }

        public Criteria andSpotregisterNotEqualTo(String value) {
            addCriterion("spotRegister <>", value, "spotregister");
            return (Criteria) this;
        }

        public Criteria andSpotregisterGreaterThan(String value) {
            addCriterion("spotRegister >", value, "spotregister");
            return (Criteria) this;
        }

        public Criteria andSpotregisterGreaterThanOrEqualTo(String value) {
            addCriterion("spotRegister >=", value, "spotregister");
            return (Criteria) this;
        }

        public Criteria andSpotregisterLessThan(String value) {
            addCriterion("spotRegister <", value, "spotregister");
            return (Criteria) this;
        }

        public Criteria andSpotregisterLessThanOrEqualTo(String value) {
            addCriterion("spotRegister <=", value, "spotregister");
            return (Criteria) this;
        }

        public Criteria andSpotregisterLike(String value) {
            addCriterion("spotRegister like", value, "spotregister");
            return (Criteria) this;
        }

        public Criteria andSpotregisterNotLike(String value) {
            addCriterion("spotRegister not like", value, "spotregister");
            return (Criteria) this;
        }

        public Criteria andSpotregisterIn(List<String> values) {
            addCriterion("spotRegister in", values, "spotregister");
            return (Criteria) this;
        }

        public Criteria andSpotregisterNotIn(List<String> values) {
            addCriterion("spotRegister not in", values, "spotregister");
            return (Criteria) this;
        }

        public Criteria andSpotregisterBetween(String value1, String value2) {
            addCriterion("spotRegister between", value1, value2, "spotregister");
            return (Criteria) this;
        }

        public Criteria andSpotregisterNotBetween(String value1, String value2) {
            addCriterion("spotRegister not between", value1, value2, "spotregister");
            return (Criteria) this;
        }

        public Criteria andReasonIsNull() {
            addCriterion("reason is null");
            return (Criteria) this;
        }

        public Criteria andReasonIsNotNull() {
            addCriterion("reason is not null");
            return (Criteria) this;
        }

        public Criteria andReasonEqualTo(String value) {
            addCriterion("reason =", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonNotEqualTo(String value) {
            addCriterion("reason <>", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonGreaterThan(String value) {
            addCriterion("reason >", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonGreaterThanOrEqualTo(String value) {
            addCriterion("reason >=", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonLessThan(String value) {
            addCriterion("reason <", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonLessThanOrEqualTo(String value) {
            addCriterion("reason <=", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonLike(String value) {
            addCriterion("reason like", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonNotLike(String value) {
            addCriterion("reason not like", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonIn(List<String> values) {
            addCriterion("reason in", values, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonNotIn(List<String> values) {
            addCriterion("reason not in", values, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonBetween(String value1, String value2) {
            addCriterion("reason between", value1, value2, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonNotBetween(String value1, String value2) {
            addCriterion("reason not between", value1, value2, "reason");
            return (Criteria) this;
        }

        public Criteria andRemarkOneIsNull() {
            addCriterion("remark_one is null");
            return (Criteria) this;
        }

        public Criteria andRemarkOneIsNotNull() {
            addCriterion("remark_one is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkOneEqualTo(String value) {
            addCriterion("remark_one =", value, "remarkOne");
            return (Criteria) this;
        }

        public Criteria andRemarkOneNotEqualTo(String value) {
            addCriterion("remark_one <>", value, "remarkOne");
            return (Criteria) this;
        }

        public Criteria andRemarkOneGreaterThan(String value) {
            addCriterion("remark_one >", value, "remarkOne");
            return (Criteria) this;
        }

        public Criteria andRemarkOneGreaterThanOrEqualTo(String value) {
            addCriterion("remark_one >=", value, "remarkOne");
            return (Criteria) this;
        }

        public Criteria andRemarkOneLessThan(String value) {
            addCriterion("remark_one <", value, "remarkOne");
            return (Criteria) this;
        }

        public Criteria andRemarkOneLessThanOrEqualTo(String value) {
            addCriterion("remark_one <=", value, "remarkOne");
            return (Criteria) this;
        }

        public Criteria andRemarkOneLike(String value) {
            addCriterion("remark_one like", value, "remarkOne");
            return (Criteria) this;
        }

        public Criteria andRemarkOneNotLike(String value) {
            addCriterion("remark_one not like", value, "remarkOne");
            return (Criteria) this;
        }

        public Criteria andRemarkOneIn(List<String> values) {
            addCriterion("remark_one in", values, "remarkOne");
            return (Criteria) this;
        }

        public Criteria andRemarkOneNotIn(List<String> values) {
            addCriterion("remark_one not in", values, "remarkOne");
            return (Criteria) this;
        }

        public Criteria andRemarkOneBetween(String value1, String value2) {
            addCriterion("remark_one between", value1, value2, "remarkOne");
            return (Criteria) this;
        }

        public Criteria andRemarkOneNotBetween(String value1, String value2) {
            addCriterion("remark_one not between", value1, value2, "remarkOne");
            return (Criteria) this;
        }

        public Criteria andRemarkTwoIsNull() {
            addCriterion("remark_two is null");
            return (Criteria) this;
        }

        public Criteria andRemarkTwoIsNotNull() {
            addCriterion("remark_two is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkTwoEqualTo(String value) {
            addCriterion("remark_two =", value, "remarkTwo");
            return (Criteria) this;
        }

        public Criteria andRemarkTwoNotEqualTo(String value) {
            addCriterion("remark_two <>", value, "remarkTwo");
            return (Criteria) this;
        }

        public Criteria andRemarkTwoGreaterThan(String value) {
            addCriterion("remark_two >", value, "remarkTwo");
            return (Criteria) this;
        }

        public Criteria andRemarkTwoGreaterThanOrEqualTo(String value) {
            addCriterion("remark_two >=", value, "remarkTwo");
            return (Criteria) this;
        }

        public Criteria andRemarkTwoLessThan(String value) {
            addCriterion("remark_two <", value, "remarkTwo");
            return (Criteria) this;
        }

        public Criteria andRemarkTwoLessThanOrEqualTo(String value) {
            addCriterion("remark_two <=", value, "remarkTwo");
            return (Criteria) this;
        }

        public Criteria andRemarkTwoLike(String value) {
            addCriterion("remark_two like", value, "remarkTwo");
            return (Criteria) this;
        }

        public Criteria andRemarkTwoNotLike(String value) {
            addCriterion("remark_two not like", value, "remarkTwo");
            return (Criteria) this;
        }

        public Criteria andRemarkTwoIn(List<String> values) {
            addCriterion("remark_two in", values, "remarkTwo");
            return (Criteria) this;
        }

        public Criteria andRemarkTwoNotIn(List<String> values) {
            addCriterion("remark_two not in", values, "remarkTwo");
            return (Criteria) this;
        }

        public Criteria andRemarkTwoBetween(String value1, String value2) {
            addCriterion("remark_two between", value1, value2, "remarkTwo");
            return (Criteria) this;
        }

        public Criteria andRemarkTwoNotBetween(String value1, String value2) {
            addCriterion("remark_two not between", value1, value2, "remarkTwo");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}