package com.i1314i.reportsystem.po;


import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
@Component
public class Basicinfolayui implements Serializable {
    private Integer code;
    private String msg;
    private Integer count;
    private List<Basicinfo> data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<Basicinfo> getData() {
        return data;
    }

    public void setData(List<Basicinfo> data) {
        this.data = data;
    }
}
