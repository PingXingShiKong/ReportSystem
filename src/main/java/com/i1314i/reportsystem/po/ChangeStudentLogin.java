package com.i1314i.reportsystem.po;

import java.io.Serializable;

/**
 * @author 平行时空
 * @created 2018-06-16 0:03
 **/
public class ChangeStudentLogin implements Serializable {
    private String sno;
    private String oldpassword;
    private String password;
    private String password1;

    public String getOldpassword() {
        return oldpassword;
    }

    public void setOldpassword(String oldpassword) {
        this.oldpassword = oldpassword;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword1() {
        return password1;
    }

    public void setPassword1(String password1) {
        this.password1 = password1;
    }
}
