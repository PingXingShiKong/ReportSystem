package com.i1314i.reportsystem.po;

import java.io.Serializable;

public class DateInformation implements Serializable {
    String college;
    Integer count;
    Integer noReport;
    int reportDate;

    public DateInformation(String college, Integer count, Integer noReport) {
        this.college = college;
        this.count = count;
        this.noReport = noReport;
        this.reportDate=(count-noReport)/count;
    }


}
