package com.i1314i.reportsystem.po;

import java.io.Serializable;

/**
 * @author 平行时空
 * @created 2018-05-12 15:44
 **/
public class JsonResult implements Serializable {
    private Integer  success;
    private String msg;
    private Object data;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public JsonResult(Integer success, String msg) {
        this.success = success;
        this.msg = msg;
    }

    public JsonResult() {
    }

    public static JsonResult createJsonResult(){
        return new JsonResult();
    }
    public static JsonResult createJsonResult(Integer success,String msg){
        return new JsonResult(success,msg);
    }
}
