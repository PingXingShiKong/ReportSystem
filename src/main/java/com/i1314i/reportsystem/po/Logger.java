package com.i1314i.reportsystem.po;

import java.io.Serializable;

public class Logger implements Serializable {
    private Integer id;

    private String uIp;

    private String uName;

    private String uOperate;

    private String uTime;

    private String uParams;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getuIp() {
        return uIp;
    }

    public void setuIp(String uIp) {
        this.uIp = uIp == null ? null : uIp.trim();
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName == null ? null : uName.trim();
    }

    public String getuOperate() {
        return uOperate;
    }

    public void setuOperate(String uOperate) {
        this.uOperate = uOperate == null ? null : uOperate.trim();
    }

    public String getuTime() {
        return uTime;
    }

    public void setuTime(String uTime) {
        this.uTime = uTime == null ? null : uTime.trim();
    }

    public String getuParams() {
        return uParams;
    }

    public void setuParams(String uParams) {
        this.uParams = uParams == null ? null : uParams.trim();
    }
}