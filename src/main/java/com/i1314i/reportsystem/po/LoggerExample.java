package com.i1314i.reportsystem.po;

import java.util.ArrayList;
import java.util.List;

public class LoggerExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public LoggerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("Id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("Id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("Id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("Id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("Id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("Id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("Id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("Id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("Id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("Id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andUIpIsNull() {
            addCriterion("u_ip is null");
            return (Criteria) this;
        }

        public Criteria andUIpIsNotNull() {
            addCriterion("u_ip is not null");
            return (Criteria) this;
        }

        public Criteria andUIpEqualTo(String value) {
            addCriterion("u_ip =", value, "uIp");
            return (Criteria) this;
        }

        public Criteria andUIpNotEqualTo(String value) {
            addCriterion("u_ip <>", value, "uIp");
            return (Criteria) this;
        }

        public Criteria andUIpGreaterThan(String value) {
            addCriterion("u_ip >", value, "uIp");
            return (Criteria) this;
        }

        public Criteria andUIpGreaterThanOrEqualTo(String value) {
            addCriterion("u_ip >=", value, "uIp");
            return (Criteria) this;
        }

        public Criteria andUIpLessThan(String value) {
            addCriterion("u_ip <", value, "uIp");
            return (Criteria) this;
        }

        public Criteria andUIpLessThanOrEqualTo(String value) {
            addCriterion("u_ip <=", value, "uIp");
            return (Criteria) this;
        }

        public Criteria andUIpLike(String value) {
            addCriterion("u_ip like", value, "uIp");
            return (Criteria) this;
        }

        public Criteria andUIpNotLike(String value) {
            addCriterion("u_ip not like", value, "uIp");
            return (Criteria) this;
        }

        public Criteria andUIpIn(List<String> values) {
            addCriterion("u_ip in", values, "uIp");
            return (Criteria) this;
        }

        public Criteria andUIpNotIn(List<String> values) {
            addCriterion("u_ip not in", values, "uIp");
            return (Criteria) this;
        }

        public Criteria andUIpBetween(String value1, String value2) {
            addCriterion("u_ip between", value1, value2, "uIp");
            return (Criteria) this;
        }

        public Criteria andUIpNotBetween(String value1, String value2) {
            addCriterion("u_ip not between", value1, value2, "uIp");
            return (Criteria) this;
        }

        public Criteria andUNameIsNull() {
            addCriterion("u_name is null");
            return (Criteria) this;
        }

        public Criteria andUNameIsNotNull() {
            addCriterion("u_name is not null");
            return (Criteria) this;
        }

        public Criteria andUNameEqualTo(String value) {
            addCriterion("u_name =", value, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameNotEqualTo(String value) {
            addCriterion("u_name <>", value, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameGreaterThan(String value) {
            addCriterion("u_name >", value, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameGreaterThanOrEqualTo(String value) {
            addCriterion("u_name >=", value, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameLessThan(String value) {
            addCriterion("u_name <", value, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameLessThanOrEqualTo(String value) {
            addCriterion("u_name <=", value, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameLike(String value) {
            addCriterion("u_name like", value, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameNotLike(String value) {
            addCriterion("u_name not like", value, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameIn(List<String> values) {
            addCriterion("u_name in", values, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameNotIn(List<String> values) {
            addCriterion("u_name not in", values, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameBetween(String value1, String value2) {
            addCriterion("u_name between", value1, value2, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameNotBetween(String value1, String value2) {
            addCriterion("u_name not between", value1, value2, "uName");
            return (Criteria) this;
        }

        public Criteria andUOperateIsNull() {
            addCriterion("u_operate is null");
            return (Criteria) this;
        }

        public Criteria andUOperateIsNotNull() {
            addCriterion("u_operate is not null");
            return (Criteria) this;
        }

        public Criteria andUOperateEqualTo(String value) {
            addCriterion("u_operate =", value, "uOperate");
            return (Criteria) this;
        }

        public Criteria andUOperateNotEqualTo(String value) {
            addCriterion("u_operate <>", value, "uOperate");
            return (Criteria) this;
        }

        public Criteria andUOperateGreaterThan(String value) {
            addCriterion("u_operate >", value, "uOperate");
            return (Criteria) this;
        }

        public Criteria andUOperateGreaterThanOrEqualTo(String value) {
            addCriterion("u_operate >=", value, "uOperate");
            return (Criteria) this;
        }

        public Criteria andUOperateLessThan(String value) {
            addCriterion("u_operate <", value, "uOperate");
            return (Criteria) this;
        }

        public Criteria andUOperateLessThanOrEqualTo(String value) {
            addCriterion("u_operate <=", value, "uOperate");
            return (Criteria) this;
        }

        public Criteria andUOperateLike(String value) {
            addCriterion("u_operate like", value, "uOperate");
            return (Criteria) this;
        }

        public Criteria andUOperateNotLike(String value) {
            addCriterion("u_operate not like", value, "uOperate");
            return (Criteria) this;
        }

        public Criteria andUOperateIn(List<String> values) {
            addCriterion("u_operate in", values, "uOperate");
            return (Criteria) this;
        }

        public Criteria andUOperateNotIn(List<String> values) {
            addCriterion("u_operate not in", values, "uOperate");
            return (Criteria) this;
        }

        public Criteria andUOperateBetween(String value1, String value2) {
            addCriterion("u_operate between", value1, value2, "uOperate");
            return (Criteria) this;
        }

        public Criteria andUOperateNotBetween(String value1, String value2) {
            addCriterion("u_operate not between", value1, value2, "uOperate");
            return (Criteria) this;
        }

        public Criteria andUTimeIsNull() {
            addCriterion("u_time is null");
            return (Criteria) this;
        }

        public Criteria andUTimeIsNotNull() {
            addCriterion("u_time is not null");
            return (Criteria) this;
        }

        public Criteria andUTimeEqualTo(String value) {
            addCriterion("u_time =", value, "uTime");
            return (Criteria) this;
        }

        public Criteria andUTimeNotEqualTo(String value) {
            addCriterion("u_time <>", value, "uTime");
            return (Criteria) this;
        }

        public Criteria andUTimeGreaterThan(String value) {
            addCriterion("u_time >", value, "uTime");
            return (Criteria) this;
        }

        public Criteria andUTimeGreaterThanOrEqualTo(String value) {
            addCriterion("u_time >=", value, "uTime");
            return (Criteria) this;
        }

        public Criteria andUTimeLessThan(String value) {
            addCriterion("u_time <", value, "uTime");
            return (Criteria) this;
        }

        public Criteria andUTimeLessThanOrEqualTo(String value) {
            addCriterion("u_time <=", value, "uTime");
            return (Criteria) this;
        }

        public Criteria andUTimeLike(String value) {
            addCriterion("u_time like", value, "uTime");
            return (Criteria) this;
        }

        public Criteria andUTimeNotLike(String value) {
            addCriterion("u_time not like", value, "uTime");
            return (Criteria) this;
        }

        public Criteria andUTimeIn(List<String> values) {
            addCriterion("u_time in", values, "uTime");
            return (Criteria) this;
        }

        public Criteria andUTimeNotIn(List<String> values) {
            addCriterion("u_time not in", values, "uTime");
            return (Criteria) this;
        }

        public Criteria andUTimeBetween(String value1, String value2) {
            addCriterion("u_time between", value1, value2, "uTime");
            return (Criteria) this;
        }

        public Criteria andUTimeNotBetween(String value1, String value2) {
            addCriterion("u_time not between", value1, value2, "uTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}