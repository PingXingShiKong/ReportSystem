package com.i1314i.reportsystem.po;

import java.io.Serializable;

public class Major implements Serializable {
    private Integer majorid;

    private String majorname;

    private Integer collegeid;

    public Integer getMajorid() {
        return majorid;
    }

    public void setMajorid(Integer majorid) {
        this.majorid = majorid;
    }

    public String getMajorname() {
        return majorname;
    }

    public void setMajorname(String majorname) {
        this.majorname = majorname == null ? null : majorname.trim();
    }

    public Integer getCollegeid() {
        return collegeid;
    }

    public void setCollegeid(Integer collegeid) {
        this.collegeid = collegeid;
    }

    @Override
    public String toString() {
        return "Major{" +
                "majorid=" + majorid +
                ", majorname='" + majorname + '\'' +
                ", collegeid=" + collegeid +
                '}';
    }
}