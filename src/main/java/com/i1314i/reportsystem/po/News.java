package com.i1314i.reportsystem.po;

import java.io.Serializable;

public class News implements Serializable {
    private Integer id;

    private String title;

    private String description;

    private String url;

    private String inputtime;

    private Integer readpoint;

    private String type;

    private String urlonly;

    private String selected;

    private String content;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public String getInputtime() {
        return inputtime;
    }

    public void setInputtime(String inputtime) {
        this.inputtime = inputtime == null ? null : inputtime.trim();
    }

    public Integer getReadpoint() {
        return readpoint;
    }

    public void setReadpoint(Integer readpoint) {
        this.readpoint = readpoint;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getUrlonly() {
        return urlonly;
    }

    public void setUrlonly(String urlonly) {
        this.urlonly = urlonly == null ? null : urlonly.trim();
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected == null ? null : selected.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}