package com.i1314i.reportsystem.po;

import java.io.Serializable;

public class Problem  implements Serializable {
    private Integer problemid;

    private String answer;

    private Integer price;

    private String a;

    private String b;

    private String c;

    private String d;

    private String problemtext;

    public Integer getProblemid() {
        return problemid;
    }

    public void setProblemid(Integer problemid) {
        this.problemid = problemid;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer == null ? null : answer.trim();
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a == null ? null : a.trim();
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b == null ? null : b.trim();
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c == null ? null : c.trim();
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d == null ? null : d.trim();
    }

    public String getProblemtext() {
        return problemtext;
    }

    public void setProblemtext(String problemtext) {
        this.problemtext = problemtext == null ? null : problemtext.trim();
    }
}