package com.i1314i.reportsystem.po;

import java.io.Serializable;

/**
 * 学院报到率类
 * 学院id，学院名，已报道人数，未报道人数，总人数，报到率
 */
public class ReportsRate implements Serializable {
    private String collegeId;
    private String collegeName;
    private Integer reportednum;
    private Integer noreportednum;
    private Integer totalnum;
    private String rate;

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public ReportsRate(String collegeId, String collegeName, Integer reportednum, Integer noreportednum, Integer totalnum) {
        this.collegeId = collegeId;
        this.collegeName = collegeName;
        this.reportednum = reportednum;
        this.noreportednum = noreportednum;
        this.totalnum = totalnum;
    }

    public ReportsRate() {
    }

    public String getCollegeId() {
        return collegeId;
    }

    public void setCollegeId(String collegeId) {
        this.collegeId = collegeId;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public Integer getReportednum() {
        return reportednum;
    }

    public void setReportednum(Integer reportednum) {
        this.reportednum = reportednum;
    }

    public Integer getNoreportednum() {
        return noreportednum;
    }

    public void setNoreportednum(Integer noreportednum) {
        this.noreportednum = noreportednum;
    }

    public Integer getTotalnum() {
        return totalnum;
    }

    public void setTotalnum(Integer totalnum) {
        this.totalnum = totalnum;
    }
}
