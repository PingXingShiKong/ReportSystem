package com.i1314i.reportsystem.po;

public class SchoolRate {
    int schoolISReport;
    int schoolNOReport;
    int schoolSum;
    double schoolRate;

    int northISReport;
    int northNOReport;
    int northSum;
    double northRate;

    int eastISReport;
    int eastNOReport;
    int eastSum;
    double eastRate;

    int southISReport;
    int southNOReport;
    int southSum;
    double southRate;

    public SchoolRate() {
    }

    public SchoolRate(int schoolISReport, int schoolNOReport, int schoolSum, double schoolRate, int northISReport, int northNOReport, int northSum, double northRate, int eastISReport, int eastNOReport, int eastSum, double eastRate, int southISReport, int southNOReport, int southSum, double southRate) {
        this.schoolISReport = schoolISReport;
        this.schoolNOReport = schoolNOReport;
        this.schoolSum = schoolSum;
        this.schoolRate = schoolRate;
        this.northISReport = northISReport;
        this.northNOReport = northNOReport;
        this.northSum = northSum;
        this.northRate = northRate;
        this.eastISReport = eastISReport;
        this.eastNOReport = eastNOReport;
        this.eastSum = eastSum;
        this.eastRate = eastRate;
        this.southISReport = southISReport;
        this.southNOReport = southNOReport;
        this.southSum = southSum;
        this.southRate = southRate;
    }

    public int getSchoolISReport() {
        return schoolISReport;
    }

    public void setSchoolISReport(int schoolISReport) {
        this.schoolISReport = schoolISReport;
    }

    public int getSchoolNOReport() {
        return schoolNOReport;
    }

    public void setSchoolNOReport(int schoolNOReport) {
        this.schoolNOReport = schoolNOReport;
    }

    public int getSchoolSum() {
        return schoolSum;
    }

    public void setSchoolSum(int schoolSum) {
        this.schoolSum = schoolSum;
    }

    public double getSchoolRate() {
        return schoolRate;
    }

    public void setSchoolRate(double schoolRate) {
        this.schoolRate = schoolRate;
    }

    public int getNorthISReport() {
        return northISReport;
    }

    public void setNorthISReport(int northISReport) {
        this.northISReport = northISReport;
    }

    public int getNorthNOReport() {
        return northNOReport;
    }

    public void setNorthNOReport(int northNOReport) {
        this.northNOReport = northNOReport;
    }

    public int getNorthSum() {
        return northSum;
    }

    public void setNorthSum(int northSum) {
        this.northSum = northSum;
    }

    public double getNorthRate() {
        return northRate;
    }

    public void setNorthRate(double northRate) {
        this.northRate = northRate;
    }

    public int getEastISReport() {
        return eastISReport;
    }

    public void setEastISReport(int eastISReport) {
        this.eastISReport = eastISReport;
    }

    public int getEastNOReport() {
        return eastNOReport;
    }

    public void setEastNOReport(int eastNOReport) {
        this.eastNOReport = eastNOReport;
    }

    public int getEastSum() {
        return eastSum;
    }

    public void setEastSum(int eastSum) {
        this.eastSum = eastSum;
    }

    public double getEastRate() {
        return eastRate;
    }

    public void setEastRate(double eastRate) {
        this.eastRate = eastRate;
    }

    public int getSouthISReport() {
        return southISReport;
    }

    public void setSouthISReport(int southISReport) {
        this.southISReport = southISReport;
    }

    public int getSouthNOReport() {
        return southNOReport;
    }

    public void setSouthNOReport(int southNOReport) {
        this.southNOReport = southNOReport;
    }

    public int getSouthSum() {
        return southSum;
    }

    public void setSouthSum(int southSum) {
        this.southSum = southSum;
    }

    public double getSouthRate() {
        return southRate;
    }

    public void setSouthRate(double southRate) {
        this.southRate = southRate;
    }
}
