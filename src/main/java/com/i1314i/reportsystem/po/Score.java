package com.i1314i.reportsystem.po;

import java.io.Serializable;

public class Score implements Serializable {
    private String sno;

    private Double score;

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno == null ? null : sno.trim();
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }
}