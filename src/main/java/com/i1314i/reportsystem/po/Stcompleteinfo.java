package com.i1314i.reportsystem.po;

import java.io.Serializable;

public class Stcompleteinfo implements Serializable {
    private String sno;

    private String bedding;

    private String phonenum;

    private String parentname;

    private String parentphonenum;

    private String homeaddr;

    private String militaryclothing;

    private Integer shoenum;

    private Integer height;

    private Integer weight;

    private String loan;

    private String trafficway;

    private String registertime;

    private Integer company;

    private String rTime;

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno == null ? null : sno.trim();
    }

    public String getBedding() {
        return bedding;
    }

    public void setBedding(String bedding) {
        this.bedding = bedding == null ? null : bedding.trim();
    }

    public String getPhonenum() {
        return phonenum;
    }

    public void setPhonenum(String phonenum) {
        this.phonenum = phonenum == null ? null : phonenum.trim();
    }

    public String getParentname() {
        return parentname;
    }

    public void setParentname(String parentname) {
        this.parentname = parentname == null ? null : parentname.trim();
    }

    public String getParentphonenum() {
        return parentphonenum;
    }

    public void setParentphonenum(String parentphonenum) {
        this.parentphonenum = parentphonenum == null ? null : parentphonenum.trim();
    }

    public String getHomeaddr() {
        return homeaddr;
    }

    public void setHomeaddr(String homeaddr) {
        this.homeaddr = homeaddr == null ? null : homeaddr.trim();
    }

    public String getMilitaryclothing() {
        return militaryclothing;
    }

    public void setMilitaryclothing(String militaryclothing) {
        this.militaryclothing = militaryclothing == null ? null : militaryclothing.trim();
    }

    public Integer getShoenum() {
        return shoenum;
    }

    public void setShoenum(Integer shoenum) {
        this.shoenum = shoenum;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getLoan() {
        return loan;
    }

    public void setLoan(String loan) {
        this.loan = loan == null ? null : loan.trim();
    }

    public String getTrafficway() {
        return trafficway;
    }

    public void setTrafficway(String trafficway) {
        this.trafficway = trafficway == null ? null : trafficway.trim();
    }

    public String getRegistertime() {
        return registertime;
    }

    public void setRegistertime(String registertime) {
        this.registertime = registertime == null ? null : registertime.trim();
    }

    public Integer getCompany() {
        return company;
    }

    public void setCompany(Integer company) {
        this.company = company;
    }

    public String getrTime() {
        return rTime;
    }

    public void setrTime(String rTime) {
        this.rTime = rTime == null ? null : rTime.trim();
    }
}