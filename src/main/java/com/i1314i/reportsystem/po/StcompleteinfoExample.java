package com.i1314i.reportsystem.po;

import java.util.ArrayList;
import java.util.List;

public class StcompleteinfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public StcompleteinfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andSnoIsNull() {
            addCriterion("sno is null");
            return (Criteria) this;
        }

        public Criteria andSnoIsNotNull() {
            addCriterion("sno is not null");
            return (Criteria) this;
        }

        public Criteria andSnoEqualTo(String value) {
            addCriterion("sno =", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoNotEqualTo(String value) {
            addCriterion("sno <>", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoGreaterThan(String value) {
            addCriterion("sno >", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoGreaterThanOrEqualTo(String value) {
            addCriterion("sno >=", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoLessThan(String value) {
            addCriterion("sno <", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoLessThanOrEqualTo(String value) {
            addCriterion("sno <=", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoLike(String value) {
            addCriterion("sno like", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoNotLike(String value) {
            addCriterion("sno not like", value, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoIn(List<String> values) {
            addCriterion("sno in", values, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoNotIn(List<String> values) {
            addCriterion("sno not in", values, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoBetween(String value1, String value2) {
            addCriterion("sno between", value1, value2, "sno");
            return (Criteria) this;
        }

        public Criteria andSnoNotBetween(String value1, String value2) {
            addCriterion("sno not between", value1, value2, "sno");
            return (Criteria) this;
        }

        public Criteria andBeddingIsNull() {
            addCriterion("bedding is null");
            return (Criteria) this;
        }

        public Criteria andBeddingIsNotNull() {
            addCriterion("bedding is not null");
            return (Criteria) this;
        }

        public Criteria andBeddingEqualTo(String value) {
            addCriterion("bedding =", value, "bedding");
            return (Criteria) this;
        }

        public Criteria andBeddingNotEqualTo(String value) {
            addCriterion("bedding <>", value, "bedding");
            return (Criteria) this;
        }

        public Criteria andBeddingGreaterThan(String value) {
            addCriterion("bedding >", value, "bedding");
            return (Criteria) this;
        }

        public Criteria andBeddingGreaterThanOrEqualTo(String value) {
            addCriterion("bedding >=", value, "bedding");
            return (Criteria) this;
        }

        public Criteria andBeddingLessThan(String value) {
            addCriterion("bedding <", value, "bedding");
            return (Criteria) this;
        }

        public Criteria andBeddingLessThanOrEqualTo(String value) {
            addCriterion("bedding <=", value, "bedding");
            return (Criteria) this;
        }

        public Criteria andBeddingLike(String value) {
            addCriterion("bedding like", value, "bedding");
            return (Criteria) this;
        }

        public Criteria andBeddingNotLike(String value) {
            addCriterion("bedding not like", value, "bedding");
            return (Criteria) this;
        }

        public Criteria andBeddingIn(List<String> values) {
            addCriterion("bedding in", values, "bedding");
            return (Criteria) this;
        }

        public Criteria andBeddingNotIn(List<String> values) {
            addCriterion("bedding not in", values, "bedding");
            return (Criteria) this;
        }

        public Criteria andBeddingBetween(String value1, String value2) {
            addCriterion("bedding between", value1, value2, "bedding");
            return (Criteria) this;
        }

        public Criteria andBeddingNotBetween(String value1, String value2) {
            addCriterion("bedding not between", value1, value2, "bedding");
            return (Criteria) this;
        }

        public Criteria andPhonenumIsNull() {
            addCriterion("phoneNum is null");
            return (Criteria) this;
        }

        public Criteria andPhonenumIsNotNull() {
            addCriterion("phoneNum is not null");
            return (Criteria) this;
        }

        public Criteria andPhonenumEqualTo(String value) {
            addCriterion("phoneNum =", value, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumNotEqualTo(String value) {
            addCriterion("phoneNum <>", value, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumGreaterThan(String value) {
            addCriterion("phoneNum >", value, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumGreaterThanOrEqualTo(String value) {
            addCriterion("phoneNum >=", value, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumLessThan(String value) {
            addCriterion("phoneNum <", value, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumLessThanOrEqualTo(String value) {
            addCriterion("phoneNum <=", value, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumLike(String value) {
            addCriterion("phoneNum like", value, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumNotLike(String value) {
            addCriterion("phoneNum not like", value, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumIn(List<String> values) {
            addCriterion("phoneNum in", values, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumNotIn(List<String> values) {
            addCriterion("phoneNum not in", values, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumBetween(String value1, String value2) {
            addCriterion("phoneNum between", value1, value2, "phonenum");
            return (Criteria) this;
        }

        public Criteria andPhonenumNotBetween(String value1, String value2) {
            addCriterion("phoneNum not between", value1, value2, "phonenum");
            return (Criteria) this;
        }

        public Criteria andParentnameIsNull() {
            addCriterion("parentName is null");
            return (Criteria) this;
        }

        public Criteria andParentnameIsNotNull() {
            addCriterion("parentName is not null");
            return (Criteria) this;
        }

        public Criteria andParentnameEqualTo(String value) {
            addCriterion("parentName =", value, "parentname");
            return (Criteria) this;
        }

        public Criteria andParentnameNotEqualTo(String value) {
            addCriterion("parentName <>", value, "parentname");
            return (Criteria) this;
        }

        public Criteria andParentnameGreaterThan(String value) {
            addCriterion("parentName >", value, "parentname");
            return (Criteria) this;
        }

        public Criteria andParentnameGreaterThanOrEqualTo(String value) {
            addCriterion("parentName >=", value, "parentname");
            return (Criteria) this;
        }

        public Criteria andParentnameLessThan(String value) {
            addCriterion("parentName <", value, "parentname");
            return (Criteria) this;
        }

        public Criteria andParentnameLessThanOrEqualTo(String value) {
            addCriterion("parentName <=", value, "parentname");
            return (Criteria) this;
        }

        public Criteria andParentnameLike(String value) {
            addCriterion("parentName like", value, "parentname");
            return (Criteria) this;
        }

        public Criteria andParentnameNotLike(String value) {
            addCriterion("parentName not like", value, "parentname");
            return (Criteria) this;
        }

        public Criteria andParentnameIn(List<String> values) {
            addCriterion("parentName in", values, "parentname");
            return (Criteria) this;
        }

        public Criteria andParentnameNotIn(List<String> values) {
            addCriterion("parentName not in", values, "parentname");
            return (Criteria) this;
        }

        public Criteria andParentnameBetween(String value1, String value2) {
            addCriterion("parentName between", value1, value2, "parentname");
            return (Criteria) this;
        }

        public Criteria andParentnameNotBetween(String value1, String value2) {
            addCriterion("parentName not between", value1, value2, "parentname");
            return (Criteria) this;
        }

        public Criteria andParentphonenumIsNull() {
            addCriterion("parentPhoneNum is null");
            return (Criteria) this;
        }

        public Criteria andParentphonenumIsNotNull() {
            addCriterion("parentPhoneNum is not null");
            return (Criteria) this;
        }

        public Criteria andParentphonenumEqualTo(String value) {
            addCriterion("parentPhoneNum =", value, "parentphonenum");
            return (Criteria) this;
        }

        public Criteria andParentphonenumNotEqualTo(String value) {
            addCriterion("parentPhoneNum <>", value, "parentphonenum");
            return (Criteria) this;
        }

        public Criteria andParentphonenumGreaterThan(String value) {
            addCriterion("parentPhoneNum >", value, "parentphonenum");
            return (Criteria) this;
        }

        public Criteria andParentphonenumGreaterThanOrEqualTo(String value) {
            addCriterion("parentPhoneNum >=", value, "parentphonenum");
            return (Criteria) this;
        }

        public Criteria andParentphonenumLessThan(String value) {
            addCriterion("parentPhoneNum <", value, "parentphonenum");
            return (Criteria) this;
        }

        public Criteria andParentphonenumLessThanOrEqualTo(String value) {
            addCriterion("parentPhoneNum <=", value, "parentphonenum");
            return (Criteria) this;
        }

        public Criteria andParentphonenumLike(String value) {
            addCriterion("parentPhoneNum like", value, "parentphonenum");
            return (Criteria) this;
        }

        public Criteria andParentphonenumNotLike(String value) {
            addCriterion("parentPhoneNum not like", value, "parentphonenum");
            return (Criteria) this;
        }

        public Criteria andParentphonenumIn(List<String> values) {
            addCriterion("parentPhoneNum in", values, "parentphonenum");
            return (Criteria) this;
        }

        public Criteria andParentphonenumNotIn(List<String> values) {
            addCriterion("parentPhoneNum not in", values, "parentphonenum");
            return (Criteria) this;
        }

        public Criteria andParentphonenumBetween(String value1, String value2) {
            addCriterion("parentPhoneNum between", value1, value2, "parentphonenum");
            return (Criteria) this;
        }

        public Criteria andParentphonenumNotBetween(String value1, String value2) {
            addCriterion("parentPhoneNum not between", value1, value2, "parentphonenum");
            return (Criteria) this;
        }

        public Criteria andHomeaddrIsNull() {
            addCriterion("homeAddr is null");
            return (Criteria) this;
        }

        public Criteria andHomeaddrIsNotNull() {
            addCriterion("homeAddr is not null");
            return (Criteria) this;
        }

        public Criteria andHomeaddrEqualTo(String value) {
            addCriterion("homeAddr =", value, "homeaddr");
            return (Criteria) this;
        }

        public Criteria andHomeaddrNotEqualTo(String value) {
            addCriterion("homeAddr <>", value, "homeaddr");
            return (Criteria) this;
        }

        public Criteria andHomeaddrGreaterThan(String value) {
            addCriterion("homeAddr >", value, "homeaddr");
            return (Criteria) this;
        }

        public Criteria andHomeaddrGreaterThanOrEqualTo(String value) {
            addCriterion("homeAddr >=", value, "homeaddr");
            return (Criteria) this;
        }

        public Criteria andHomeaddrLessThan(String value) {
            addCriterion("homeAddr <", value, "homeaddr");
            return (Criteria) this;
        }

        public Criteria andHomeaddrLessThanOrEqualTo(String value) {
            addCriterion("homeAddr <=", value, "homeaddr");
            return (Criteria) this;
        }

        public Criteria andHomeaddrLike(String value) {
            addCriterion("homeAddr like", value, "homeaddr");
            return (Criteria) this;
        }

        public Criteria andHomeaddrNotLike(String value) {
            addCriterion("homeAddr not like", value, "homeaddr");
            return (Criteria) this;
        }

        public Criteria andHomeaddrIn(List<String> values) {
            addCriterion("homeAddr in", values, "homeaddr");
            return (Criteria) this;
        }

        public Criteria andHomeaddrNotIn(List<String> values) {
            addCriterion("homeAddr not in", values, "homeaddr");
            return (Criteria) this;
        }

        public Criteria andHomeaddrBetween(String value1, String value2) {
            addCriterion("homeAddr between", value1, value2, "homeaddr");
            return (Criteria) this;
        }

        public Criteria andHomeaddrNotBetween(String value1, String value2) {
            addCriterion("homeAddr not between", value1, value2, "homeaddr");
            return (Criteria) this;
        }

        public Criteria andMilitaryclothingIsNull() {
            addCriterion("militaryClothing is null");
            return (Criteria) this;
        }

        public Criteria andMilitaryclothingIsNotNull() {
            addCriterion("militaryClothing is not null");
            return (Criteria) this;
        }

        public Criteria andMilitaryclothingEqualTo(String value) {
            addCriterion("militaryClothing =", value, "militaryclothing");
            return (Criteria) this;
        }

        public Criteria andMilitaryclothingNotEqualTo(String value) {
            addCriterion("militaryClothing <>", value, "militaryclothing");
            return (Criteria) this;
        }

        public Criteria andMilitaryclothingGreaterThan(String value) {
            addCriterion("militaryClothing >", value, "militaryclothing");
            return (Criteria) this;
        }

        public Criteria andMilitaryclothingGreaterThanOrEqualTo(String value) {
            addCriterion("militaryClothing >=", value, "militaryclothing");
            return (Criteria) this;
        }

        public Criteria andMilitaryclothingLessThan(String value) {
            addCriterion("militaryClothing <", value, "militaryclothing");
            return (Criteria) this;
        }

        public Criteria andMilitaryclothingLessThanOrEqualTo(String value) {
            addCriterion("militaryClothing <=", value, "militaryclothing");
            return (Criteria) this;
        }

        public Criteria andMilitaryclothingLike(String value) {
            addCriterion("militaryClothing like", value, "militaryclothing");
            return (Criteria) this;
        }

        public Criteria andMilitaryclothingNotLike(String value) {
            addCriterion("militaryClothing not like", value, "militaryclothing");
            return (Criteria) this;
        }

        public Criteria andMilitaryclothingIn(List<String> values) {
            addCriterion("militaryClothing in", values, "militaryclothing");
            return (Criteria) this;
        }

        public Criteria andMilitaryclothingNotIn(List<String> values) {
            addCriterion("militaryClothing not in", values, "militaryclothing");
            return (Criteria) this;
        }

        public Criteria andMilitaryclothingBetween(String value1, String value2) {
            addCriterion("militaryClothing between", value1, value2, "militaryclothing");
            return (Criteria) this;
        }

        public Criteria andMilitaryclothingNotBetween(String value1, String value2) {
            addCriterion("militaryClothing not between", value1, value2, "militaryclothing");
            return (Criteria) this;
        }

        public Criteria andShoenumIsNull() {
            addCriterion("shoeNum is null");
            return (Criteria) this;
        }

        public Criteria andShoenumIsNotNull() {
            addCriterion("shoeNum is not null");
            return (Criteria) this;
        }

        public Criteria andShoenumEqualTo(Integer value) {
            addCriterion("shoeNum =", value, "shoenum");
            return (Criteria) this;
        }

        public Criteria andShoenumNotEqualTo(Integer value) {
            addCriterion("shoeNum <>", value, "shoenum");
            return (Criteria) this;
        }

        public Criteria andShoenumGreaterThan(Integer value) {
            addCriterion("shoeNum >", value, "shoenum");
            return (Criteria) this;
        }

        public Criteria andShoenumGreaterThanOrEqualTo(Integer value) {
            addCriterion("shoeNum >=", value, "shoenum");
            return (Criteria) this;
        }

        public Criteria andShoenumLessThan(Integer value) {
            addCriterion("shoeNum <", value, "shoenum");
            return (Criteria) this;
        }

        public Criteria andShoenumLessThanOrEqualTo(Integer value) {
            addCriterion("shoeNum <=", value, "shoenum");
            return (Criteria) this;
        }

        public Criteria andShoenumIn(List<Integer> values) {
            addCriterion("shoeNum in", values, "shoenum");
            return (Criteria) this;
        }

        public Criteria andShoenumNotIn(List<Integer> values) {
            addCriterion("shoeNum not in", values, "shoenum");
            return (Criteria) this;
        }

        public Criteria andShoenumBetween(Integer value1, Integer value2) {
            addCriterion("shoeNum between", value1, value2, "shoenum");
            return (Criteria) this;
        }

        public Criteria andShoenumNotBetween(Integer value1, Integer value2) {
            addCriterion("shoeNum not between", value1, value2, "shoenum");
            return (Criteria) this;
        }

        public Criteria andHeightIsNull() {
            addCriterion("height is null");
            return (Criteria) this;
        }

        public Criteria andHeightIsNotNull() {
            addCriterion("height is not null");
            return (Criteria) this;
        }

        public Criteria andHeightEqualTo(Integer value) {
            addCriterion("height =", value, "height");
            return (Criteria) this;
        }

        public Criteria andHeightNotEqualTo(Integer value) {
            addCriterion("height <>", value, "height");
            return (Criteria) this;
        }

        public Criteria andHeightGreaterThan(Integer value) {
            addCriterion("height >", value, "height");
            return (Criteria) this;
        }

        public Criteria andHeightGreaterThanOrEqualTo(Integer value) {
            addCriterion("height >=", value, "height");
            return (Criteria) this;
        }

        public Criteria andHeightLessThan(Integer value) {
            addCriterion("height <", value, "height");
            return (Criteria) this;
        }

        public Criteria andHeightLessThanOrEqualTo(Integer value) {
            addCriterion("height <=", value, "height");
            return (Criteria) this;
        }

        public Criteria andHeightIn(List<Integer> values) {
            addCriterion("height in", values, "height");
            return (Criteria) this;
        }

        public Criteria andHeightNotIn(List<Integer> values) {
            addCriterion("height not in", values, "height");
            return (Criteria) this;
        }

        public Criteria andHeightBetween(Integer value1, Integer value2) {
            addCriterion("height between", value1, value2, "height");
            return (Criteria) this;
        }

        public Criteria andHeightNotBetween(Integer value1, Integer value2) {
            addCriterion("height not between", value1, value2, "height");
            return (Criteria) this;
        }

        public Criteria andWeightIsNull() {
            addCriterion("weight is null");
            return (Criteria) this;
        }

        public Criteria andWeightIsNotNull() {
            addCriterion("weight is not null");
            return (Criteria) this;
        }

        public Criteria andWeightEqualTo(Integer value) {
            addCriterion("weight =", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotEqualTo(Integer value) {
            addCriterion("weight <>", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightGreaterThan(Integer value) {
            addCriterion("weight >", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightGreaterThanOrEqualTo(Integer value) {
            addCriterion("weight >=", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightLessThan(Integer value) {
            addCriterion("weight <", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightLessThanOrEqualTo(Integer value) {
            addCriterion("weight <=", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightIn(List<Integer> values) {
            addCriterion("weight in", values, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotIn(List<Integer> values) {
            addCriterion("weight not in", values, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightBetween(Integer value1, Integer value2) {
            addCriterion("weight between", value1, value2, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotBetween(Integer value1, Integer value2) {
            addCriterion("weight not between", value1, value2, "weight");
            return (Criteria) this;
        }

        public Criteria andLoanIsNull() {
            addCriterion("loan is null");
            return (Criteria) this;
        }

        public Criteria andLoanIsNotNull() {
            addCriterion("loan is not null");
            return (Criteria) this;
        }

        public Criteria andLoanEqualTo(String value) {
            addCriterion("loan =", value, "loan");
            return (Criteria) this;
        }

        public Criteria andLoanNotEqualTo(String value) {
            addCriterion("loan <>", value, "loan");
            return (Criteria) this;
        }

        public Criteria andLoanGreaterThan(String value) {
            addCriterion("loan >", value, "loan");
            return (Criteria) this;
        }

        public Criteria andLoanGreaterThanOrEqualTo(String value) {
            addCriterion("loan >=", value, "loan");
            return (Criteria) this;
        }

        public Criteria andLoanLessThan(String value) {
            addCriterion("loan <", value, "loan");
            return (Criteria) this;
        }

        public Criteria andLoanLessThanOrEqualTo(String value) {
            addCriterion("loan <=", value, "loan");
            return (Criteria) this;
        }

        public Criteria andLoanLike(String value) {
            addCriterion("loan like", value, "loan");
            return (Criteria) this;
        }

        public Criteria andLoanNotLike(String value) {
            addCriterion("loan not like", value, "loan");
            return (Criteria) this;
        }

        public Criteria andLoanIn(List<String> values) {
            addCriterion("loan in", values, "loan");
            return (Criteria) this;
        }

        public Criteria andLoanNotIn(List<String> values) {
            addCriterion("loan not in", values, "loan");
            return (Criteria) this;
        }

        public Criteria andLoanBetween(String value1, String value2) {
            addCriterion("loan between", value1, value2, "loan");
            return (Criteria) this;
        }

        public Criteria andLoanNotBetween(String value1, String value2) {
            addCriterion("loan not between", value1, value2, "loan");
            return (Criteria) this;
        }

        public Criteria andTrafficwayIsNull() {
            addCriterion("trafficWay is null");
            return (Criteria) this;
        }

        public Criteria andTrafficwayIsNotNull() {
            addCriterion("trafficWay is not null");
            return (Criteria) this;
        }

        public Criteria andTrafficwayEqualTo(String value) {
            addCriterion("trafficWay =", value, "trafficway");
            return (Criteria) this;
        }

        public Criteria andTrafficwayNotEqualTo(String value) {
            addCriterion("trafficWay <>", value, "trafficway");
            return (Criteria) this;
        }

        public Criteria andTrafficwayGreaterThan(String value) {
            addCriterion("trafficWay >", value, "trafficway");
            return (Criteria) this;
        }

        public Criteria andTrafficwayGreaterThanOrEqualTo(String value) {
            addCriterion("trafficWay >=", value, "trafficway");
            return (Criteria) this;
        }

        public Criteria andTrafficwayLessThan(String value) {
            addCriterion("trafficWay <", value, "trafficway");
            return (Criteria) this;
        }

        public Criteria andTrafficwayLessThanOrEqualTo(String value) {
            addCriterion("trafficWay <=", value, "trafficway");
            return (Criteria) this;
        }

        public Criteria andTrafficwayLike(String value) {
            addCriterion("trafficWay like", value, "trafficway");
            return (Criteria) this;
        }

        public Criteria andTrafficwayNotLike(String value) {
            addCriterion("trafficWay not like", value, "trafficway");
            return (Criteria) this;
        }

        public Criteria andTrafficwayIn(List<String> values) {
            addCriterion("trafficWay in", values, "trafficway");
            return (Criteria) this;
        }

        public Criteria andTrafficwayNotIn(List<String> values) {
            addCriterion("trafficWay not in", values, "trafficway");
            return (Criteria) this;
        }

        public Criteria andTrafficwayBetween(String value1, String value2) {
            addCriterion("trafficWay between", value1, value2, "trafficway");
            return (Criteria) this;
        }

        public Criteria andTrafficwayNotBetween(String value1, String value2) {
            addCriterion("trafficWay not between", value1, value2, "trafficway");
            return (Criteria) this;
        }

        public Criteria andRegistertimeIsNull() {
            addCriterion("registerTime is null");
            return (Criteria) this;
        }

        public Criteria andRegistertimeIsNotNull() {
            addCriterion("registerTime is not null");
            return (Criteria) this;
        }

        public Criteria andRegistertimeEqualTo(String value) {
            addCriterion("registerTime =", value, "registertime");
            return (Criteria) this;
        }

        public Criteria andRegistertimeNotEqualTo(String value) {
            addCriterion("registerTime <>", value, "registertime");
            return (Criteria) this;
        }

        public Criteria andRegistertimeGreaterThan(String value) {
            addCriterion("registerTime >", value, "registertime");
            return (Criteria) this;
        }

        public Criteria andRegistertimeGreaterThanOrEqualTo(String value) {
            addCriterion("registerTime >=", value, "registertime");
            return (Criteria) this;
        }

        public Criteria andRegistertimeLessThan(String value) {
            addCriterion("registerTime <", value, "registertime");
            return (Criteria) this;
        }

        public Criteria andRegistertimeLessThanOrEqualTo(String value) {
            addCriterion("registerTime <=", value, "registertime");
            return (Criteria) this;
        }

        public Criteria andRegistertimeLike(String value) {
            addCriterion("registerTime like", value, "registertime");
            return (Criteria) this;
        }

        public Criteria andRegistertimeNotLike(String value) {
            addCriterion("registerTime not like", value, "registertime");
            return (Criteria) this;
        }

        public Criteria andRegistertimeIn(List<String> values) {
            addCriterion("registerTime in", values, "registertime");
            return (Criteria) this;
        }

        public Criteria andRegistertimeNotIn(List<String> values) {
            addCriterion("registerTime not in", values, "registertime");
            return (Criteria) this;
        }

        public Criteria andRegistertimeBetween(String value1, String value2) {
            addCriterion("registerTime between", value1, value2, "registertime");
            return (Criteria) this;
        }

        public Criteria andRegistertimeNotBetween(String value1, String value2) {
            addCriterion("registerTime not between", value1, value2, "registertime");
            return (Criteria) this;
        }

        public Criteria andCompanyIsNull() {
            addCriterion("company is null");
            return (Criteria) this;
        }

        public Criteria andCompanyIsNotNull() {
            addCriterion("company is not null");
            return (Criteria) this;
        }

        public Criteria andCompanyEqualTo(Integer value) {
            addCriterion("company =", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyNotEqualTo(Integer value) {
            addCriterion("company <>", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyGreaterThan(Integer value) {
            addCriterion("company >", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyGreaterThanOrEqualTo(Integer value) {
            addCriterion("company >=", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyLessThan(Integer value) {
            addCriterion("company <", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyLessThanOrEqualTo(Integer value) {
            addCriterion("company <=", value, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyIn(List<Integer> values) {
            addCriterion("company in", values, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyNotIn(List<Integer> values) {
            addCriterion("company not in", values, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyBetween(Integer value1, Integer value2) {
            addCriterion("company between", value1, value2, "company");
            return (Criteria) this;
        }

        public Criteria andCompanyNotBetween(Integer value1, Integer value2) {
            addCriterion("company not between", value1, value2, "company");
            return (Criteria) this;
        }

        public Criteria andRTimeIsNull() {
            addCriterion("r_time is null");
            return (Criteria) this;
        }

        public Criteria andRTimeIsNotNull() {
            addCriterion("r_time is not null");
            return (Criteria) this;
        }

        public Criteria andRTimeEqualTo(String value) {
            addCriterion("r_time =", value, "rTime");
            return (Criteria) this;
        }

        public Criteria andRTimeNotEqualTo(String value) {
            addCriterion("r_time <>", value, "rTime");
            return (Criteria) this;
        }

        public Criteria andRTimeGreaterThan(String value) {
            addCriterion("r_time >", value, "rTime");
            return (Criteria) this;
        }

        public Criteria andRTimeGreaterThanOrEqualTo(String value) {
            addCriterion("r_time >=", value, "rTime");
            return (Criteria) this;
        }

        public Criteria andRTimeLessThan(String value) {
            addCriterion("r_time <", value, "rTime");
            return (Criteria) this;
        }

        public Criteria andRTimeLessThanOrEqualTo(String value) {
            addCriterion("r_time <=", value, "rTime");
            return (Criteria) this;
        }

        public Criteria andRTimeLike(String value) {
            addCriterion("r_time like", value, "rTime");
            return (Criteria) this;
        }

        public Criteria andRTimeNotLike(String value) {
            addCriterion("r_time not like", value, "rTime");
            return (Criteria) this;
        }

        public Criteria andRTimeIn(List<String> values) {
            addCriterion("r_time in", values, "rTime");
            return (Criteria) this;
        }

        public Criteria andRTimeNotIn(List<String> values) {
            addCriterion("r_time not in", values, "rTime");
            return (Criteria) this;
        }

        public Criteria andRTimeBetween(String value1, String value2) {
            addCriterion("r_time between", value1, value2, "rTime");
            return (Criteria) this;
        }

        public Criteria andRTimeNotBetween(String value1, String value2) {
            addCriterion("r_time not between", value1, value2, "rTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}