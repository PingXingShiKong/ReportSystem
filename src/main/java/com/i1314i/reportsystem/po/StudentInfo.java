package com.i1314i.reportsystem.po;

import java.io.Serializable;

/**
 * @author 平行时空
 * @created 2018-05-13 16:13
 **/
public class StudentInfo implements Serializable {

    private Basicinfo basicinfo;
    private Stcompleteinfo stcompleteinfo;


    public Basicinfo getBasicinfo() {
        return basicinfo;
    }

    public void setBasicinfo(Basicinfo basicinfo) {
        this.basicinfo = basicinfo;
    }

    public Stcompleteinfo getStcompleteinfo() {
        return stcompleteinfo;
    }

    public void setStcompleteinfo(Stcompleteinfo stcompleteinfo) {
        this.stcompleteinfo = stcompleteinfo;
    }
}
