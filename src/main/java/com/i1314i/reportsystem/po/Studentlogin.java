package com.i1314i.reportsystem.po;

import java.io.Serializable;

public class Studentlogin implements Serializable {
    private String sno;

    private String password;

    private String code;

    private String newphone;


    public String getNewphone() {
        return newphone;
    }

    public void setNewphone(String newphone) {
        this.newphone = newphone;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno == null ? null : sno.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

