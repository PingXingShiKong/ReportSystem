package com.i1314i.reportsystem.po.vo;

import java.io.Serializable;

public class ClassRegister implements Serializable {
    private String classNum;

    private Integer headcount;

    private Integer bdcount;

    private Double register;

    public Integer getHeadcount() {
        return headcount;
    }

    public void setHeadcount(Integer headcount) {
        this.headcount = headcount;
    }

    public Integer getBdcount() {
        return bdcount;
    }

    public void setBdcount(Integer bdcount) {
        this.bdcount = bdcount;
    }

    public Double getRegister() {
        return register;
    }

    public void setRegister(Double register) {
        this.register = register;
    }

    public String getClassNum() {
        return classNum;
    }

    public void setClassNum(String classNum) {
        this.classNum = classNum;
    }
}
