package com.i1314i.reportsystem.po.vo;

import java.io.Serializable;

public class CollegeRegister implements Serializable {
    private Integer majorid;

    private String majorname;

    private Integer headcount;

    private Integer bdcount;

    private Double register;

    public Integer getMajorid() {
        return majorid;
    }

    public void setMajorid(Integer majorid) {
        this.majorid = majorid;
    }

    public String getMajorname() {
        return majorname;
    }

    public void setMajorname(String majorname) {
        this.majorname = majorname;
    }

    public Integer getHeadcount() {
        return headcount;
    }

    public void setHeadcount(Integer headcount) {
        this.headcount = headcount;
    }

    public Integer getBdcount() {
        return bdcount;
    }

    public void setBdcount(Integer bdcount) {
        this.bdcount = bdcount;
    }

    public Double getRegister() {
        return register;
    }

    public void setRegister(Double register) {
        this.register = register;
    }

}

