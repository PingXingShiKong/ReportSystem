package com.i1314i.reportsystem.service;

import com.i1314i.reportsystem.po.Basicinfo;
import com.i1314i.reportsystem.service.exception.StudentException;

import javax.servlet.http.HttpServletRequest;

public interface CollegeSideService {
    boolean noSpotRegister(String userId,String fromUserName, String rematkText, Boolean flag) throws StudentException;

    Basicinfo checkFreshman(String userId, String fromUserName, Boolean flag) throws StudentException;

    int setCollegeId(String userId, HttpServletRequest request) throws StudentException;
}
