package com.i1314i.reportsystem.service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public interface IdentityService {

    Boolean identity(String state, String userId, HttpServletRequest request);
    String getUserId(String code, String state, HttpServletRequest request) throws IOException;
    String getTekon(String agentid, String secret) throws IOException;
}