package com.i1314i.reportsystem.service;

import com.i1314i.reportsystem.po.Studentlogin;
import com.i1314i.reportsystem.service.exception.LoginException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author 平行时空
 * @created 2018-05-12 15:54
 **/
public interface LoginService {
    Integer isStudentByNo(Studentlogin sdata,HttpSession session,HttpServletRequest request) throws LoginException, IOException;
}
