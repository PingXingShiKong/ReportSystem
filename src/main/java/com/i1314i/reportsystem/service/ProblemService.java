package com.i1314i.reportsystem.service;

import com.github.pagehelper.PageInfo;
import com.i1314i.reportsystem.po.Problem;
import com.i1314i.reportsystem.po.pageHelper.PageBean;
import com.i1314i.reportsystem.service.exception.ProblemException;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author 平行时空
 * @created 2018-06-06 8:06
 **/
public interface ProblemService {
    double getScore(String sno);
    boolean isJump(String sno) throws IOException;
    List<Problem> getProblems(String sno) throws IOException, ProblemException;
    double[]  getScore(Map<Integer,String>exams,String sno) throws IOException, ProblemException;
    List<Problem>wechatGetAnswers(String sno) throws IOException, ProblemException;

    /**
     * 获取所有试题
     * @return
     * @throws Exception
     */
    PageInfo<Problem> getAllProblems(PageBean pageBean)throws Exception;
}
