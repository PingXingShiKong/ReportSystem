package com.i1314i.reportsystem.service;


import com.i1314i.reportsystem.service.exception.StudentException;

import java.io.IOException;

public interface ScanQRCodeService {
    String processRequest(String request);
    Boolean postTextMsg(String userId, String content);
    Boolean iscollege(String fromUserName, String college) throws StudentException;
    String responseMsg(String userId,String qrto, String formUserName) throws IOException;

}
