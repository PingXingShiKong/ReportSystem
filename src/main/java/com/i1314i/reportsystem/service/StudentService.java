package com.i1314i.reportsystem.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.i1314i.reportsystem.po.*;
import com.i1314i.reportsystem.service.exception.StudentException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * @author 平行时空
 * @created 2018-05-12 16:37
 **/
public interface StudentService {
    JsonResult getInfoBySnoResult(HttpServletRequest request) throws StudentException, IOException;
    Basicinfo findStudentDataBySno(String sno) throws IOException;
    JsonResult updateStcompleteinfo(Stcompleteinfo stcompleteinfo,HttpServletRequest request) throws StudentException, JsonProcessingException;
    JsonResult getStudentStcompleteinfos(HttpServletRequest request) throws StudentException, IOException;
    boolean isspotRegister(String userId) throws StudentException;
    JsonResult getGrade(List<Problem> problems);
    JsonResult changelogininfo(ChangeStudentLogin studentLogin);
    JsonResult selectStudentIsGetSchool(Basicinfo basicinfo) throws IOException;
    JsonResult studentPublicInfo(String type);
}
