package com.i1314i.reportsystem.service;

import net.sf.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public interface StudentSideService {
    JSONObject getMPerson(String userId, String state) throws IOException;
    String phoneChange(String userId, String mobile, String state) throws IOException;
}
