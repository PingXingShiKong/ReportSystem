package com.i1314i.reportsystem.service.admin;

import com.i1314i.reportsystem.po.Basicinfo;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class DownloadTemplateService {
    public HSSFWorkbook export(String[] information) {
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("新生信息状态");
        HSSFRow row = sheet.createRow((int) 0);
        for (int j=1;j<information.length;j++){
            sheet.setColumnWidth(j, 10 * 256);
        }
        HSSFCellStyle style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        for (int i = 0; i < information.length; i++) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue(information[i]);
            cell.setCellStyle(style);
//            sheet.autoSizeColumn(i);
        }

        return wb;
    }
}
