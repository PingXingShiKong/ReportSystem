package com.i1314i.reportsystem.service.admin;

/**
 * 选择并导出学生信息对应实体类
 */
public class Exportpo {
    private String sno;

    private String name;

    private String sex;

    private String examnum;

    private String idnum;

    private String campus;

    private String college;

    private String major;

    private String classnum;

    private String dormnum;

    private String bankcardid;

    private String paysta;

    private String collegetel;

    private String schooltel;

    private String mastername;

    private String mastertel;

    private String brother;

    private String brothertel;

    private String helper;

    private String helpertel;

    private String spotregister;

    private String reason;

    private String remarkOne;

    private String remarkTwo;
    private String bedding;

    private String phonenum;

    private String parentname;

    private String parentphonenum;

    private String homeaddr;

    private String militaryclothing;

    private String shoenum;

    private String height;

    private String weight;

    private String loan;

    private String trafficway;

    private String registertime;

    private String company;

    private String rTime;

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getExamnum() {
        return examnum;
    }

    public void setExamnum(String examnum) {
        this.examnum = examnum;
    }

    public String getIdnum() {
        return idnum;
    }

    public void setIdnum(String idnum) {
        this.idnum = idnum;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getClassnum() {
        return classnum;
    }

    public void setClassnum(String classnum) {
        this.classnum = classnum;
    }

    public String getDormnum() {
        return dormnum;
    }

    public void setDormnum(String dormnum) {
        this.dormnum = dormnum;
    }

    public String getBankcardid() {
        return bankcardid;
    }

    public void setBankcardid(String bankcardid) {
        this.bankcardid = bankcardid;
    }

    public String getPaysta() {
        return paysta;
    }

    public void setPaysta(String paysta) {
        this.paysta = paysta;
    }

    public String getCollegetel() {
        return collegetel;
    }

    public void setCollegetel(String collegetel) {
        this.collegetel = collegetel;
    }

    public String getSchooltel() {
        return schooltel;
    }

    public void setSchooltel(String schooltel) {
        this.schooltel = schooltel;
    }

    public String getMastername() {
        return mastername;
    }

    public void setMastername(String mastername) {
        this.mastername = mastername;
    }

    public String getMastertel() {
        return mastertel;
    }

    public void setMastertel(String mastertel) {
        this.mastertel = mastertel;
    }

    public String getBrother() {
        return brother;
    }

    public void setBrother(String brother) {
        this.brother = brother;
    }

    public String getBrothertel() {
        return brothertel;
    }

    public void setBrothertel(String brothertel) {
        this.brothertel = brothertel;
    }

    public String getHelper() {
        return helper;
    }

    public void setHelper(String helper) {
        this.helper = helper;
    }

    public String getHelpertel() {
        return helpertel;
    }

    public void setHelpertel(String helpertel) {
        this.helpertel = helpertel;
    }

    public String getSpotregister() {
        return spotregister;
    }

    public void setSpotregister(String spotregister) {
        this.spotregister = spotregister;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getRemarkOne() {
        return remarkOne;
    }

    public void setRemarkOne(String remarkOne) {
        this.remarkOne = remarkOne;
    }

    public String getRemarkTwo() {
        return remarkTwo;
    }

    public void setRemarkTwo(String remarkTwo) {
        this.remarkTwo = remarkTwo;
    }

    public String getBedding() {
        return bedding;
    }

    public void setBedding(String bedding) {
        this.bedding = bedding;
    }

    public String getPhonenum() {
        return phonenum;
    }

    public void setPhonenum(String phonenum) {
        this.phonenum = phonenum;
    }

    public String getParentname() {
        return parentname;
    }

    public void setParentname(String parentname) {
        this.parentname = parentname;
    }

    public String getParentphonenum() {
        return parentphonenum;
    }

    public void setParentphonenum(String parentphonenum) {
        this.parentphonenum = parentphonenum;
    }

    public String getHomeaddr() {
        return homeaddr;
    }

    public void setHomeaddr(String homeaddr) {
        this.homeaddr = homeaddr;
    }

    public String getMilitaryclothing() {
        return militaryclothing;
    }

    public void setMilitaryclothing(String militaryclothing) {
        this.militaryclothing = militaryclothing;
    }

    public String getShoenum() {
        return shoenum;
    }

    public void setShoenum(String shoenum) {
        this.shoenum = shoenum;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getLoan() {
        return loan;
    }

    public void setLoan(String loan) {
        this.loan = loan;
    }

    public String getTrafficway() {
        return trafficway;
    }

    public void setTrafficway(String trafficway) {
        this.trafficway = trafficway;
    }

    public String getRegistertime() {
        return registertime;
    }

    public void setRegistertime(String registertime) {
        this.registertime = registertime;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getrTime() {
        return rTime;
    }

    public void setrTime(String rTime) {
        this.rTime = rTime;
    }
}
