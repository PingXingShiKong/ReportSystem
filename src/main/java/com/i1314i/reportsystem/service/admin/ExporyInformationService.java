package com.i1314i.reportsystem.service.admin;

import com.i1314i.reportsystem.po.Basicinfo;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ExporyInformationService {
//    String[] excelHeader = {"学号", "姓名", "学院", "报道状态", "未报道理由"};

    public HSSFWorkbook export(Exportpo exportpo, List<Exportpo> exportpoList) {
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("新生信息");
        HSSFRow row = sheet.createRow((int) 0);
        HSSFCellStyle style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
//        for (int i = 0; i < excelHeader.length; i++) {
//            HSSFCell cell = row.createCell(i);
//            cell.setCellValue(excelHeader[i]);
//            cell.setCellStyle(style);
////            sheet.autoSizeColumn(i);
//        }
        int i = 0;
        if (exportpo.getSno() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("学号");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getName() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("姓名");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getSex() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("性别");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getExamnum() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("考生号");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getIdnum() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("身份证号");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getCampus() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("校区");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getCollege() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("学院");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getMajor() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("录取专业");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getClassnum() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("班级");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getDormnum() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("宿舍");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getBankcardid() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("银行卡号");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getPaysta() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("缴费情况");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getCollegetel() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("学院联系方式");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getSchooltel() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("学校联系方式");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getMastername() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("辅导员");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getMastertel() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("辅导员电话");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getBrother() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("班主任");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getBrothertel() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("班主任联系方式");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getHelper() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("班助");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getHelpertel() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("班助联系方式");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getSpotregister() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("是否报到");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getReason() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("未报到原因");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getRemarkOne() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("备注1");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getRemarkTwo() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("备注2");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getBedding() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("是否需要卧具");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getPhonenum() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("学生联系方式");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getParentname() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("家长姓名");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getParentphonenum() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("家长联系方式");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getHomeaddr() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("家庭详细地址");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getMilitaryclothing() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("是否购买军需服装");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getShoenum() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("鞋号");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getHeight() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("身高");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getWeight() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("体重");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getLoan() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("是否办理生源地助学贷款");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getTrafficway() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("到校方式");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getRegistertime() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("预计报到时间");
            cell.setCellStyle(style);

            i++;
        }
        if (exportpo.getCompany() != null) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue("同行人数");
            cell.setCellStyle(style);

            i++;
        }
//        if (exportpo.getrTime() != null) {
//            HSSFCell cell = row.createCell(i);
//            cell.setCellValue("学号");
//            cell.setCellStyle(style);
//
//            i++;
//        }

//        for (int i = 0; i < exportpoList.size(); i++) {
//            row = sheet.createRow(i + 1);
//            Basicinfo basicinfo = list.get(i);
//            row.createCell(0).setCellValue(basicinfo.getSno());
//            row.createCell(1).setCellValue(basicinfo.getName());
//            row.createCell(2).setCellValue(basicinfo.getCollege());
//            row.createCell(3).setCellValue(basicinfo.getSpotregister());
//            row.createCell(4).setCellValue(basicinfo.getReason());
//        }
        for (int j = 0; j < exportpoList.size(); j++) {
            row = sheet.createRow(j + 1);
            int k = 0;
            if (exportpo.getSno() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getSno());
                k++;
            }
            if (exportpo.getName() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getName());
                k++;
            }
            if (exportpo.getSex() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getSex());
                k++;
            }
            if (exportpo.getExamnum() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getExamnum());
                k++;
            }
            if (exportpo.getIdnum() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getIdnum());
                k++;
            }
            if (exportpo.getCampus() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getCampus());
                k++;
            }
            if (exportpo.getCollege() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getCollege());
                k++;
            }
            if (exportpo.getMajor() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getMajor());
                k++;
            }
            if (exportpo.getClassnum() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getClassnum());
                k++;
            }
            if (exportpo.getDormnum() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getDormnum());
                k++;
            }
            if (exportpo.getBankcardid() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getBankcardid());
                k++;
            }
            if (exportpo.getPaysta() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getPaysta());
                k++;
            }
            if (exportpo.getCollegetel() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getCollegetel());
                k++;
            }
            if (exportpo.getSchooltel() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getSchooltel());
                k++;
            }
            if (exportpo.getMastername() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getMastername());
                k++;
            }
            if (exportpo.getMastertel() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getMastertel());
                k++;
            }
            if (exportpo.getBrother() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getBrother());
                k++;
            }
            if (exportpo.getBrothertel() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getBrothertel());
                k++;
            }
            if (exportpo.getHelper() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getHelper());
                k++;
            }
            if (exportpo.getHelpertel() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getHelpertel());
                k++;
            }
            if (exportpo.getSpotregister() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getSpotregister());
                k++;
            }
            if (exportpo.getReason() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getReason());
                k++;
            }
            if (exportpo.getRemarkOne() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getRemarkOne());
                k++;
            }
            if (exportpo.getRemarkTwo() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getRemarkTwo());
                k++;
            }
            if (exportpo.getBedding() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getBedding());
                k++;
            }
            if (exportpo.getPhonenum() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getPhonenum());
                k++;
            }
            if (exportpo.getParentname() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getParentname());
                k++;
            }
            if (exportpo.getParentphonenum() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getParentphonenum());
                k++;
            }
            if (exportpo.getHomeaddr() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getHomeaddr());
                k++;
            }
            if (exportpo.getMilitaryclothing() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getMilitaryclothing());
                k++;
            }
            if (exportpo.getShoenum() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getShoenum());
                k++;
            }
            if (exportpo.getHeight() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getHeight());
                k++;
            }
            if (exportpo.getWeight() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getWeight());
                k++;
            }
            if (exportpo.getLoan() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getLoan());
                k++;
            }
            if (exportpo.getTrafficway() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getTrafficway());
                k++;
            }
            if (exportpo.getRegistertime() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getRegistertime());
                k++;
            }
            if (exportpo.getCompany() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getCompany());
                k++;
            }
            if (exportpo.getrTime() != null) {
                row.createCell(k).setCellValue(exportpoList.get(j).getrTime());
                k++;
            }


        }
        return wb;
    }
}
