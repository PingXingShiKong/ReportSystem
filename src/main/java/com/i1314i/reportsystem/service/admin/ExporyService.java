package com.i1314i.reportsystem.service.admin;

import com.i1314i.reportsystem.po.Basicinfo;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 缴费情况信息导出service
 **/
@Service
public class ExporyService {
    String[] excelHeader = {"学号", "姓名", "学院", "报到状态", "未报到理由"};

    public HSSFWorkbook export(List<Basicinfo> list) {
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("报到状态");
        HSSFRow row = sheet.createRow((int) 0);
        sheet.setColumnWidth(1, 31 * 256);
        sheet.setColumnWidth(2, 31 * 256);
        sheet.setColumnWidth(3, 31 * 256);
        sheet.setColumnWidth(4, 31 * 256);
        sheet.setColumnWidth(5, 31 * 256);
        HSSFCellStyle style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        for (int i = 0; i < excelHeader.length; i++) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue(excelHeader[i]);
            cell.setCellStyle(style);
//            sheet.autoSizeColumn(i);
        }
        for (int i = 0; i < list.size(); i++) {
            row = sheet.createRow(i + 1);
            Basicinfo basicinfo = list.get(i);
            row.createCell(0).setCellValue(basicinfo.getSno());
            row.createCell(1).setCellValue(basicinfo.getName());
            row.createCell(2).setCellValue(basicinfo.getCollege());
            row.createCell(3).setCellValue(basicinfo.getSpotregister());
            row.createCell(4).setCellValue(basicinfo.getReason());
        }
        return wb;
    }
}
