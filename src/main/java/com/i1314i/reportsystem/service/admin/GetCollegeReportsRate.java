package com.i1314i.reportsystem.service.admin;

import com.i1314i.reportsystem.mapper.CollegeMapper;
import com.i1314i.reportsystem.mapper.ReportsRateMapper;
import com.i1314i.reportsystem.po.College;
import com.i1314i.reportsystem.po.ReportsRate;
import com.i1314i.reportsystem.service.exception.AdminException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 获得各学院报到率list
 */
@Component
public class GetCollegeReportsRate {
    @Autowired
    ReportsRateMapper reportsRateMapper;

    @Autowired
    CollegeMapper collegeMapper;




    private final static Logger logger= LoggerFactory.getLogger(GetCollegeReportsRate.class);


    public List<ReportsRate> getCollegeReportsRateList1() {
        NumberFormat nt = NumberFormat.getPercentInstance();
        //设置百分数精确度2即保留两位小数
        nt.setMinimumFractionDigits(2);
        List<ReportsRate> list = new ArrayList<>();
        List<College>collegeList=new ArrayList<>();
        collegeList=collegeMapper.selectAllColleges();
        for (College college:collegeList
             ) {
            ReportsRate tiyu = new ReportsRate(String.valueOf(college.getCollegeid()), college.getCollegename(), reportsRateMapper.selectReport(college.getCollegeid()), reportsRateMapper.selectNoReport(college.getCollegeid()), reportsRateMapper.selectTotalNum(college.getCollegeid()));
            if (tiyu.getTotalnum()!=0){
                tiyu.setRate(nt.format((float) tiyu.getReportednum() / (float)tiyu.getTotalnum()));
            }
            else tiyu.setRate("0");
            list.add(tiyu);
        }
        return list;
    }

    public List<ReportsRate> getCollegeReportsRateList() throws AdminException {
        NumberFormat nt = NumberFormat.getPercentInstance();
        //设置百分数精确度2即保留两位小数
        nt.setMinimumFractionDigits(2);

        List<ReportsRate> reportsRateList=new ArrayList<>();
        List<College>collegeList=null;
        collegeList=collegeMapper.selectAllColleges();

        if(collegeList!=null&&collegeList.size()>0){
            for(College college:collegeList){
                ReportsRate reportsRate=new ReportsRate();
                reportsRate.setCollegeId(String.valueOf(college.getCollegeid()));
                reportsRate.setCollegeName(college.getCollegename());
                reportsRate.setReportednum(reportsRateMapper.selectReport(college.getCollegeid()));
                reportsRate.setNoreportednum(reportsRateMapper.selectNoReport(college.getCollegeid()));
                reportsRate.setTotalnum(reportsRateMapper.selectTotalNum(college.getCollegeid()));
                if (reportsRate.getTotalnum()!=0)
                    reportsRate.setRate(nt.format((float)reportsRate.getReportednum() / (float)reportsRate.getTotalnum()));
                else reportsRate.setRate("0");
                reportsRateList.add(reportsRate);
            }

        }else {
            logger.info("college is empty type:500");
            throw new AdminException("college is empty");

        }
        return  reportsRateList;
    }

}