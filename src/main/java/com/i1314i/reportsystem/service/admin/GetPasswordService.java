package com.i1314i.reportsystem.service.admin;

import org.springframework.stereotype.Component;

/**
 * 根据新生身份证号获取学生密码后六位
 * 导入基础信息时设置学生密码为身份证后六位
 */
@Component
public class GetPasswordService {
    public String getPassword(String idNum){
        return idNum.substring(12).toLowerCase();
    }
}
