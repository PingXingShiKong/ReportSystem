package com.i1314i.reportsystem.service.admin;

import com.i1314i.reportsystem.mapper.BasicinfoMapper;
import com.i1314i.reportsystem.po.Basicinfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InsertService {
    @Autowired
    BasicinfoMapper basicinfoMapper;
    public void insertBasicinfo(Basicinfo basicinfo){
        basicinfoMapper.insert(basicinfo);
    }
}
