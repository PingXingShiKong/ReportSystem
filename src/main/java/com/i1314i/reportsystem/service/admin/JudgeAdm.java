package com.i1314i.reportsystem.service.admin;

import com.i1314i.reportsystem.mapper.AdminloginMapper;
import com.i1314i.reportsystem.po.Adminlogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JudgeAdm {
    @Autowired
    AdminloginMapper adminloginMapper;
    public String judegAdministratoreexist(Adminlogin adminlogin){
        if (adminloginMapper.selectByadminname(adminlogin.getAdminname())!=null){
            return "你要添加的老师已存在,请重新输入";
        }
        else {
            adminlogin.setEnabled("true");
            adminlogin.setToken(null);
            adminloginMapper.insert(adminlogin);
            return "添加成功";
        }
    }
}
