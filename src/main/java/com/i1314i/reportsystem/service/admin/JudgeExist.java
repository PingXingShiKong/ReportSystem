package com.i1314i.reportsystem.service.admin;

import com.i1314i.reportsystem.mapper.BasicinfoMapper;
import com.i1314i.reportsystem.mapper.StudentloginMapper;
import com.i1314i.reportsystem.po.Basicinfo;
import com.i1314i.reportsystem.po.Studentlogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JudgeExist {
    @Autowired
    BasicinfoMapper basicinfoMapper;
    @Autowired
    StudentloginMapper studentloginMapper;
    public boolean judgeExist(String sno){
        Basicinfo basicinfo=basicinfoMapper.selectByPrimaryKey(sno);
        if (basicinfo==null)return false;
        else return true;
    }
    public boolean judgeStudentloginExist(String sno){
        Studentlogin studentlogin=studentloginMapper.selectByPrimaryKey(sno);
        if (studentlogin==null) return false;
        else return true;
    }
}
