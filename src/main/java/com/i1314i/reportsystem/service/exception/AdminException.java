package com.i1314i.reportsystem.service.exception;

/**
 * @author 平行时空
 * @created 2018-07-19 21:44
 **/
public class AdminException extends Exception  {
    public AdminException() {
        super();
    }

    public AdminException(String message) {
        super(message);
    }

    public AdminException(String message, Throwable cause) {
        super(message, cause);
    }
}
