package com.i1314i.reportsystem.service.exception;

/**
 * @author 平行时空
 * @created 2018-05-12 16:01
 **/
public class LoginException extends Exception {

    public LoginException() {
        super();
    }

    public LoginException(String message) {
        super(message);
    }

    public LoginException(String message, Throwable cause) {
        super(message, cause);
    }
}
