package com.i1314i.reportsystem.service.exception;

/**
 * @author 平行时空
 * @created 2018-06-06 8:54
 **/
public class ProblemException extends Exception {
    public ProblemException() {
        super();
    }

    public ProblemException(String message) {
        super(message);
    }

    public ProblemException(String message, Throwable cause) {
        super(message, cause);
    }
}
