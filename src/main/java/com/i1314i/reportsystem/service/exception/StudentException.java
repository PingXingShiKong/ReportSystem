package com.i1314i.reportsystem.service.exception;

/**
 * @author 平行时空
 * @created 2018-05-19 17:46
 **/
public class StudentException extends Exception {

    public StudentException() {
        super();
    }

    public StudentException(String message) {
        super(message);
    }

    public StudentException(String message, Throwable cause) {
        super(message, cause);
    }
}
