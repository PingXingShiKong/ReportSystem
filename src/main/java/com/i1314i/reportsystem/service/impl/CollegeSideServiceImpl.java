package com.i1314i.reportsystem.service.impl;

import com.i1314i.reportsystem.mapper.AdminloginMapper;
import com.i1314i.reportsystem.mapper.BasicinfoMapper;
import com.i1314i.reportsystem.po.Adminlogin;
import com.i1314i.reportsystem.po.Basicinfo;
import com.i1314i.reportsystem.service.ScanQRCodeService;
import com.i1314i.reportsystem.service.exception.StudentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Service("collegeSideService")
public class CollegeSideServiceImpl implements com.i1314i.reportsystem.service.CollegeSideService {

    @Autowired
    BasicinfoMapper basicinfoMapper;
    @Autowired
    ScanQRCodeService scanQRCodeService;
    @Override
    public boolean noSpotRegister(String userId,String fromUserName, String remarkText, Boolean flag) throws StudentException {
        Basicinfo basicinfo=basicinfoMapper.selectByPrimaryKey(userId);
        if(basicinfo==null){

            throw new StudentException(userId+":用户不存在");
        }else {
            //用户存在
            if (flag){
                Boolean iscollage = scanQRCodeService.iscollege(fromUserName,basicinfo.getCollege());
                if (!iscollage){
                    throw new StudentException(userId+"不是本学院的学生");
                }
            }
            if (basicinfo.getSpotregister() != null && "已报到".equalsIgnoreCase(basicinfo.getSpotregister().trim())){
                throw new StudentException(userId+":该用户之前已经报到了");
            }else {
                //该用户还未报道
                basicinfo.setReason(remarkText);
                basicinfoMapper.updateByPrimaryKey(basicinfo);
                return true;
            }
        }
    }


    @Override
    public Basicinfo checkFreshman(String userId, String fromUserName, Boolean flag) throws StudentException {
        Basicinfo basicinfo=basicinfoMapper.selectByPrimaryKey(userId);
        if(basicinfo==null){

            throw new StudentException(userId+":用户不存在");
        }else {
            //用户存在
            if (flag){
                Boolean iscollage = scanQRCodeService.iscollege(fromUserName,basicinfo.getCollege());
                if (!iscollage)
                    throw new StudentException(userId+"不是本学院的学生");
            }
            return basicinfo;
//            if (!iscollage){
//                throw new StudentException(userId+"不是本学院的学生");
//            }else {
//                //是本学院的学生
//                return basicinfo;
//            }
        }
    }

    @Autowired
    AdminloginMapper adminloginMapper;
    @Override
    public int setCollegeId(String userId, HttpServletRequest request) throws StudentException{
        try{
            int intUserid = Integer.parseInt(userId);
            Adminlogin adminlogin = adminloginMapper.selectByPrimaryKey(intUserid);
            if (adminlogin == null){
                throw new StudentException(userId+"学院管理员");
            }
            String collegeId = adminlogin.getCollege();
            HttpSession session = request.getSession();
            session.setAttribute("collegeId",collegeId);
        }catch (Exception e){
            throw new StudentException("查询过程中出错");
        }
        return 0;
    }
}
