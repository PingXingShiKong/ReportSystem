package com.i1314i.reportsystem.service.impl;

import com.i1314i.reportsystem.mapper.AdminloginMapper;
import com.i1314i.reportsystem.mapper.BasicinfoMapper;
import com.i1314i.reportsystem.po.Adminlogin;
import com.i1314i.reportsystem.po.Basicinfo;
import com.i1314i.reportsystem.service.IdentityService;
import com.i1314i.reportsystem.utils.CacheGetKeyUtils;
import com.i1314i.reportsystem.utils.CacheUtils;
import com.i1314i.reportsystem.utils.Jedis.IJedisClient;
import com.i1314i.reportsystem.utils.Jedis.JedisClusterClient;
import com.i1314i.reportsystem.utils.TemplateUtils;
import com.i1314i.reportsystem.utils.weixin.ParamesAPI.util.ParamesAPI;
import com.i1314i.reportsystem.utils.weixin.ParamesAPI.util.WeixinUtil;
import com.i1314i.reportsystem.utils.weixin.contacts.util.MPerson;
import com.i1314i.reportsystem.utils.weixin.oauth2.util.GOauth2Core;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Service("identityService")

public class IdentityServiceImpl implements IdentityService {

    @Autowired
    BasicinfoMapper basicinfoMapper;
    @Autowired
    AdminloginMapper adminloginMapper;
    @Autowired
    private IJedisClient jedisClient;

    @Override
    public Boolean identity(String state, String userId, HttpServletRequest request) {
        Object object = null;
        int id = Integer.parseInt(userId);
        if (state.equals("sc") || state.equals("qrcode") || state.equals("ws") || state.equals("answer") || state.equals("xx")){
            Basicinfo basicinfo = basicinfoMapper.selectByPrimaryKey(userId);
            if (basicinfo == null){
                return false;
            }else{
                return true;
            }
        }else {
            Adminlogin adminlogin = adminloginMapper.selectByPrimaryKey(id);
            if (adminlogin == null)
                return false;
            int type = adminlogin.getType();
            if (state.equals("sdbd") || state.equals("lcx") || state.equals("xckbd") || state.equals("xyreason")
                    || state.equals("xywbd")){
                if (type == 2 || type == 1){
                    request.getSession().setAttribute("admindata",adminlogin);
                    System.out.println("collegeid----" + adminlogin.getCollege());
                    request.getSession().setAttribute("collegeid",adminlogin.getCollege());
                    return true;
                } else
                    return false;

            }else{
                if (state.equals("zsck") || state.equals("zsbdl") || state.equals("zsreason")){
                    if (type == 1){
                        request.getSession().setAttribute("admindata",adminlogin);
                        System.out.println("collegeid----" + adminlogin.getCollege());
                        request.getSession().setAttribute("collegeid",adminlogin.getCollege());
                        return true;
                    } else
                        return false;
                }
            }
        }
        return false;
    }

    @Override
    public String getTekon(String agentid, String secret) throws IOException {


        String resourcepath="other.properties";
        //缓存类型
        String cacheType=TemplateUtils.getPropertiesdata(resourcepath,"cache.type");
        boolean cacheStatus=cacheType.trim().equalsIgnoreCase("redis");
        boolean status=false;
        String key=CacheGetKeyUtils.getWechatToken(agentid);
        System.out.println(key);
//            Jedis jedis=null;
//            String qrtoken=TemplateUtils.uuid();
        String access_token;
        if(cacheStatus){
            status=jedisClient.exists(key);
            if(status) {
                access_token = jedisClient.get(key);
                System.out.println(access_token);
            }else {
//                jedis=JedisUtils.getJedis();
                //key不存在

                access_token = WeixinUtil.getAccessToken(ParamesAPI.corpId,secret).getToken();
                if (access_token == null){
                    return null;
                }
                System.out.println("刷新二维码校验token(redis)：" + access_token);
//                    jedis.set(key,qrtoken);
                jedisClient.set(key, access_token, 60 * 60 * 1);
                //设置缓存时间
//                    jedis.expire(key,60*60*24*7);
//                    jedis.close();
            }
        }else {
            //加入缓存
            status = CacheUtils.qrconcurrentHashMap.containsKey(key);
            if (status) {
                access_token=CacheUtils.qrconcurrentHashMap.get(key);
            } else{

                access_token = WeixinUtil.getAccessToken(ParamesAPI.corpId,secret).getToken();
                CacheUtils.qrconcurrentHashMap.put(key, access_token);
                System.out.println("刷新二维码校验token(concurrentHashMap)：" + access_token);
            }
        }
        return access_token;
    }

    @Override
    public String getUserId(String code, String state, HttpServletRequest request) throws IOException {
        System.out.println("inde");
        HttpSession session = request.getSession();
        String userId = null;
        if (session.getAttribute("userId") == null){
            System.out.println("session");
            String agentid_secret[] = WeixinUtil.getAgentidSecret(state).split("123456");

            String access_token = getTekon(agentid_secret[0],agentid_secret[1]);
            userId = GOauth2Core.GetUserID(access_token,code,agentid_secret[0],request);
            System.out.println("userid :" + userId);
            if (userId == null){
                System.out.println("-------从Goauth2中获取userId 为空  失败！！！-------");
                return null;
            }else {

                System.out.println("-------从Goauth2中获取userId 不不不空-------");
                session.setAttribute("userId",userId);
                return userId;
            }
        }else{
            System.out.println("----从session中获取userId ----");
            userId = (String)session.getAttribute("userId");
            if (userId != null){
                System.out.println("---- 从session获取userId成功------");
                return userId;
            }else{
                System.out.println("---- 从session获取userId失败！！！！------");
                return null;
            }
        }

    }


}
