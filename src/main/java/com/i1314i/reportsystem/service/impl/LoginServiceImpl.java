package com.i1314i.reportsystem.service.impl;

import com.i1314i.reportsystem.mapper.AdminloginMapper;
import com.i1314i.reportsystem.mapper.LoggerMapper;
import com.i1314i.reportsystem.mapper.ScoreMapper;
import com.i1314i.reportsystem.mapper.StudentloginMapper;
import com.i1314i.reportsystem.po.Adminlogin;
import com.i1314i.reportsystem.po.AdminloginExample;
import com.i1314i.reportsystem.po.Score;
import com.i1314i.reportsystem.po.Studentlogin;
import com.i1314i.reportsystem.service.LoginService;
import com.i1314i.reportsystem.service.exception.LoginException;
import com.i1314i.reportsystem.utils.CusAccessObjectUtil;
import com.i1314i.reportsystem.utils.TemplateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author 平行时空
 * @created 2018-05-12 15:54
 **/

@Service("loginService")
public class LoginServiceImpl implements LoginService {
    private final static Logger logger= LoggerFactory.getLogger(LoginServiceImpl.class);
    @Autowired
    StudentloginMapper studentloginMapper;
    @Autowired
    AdminloginMapper adminloginMapper;
    @Autowired
    private ScoreMapper scoreMapper;

    @Autowired
    LoggerMapper loggerMapper;
    /**
     * 学生端/管理端登陆
     * @param sdata
     * @return Integer 失败:抛异常  1：学生登陆成功 2:管理员登陆成功 3超级管理员登陆成功
     * @throws LoginException
     */
    @Override
    public Integer  isStudentByNo(Studentlogin sdata, HttpSession session, HttpServletRequest request) throws LoginException, IOException {
        Integer status=0;
        String sessioncode= (String) session.getAttribute("verCode");
        Studentlogin sqldata=studentloginMapper.selectByPrimaryKey(sdata.getSno());

        /*
            查询管理端用户
         */
        if(sqldata==null){
            AdminloginExample example=new AdminloginExample();
            AdminloginExample.Criteria criteria=example.createCriteria();
            criteria.andAdminnameEqualTo(sdata.getSno());
            List<Adminlogin> adminloginlist=adminloginMapper.selectByExample(example);
            System.out.println(adminloginlist.size());
            if(adminloginlist.size()==1){
                    Adminlogin adminloginsql=adminloginlist.get(0);
                    if(adminloginsql.getAdminname().equals(sdata.getSno())&&adminloginsql.getPassword().equals(sdata.getPassword())){
                        session.setAttribute("admindata",adminloginsql);
                        com.i1314i.reportsystem.po.Logger loggers=new com.i1314i.reportsystem.po.Logger();
                        loggers.setuIp(CusAccessObjectUtil.getIpAddress(request));
                        Date day=new Date();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        loggers.setuTime(df.format(day));
                        loggers.setuParams(adminloginsql.getAdminname());
                        if(adminloginsql.getType()==1){
                            loggers.setuName("超级管理员");
                            loggers.setuOperate("超级管理员登陆");
                            logger.info(adminloginsql.getAdminname()+":超级管理员登陆成功");
                            loggerMapper.insert(loggers);
                            //超级管理员
                            return 3;
                        }else if(adminloginsql.getType()==2){
                            loggers.setuName("学院管理员登陆");
                            loggers.setuOperate("学院管理员登陆");
                            loggerMapper.insert(loggers);
                            logger.info(adminloginsql.getAdminname()+":学院管理员登陆成功");
                            //学院管理员
                            return 2;
                        }

                    }else {
                        logger.info("用户名或者密码错误");
                        throw new LoginException("用户名或者密码错误");
                    }
            }else{
                logger.info("未知错误,该用户不存在或者数据库数据错误");
                throw new LoginException("用户名或者密码错误");//未知错误,该用户不存在或者数据库数据错误
            }

        //验证学生用户
        }else if(!(sdata.getSno().trim().equals(sqldata.getSno())&&sdata.getPassword().trim().equals(sqldata.getPassword()))){
            logger.info(sqldata.getSno()+" :用户名或者密码错误");
            throw new LoginException("用户名或者密码错误");
        }else if(!sdata.getCode().trim().equalsIgnoreCase(sessioncode)){
            logger.info(sqldata.getSno()+" :验证码错误");
            throw new LoginException("验证码错误");
        } else {

            /*
                考试题是否过关验证
             */
            String resourcepath="other.properties";
            Integer examisOpen= Integer.valueOf(TemplateUtils.getPropertiesdata(resourcepath,"ExamisOpen"));

            if(examisOpen==null){
                examisOpen=0;
            }

            if(examisOpen!=0){
                //最小过关成绩
                Integer minScore= Integer.valueOf(TemplateUtils.getPropertiesdata(resourcepath,"ExamMinScore"));
                Score score=scoreMapper.selectByPrimaryKey(sqldata.getSno());
                if(score==null){
                    score=new Score();
                    session.setAttribute("exam",0);
                }
                if(score.getScore()==null){

//                    score=new Score();
                    score.setScore(0.0);
                }

                if(minScore>score.getScore()){
                    session.setAttribute("exam",0);
                }
            }

            logger.info(sqldata.getSno()+" :登陆成功");
            status=1;
        }
        session.setAttribute("userId",sqldata.getSno());

        return status;
    }
}
