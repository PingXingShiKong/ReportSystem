package com.i1314i.reportsystem.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.i1314i.reportsystem.mapper.ProblemMapper;
import com.i1314i.reportsystem.mapper.ScoreMapper;
import com.i1314i.reportsystem.po.Problem;
import com.i1314i.reportsystem.po.Score;
import com.i1314i.reportsystem.po.pageHelper.PageBean;
import com.i1314i.reportsystem.service.ProblemService;
import com.i1314i.reportsystem.service.exception.ProblemException;
import com.i1314i.reportsystem.utils.*;
import com.i1314i.reportsystem.utils.Jedis.IJedisClient;
import com.i1314i.reportsystem.utils.Jedis.JedisClusterClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import javax.servlet.http.HttpSession;
import java.io.IOException;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author 平行时空
 * @created 2018-06-06 8:06
 **/
@Service("problemService")
public class ProblemServiceImpl implements ProblemService {
    Logger logger= LoggerFactory.getLogger(ProblemServiceImpl.class);
    @Autowired
    private ProblemMapper problemMapper;
    @Autowired
    private ScoreMapper scoreMapper;

    @Autowired
    private IJedisClient jedisClient;

    @Override
    public double getScore(String sno) {
        Score score=scoreMapper.selectByPrimaryKey(sno);
        return score.getScore();
    }

    /**
     * 判断是否过关
     * @param sno
     * @return
     * @throws IOException
     */
    @Override
    public boolean isJump(String sno) throws IOException {

        //是否开启  关闭时返回true
        Integer examisOpen= Integer.valueOf(TemplateUtils.getPropertiesdata("other.properties","ExamisOpen"));
        //最小过关成绩
        Integer minScore= Integer.valueOf(TemplateUtils.getPropertiesdata("other.properties","ExamMinScore"));
        Score score=null;
        if(examisOpen==0){
            return true;
        }else{
             score=scoreMapper.selectByPrimaryKey(sno);
             if(score==null){
                 return false;
             }
            if(minScore<=score.getScore()){
                return true;
            }
        }

        return false;
    }

    /**
     * 随机获取考试试题并加入缓存（每个人的试题随机）
     * 获取成绩/若已经过关抛出ProblemException 否则返回试题列表
     * 先查询缓存，若试题存在从缓存中获取，否则从数据库获取加入缓存，每个人题目不一样
     * 学生提交试题后清除缓存中的内容
     * @param sno
     * @return
     * @throws IOException
     */
    @Override
    public List<Problem> getProblems(String sno) throws IOException, ProblemException {
        String resourcepath="other.properties";
        Score score=scoreMapper.selectByPrimaryKey(sno);

        if(score==null){
            score=new Score();
            score.setScore(0.0);
            score.setSno(sno);
            scoreMapper.insert(score);
        }
        //最小过关成绩
        Integer minScore= Integer.valueOf(TemplateUtils.getPropertiesdata(resourcepath,"ExamMinScore"));

        //考试题数目
        Integer scoreNums= 10;

        //试题列表
        List<Problem>problems=null;
        String key= CacheGetKeyUtils.getProblemKey(sno);



        //缓存类型
        String cacheType=TemplateUtils.getPropertiesdata(resourcepath,"cache.type");
        //考试类型
        Integer examNumType= Integer.valueOf(TemplateUtils.getPropertiesdata(resourcepath,"ExamNumType"));


        Integer minExamNums=0;
        Integer maxExamNums=0;

        if(examNumType==1){
            minExamNums=Integer.valueOf(TemplateUtils.getPropertiesdata(resourcepath,"ExamMinNums"));
            maxExamNums=Integer.valueOf(TemplateUtils.getPropertiesdata(resourcepath,"ExamMaxNums"));
            if(maxExamNums>minExamNums){
                Random random=new Random();
                scoreNums = random.nextInt(maxExamNums - minExamNums + 1) + minExamNums;
            }else {
                scoreNums=Integer.valueOf(TemplateUtils.getPropertiesdata(resourcepath,"ExamNums"));
            }
        }else{
            scoreNums=Integer.valueOf(TemplateUtils.getPropertiesdata(resourcepath,"ExamNums"));
        }

//        Jedis jedis=null;

        //获取试题列表

        if(minScore>score.getScore()){
            boolean cacheStatus=cacheType.trim().equalsIgnoreCase("redis");
            boolean status=false;
            if(cacheStatus){
//                jedis=JedisUtils.getJedis();
                status=jedisClient.exists(key);
                if(status){
                    logger.info("试题从redis缓存中获取....");
                    problems= JsonUtils.jsonToList(jedisClient.get(key),Problem.class);
//                    jedis.close();
                }else {
                    //key不存在
                    logger.info("试题从数据库中读取并加入缓存....");
                    problems=problemMapper.selectProblemsbyRand(scoreNums);
//                    jedis.set(key,JsonUtils.objectToJson(problems));
//                    jedis.set(key,JsonUtils.objectToJson(problems));
                    jedisClient.set(key,JsonUtils.objectToJson(problems),60*60*24);
                    //设置缓存时间
//                    jedis.expire(key,60*60*24);
//                    jedis.close();
                }
            }else {
                status=CacheUtils.containsKey(key);
                if(status){
                    logger.info("试题从CurrentHashMap缓存中获取....");
                    problems=CacheUtils.getListData(key,Problem.class);
                }else {
                    logger.info("试题从数据库中读取并加入缓存....");
                    problems=problemMapper.selectProblemsbyRand(scoreNums);
                    //加入缓存
                    CacheUtils.putData(key,problems);
                }
            }

        }else{
            throw new ProblemException("您的考试成绩已过关");
        }

        //返回前端无答案的试题
        List<Problem>userProblems= new ArrayList<>();
        for (int i=0;i<problems.size();i++){
            Problem problem=problems.get(i);
            problem.setAnswer(null);
            userProblems.add(problem);
        }

        return userProblems;
    }

    @Override
    public double[] getScore(Map<Integer, String> exams,String sno) throws IOException, ProblemException {
        String resourcepath="other.properties";
        String key= CacheGetKeyUtils.getProblemKey(sno);
        //缓存类型
        String cacheType=TemplateUtils.getPropertiesdata(resourcepath,"cache.type");

        Integer examType= Integer.valueOf(TemplateUtils.getPropertiesdata(resourcepath,"ExamType"));

//        Jedis jedis=null;
        boolean cacheStatus=cacheType.trim().equalsIgnoreCase("redis");
        boolean status=false;
        //试题列表
        List<Problem>problems=null;
        if(cacheStatus){
//           jedis=JedisUtils.getJedis();
            status=jedisClient.exists(key);
            if(status){
                logger.info("试题从redis缓存中获取....");
                problems= JsonUtils.jsonToList(jedisClient.get(key),Problem.class);
//                jedis.close();
            }else {
//               jedis.close();
                throw  new ProblemException("考试不合要求，请重新考试");
            }
        }else {
            status=CacheUtils.containsKey(key);
            if(status){
                logger.info("试题从CurrentHashMap缓存中获取....");
                problems=CacheUtils.getListData(key,Problem.class);
            }else {
                throw  new ProblemException("考试不合要求，请重新考试");
            }
        }

        //考试题总分
        Integer examAllScore= Integer.valueOf(TemplateUtils.getPropertiesdata(resourcepath,"ExamAllScore"));

        //最小过关成绩
        Integer minScore= Integer.valueOf(TemplateUtils.getPropertiesdata(resourcepath,"ExamMinScore"));

        Integer nums=0;
        Integer allnums=problems.size();
        for (int i=0;i<problems.size();i++){
            Problem problem=problems.get(i);
            String userAnswer=exams.get(problem.getProblemid());
            if(problem.getAnswer().trim().equalsIgnoreCase(userAnswer)){
                nums++;
            }
        }

        Score score=new Score();
        score.setSno(sno);
        double cengji=examAllScore/(allnums*1.0)*nums;
        System.out.println(cengji);
//        BigDecimal b = new BigDecimal(cengji);
//         cengji = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        cengji = (double) Math.round(cengji * 10) / 10;

        score.setScore(cengji);
        double[] ans= new double[2];
        if(score.getScore()>=minScore){
            ans[0]=1;
        }else{
            if(cacheStatus){
                if(examType==2){
//                    jedis=JedisUtils.getJedis();
                    jedisClient.del(key);
//                    jedis.close();
                }
            }else{
                if(examType==2){
                    CacheUtils.removeData(key);
                }
            }
            ans[0]=0;
        }

        //更新数据库
        scoreMapper.updateByPrimaryKey(score);
        ans[1]=score.getScore();
        return ans;
    }

    /**
     *微信公众号获取部分答案
     * @param sno
     * @return
     */
    @Override
    public List<Problem> wechatGetAnswers(String sno) throws IOException, ProblemException {
        String resourcepath="other.properties";
        String key= CacheGetKeyUtils.getProblemKey(sno);
        Score score=scoreMapper.selectByPrimaryKey(sno);

        if(score==null){
            score=new Score();
            score.setSno(sno);
            score.setScore(0.0);
            scoreMapper.insert(score);
        }
        //最小过关成绩
        Integer minScore= Integer.valueOf(TemplateUtils.getPropertiesdata(resourcepath,"ExamMinScore"));

        //成绩答案数量
        Integer answerNums= Integer.valueOf(TemplateUtils.getPropertiesdata(resourcepath,"WechatAnswersNum"));

        //试题列表
        List<Problem>problems=null;

        if(answerNums==0||answerNums==null){
            throw new ProblemException("获取答案系统已关闭");
        }

        //缓存类型
        String cacheType=TemplateUtils.getPropertiesdata(resourcepath,"cache.type");

//       Jedis jedis=null;

        //获取试题列表

        if(minScore>score.getScore()){
            boolean cacheStatus=cacheType.trim().equalsIgnoreCase("redis");
            boolean status=false;
            if(cacheStatus){
//               jedis=JedisUtils.getJedis();
                status=jedisClient.exists(key);
                if(status){
                    logger.info(sno+" :查询试题答案(redis)");
                    problems= JsonUtils.jsonToList(jedisClient.get(key),Problem.class);
//                    jedis.close();
                }
            }else {
                status=CacheUtils.containsKey(key);
                if(status){
                    logger.info(sno+" :查询试题答案(CurrentHashMap)");
                    problems=CacheUtils.getListData(key,Problem.class);
                }
            }

        }else{
            throw new ProblemException("您的考试成绩已过关");
        }

        if(problems==null){
            throw new ProblemException("您还未参加考试..请获取试题后再查询");
        }

        //返回前端指定数量带答案的试题
        List<Problem>userProblems= new ArrayList<>();
        for (int i=0;i<answerNums;i++){
            Problem problem=problems.get(i);
            userProblems.add(problem);
        }
        return userProblems;
    }

    /**
     * 获取所有试题
     * @return
     * @throws Exception
     */
    @Override
    public PageInfo<Problem> getAllProblems(PageBean pageBean) throws Exception {
        PageHelper.startPage(pageBean.getCurrentPage(),pageBean.getPageSize());
        List<Problem> list = problemMapper.selectAllProblems(pageBean);
        PageInfo<Problem> pageInfo = new PageInfo<Problem>(list);
        return pageInfo;
    }
}
