package com.i1314i.reportsystem.service.impl;

import com.i1314i.reportsystem.mapper.AdminloginMapper;
import com.i1314i.reportsystem.mapper.BasicinfoMapper;
import com.i1314i.reportsystem.mapper.CollegeMapper;
import com.i1314i.reportsystem.po.Adminlogin;
import com.i1314i.reportsystem.po.Basicinfo;
import com.i1314i.reportsystem.po.College;
import com.i1314i.reportsystem.po.JsonResult;
import com.i1314i.reportsystem.service.ScanQRCodeService;
import com.i1314i.reportsystem.service.StudentService;
import com.i1314i.reportsystem.service.exception.StudentException;
import com.i1314i.reportsystem.utils.CacheGetKeyUtils;
import com.i1314i.reportsystem.utils.CacheUtils;
import com.i1314i.reportsystem.utils.Jedis.IJedisClient;
import com.i1314i.reportsystem.utils.Jedis.JedisClusterClient;
import com.i1314i.reportsystem.utils.JedisUtils;
import com.i1314i.reportsystem.utils.TemplateUtils;
import com.i1314i.reportsystem.utils.weixin.ParamesAPI.util.ParamesAPI;
import com.i1314i.reportsystem.utils.weixin.ParamesAPI.util.WeixinUtil;
import com.i1314i.reportsystem.utils.weixin.ScanQRCode.util.ParseBasicInfo;
import com.i1314i.reportsystem.utils.weixin.msg.Resp.TextMessage;
import com.i1314i.reportsystem.utils.weixin.msg.Util.MessageUtil;
import com.i1314i.reportsystem.utils.weixin.msg.Util.SMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

@Service("scamQRCodeService")
public class ScanQRCodeServiceImpl implements ScanQRCodeService {
    private final static Logger logger=LoggerFactory.getLogger(ScanQRCodeServiceImpl.class);
    @Autowired
    private StudentService studentService;

    @Autowired
    private BasicinfoMapper basicinfoMapper;

    @Autowired
    private IJedisClient jedisClient;


    @Override
    public String processRequest(String request) {
        // xml格式的消息数据s
        String respXml = null;
        // 默认返回的文本消息内容
        String respContent = "";
        // 调用parseXml方法解析请求消息
        try {
            Map<String, String> requestMap = MessageUtil.parseXml(request);
            // 发送方帐号
            String fromUserName = requestMap.get("FromUserName");
            // 开发者微信号
            String toUserName = requestMap.get("ToUserName");
            // 消息类型
            String msgType = requestMap.get("MsgType");

            // 回复文本消息
            TextMessage textMessage = new TextMessage();
            //toUserName 成员UserID
            textMessage.setToUserName(fromUserName);
            //fromUserName 企业微信CorpID
            textMessage.setFromUserName(toUserName);
            textMessage.setCreateTime(new Date().getTime());
            textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);

            // 文本消息
            if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_TEXT)) {
                respContent = "您发送的是文本消息！";
            }
            // 图片消息
            else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_IMAGE)) {
                respContent = "您发送的是图片消息！";
            }
            // 语音消息
            else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_VOICE)) {
                respContent = "您发送的是语音消息！";
            }
            // 视频消息
            else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_VIDEO)) {
                respContent = "您发送的是视频消息！";
            }
            // 地理位置消息
            else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_LOCATION)) {
                respContent = "您发送的是地理位置消息！";
            }
            // 链接消息
            else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_LINK)) {
                respContent = "您发送的是链接消息！";
            }
            // 事件推送
            else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_EVENT)){

                // 事件类型
                String eventType = requestMap.get("Event");
                // 关注
                if (eventType.equals(MessageUtil.EVENT_TYPE_SUBSCRIBE)) {
                    respContent = "谢谢您的关注！";
                }
                // 取消关注
                else if (eventType.equals(MessageUtil.EVENT_TYPE_UNSUBSCRIBE)) {
                    // TODO 取消订阅后用户不会再收到公众账号发送的消息，因此不需要回复
                }
                // 扫码推事件的事件推送
                else if (eventType.equals(MessageUtil.EVENT_TYPE_SCANCODE_PUSH)) {
                    // TODO 处理扫描带参数二维码事件
                }
                //扫码推事件且弹出“消息接收中”提示框的事件推送
                else if (eventType.equals(MessageUtil.EVENT_TYPE_SCANCODE_WAITMSG)){
                    // TODO 处理二维码
                    System.out.println("----不意外-----");
                    String userId = requestMap.get("ScanResult");
                    if(userId.indexOf("-")<0){
                        Boolean postTextMsg = postTextMsg(userId,"兄弟，报到失败了，您的二维码是伪造的吧！！");
                        respContent="报到失败,二维码不合法,请让同学在官方生成";
                    }else {
                        String []qrtokens=userId.split("-");
                        if(qrtokens.length!=2||qrtokens[1]==""||qrtokens[1]==null){
                            Boolean postTextMsg = postTextMsg(qrtokens[0],"兄弟，报到失败了，您的二维码是伪造的吧！！");
                            respContent="报到失败,二维码不合法,请让同学在官方生成";
                        }else{

                            respContent = responseMsg(qrtokens[0],qrtokens[1],fromUserName);
                        }
                    }


                }
                // 上报地理位置
                else if (eventType.equals(MessageUtil.REQ_MESSAGE_TYPE_LOCATION)) {
                    // TODO 处理上报地理位置事件
                }
                // 自定义菜单
                else if (eventType.equals(MessageUtil.EVENT_TYPE_CLICK)) {
                    // TODO 处理菜单点击事件
                    //事件KEY值，与创建自定义菜单时指定的KEY值对应
//                    String eventKey = requestMap.get("EventKey");
//                    System.out.println("EventKey=" + eventKey);
//                    respContent = "点击的菜单KEY:" + eventKey;
                }
            }
            // 设置文本消息的内容
            textMessage.setContent(respContent);
            // 将文本消息对象转换成xml
            respXml = MessageUtil.textMessageToXml(textMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return respXml;

    }


//    @Override
//    public JsonResult postSTextCardMsg(String userId) {
//        JsonResult jsonResult=JsonResult.createJsonResult();
//        //获取sccess_token
//        String access_token = WeixinUtil.getAccessToken(ParamesAPI.corpId,ParamesAPI.secret_stu).getToken();
//        //获取学生信息
////        Basicinfo basicinfo=studentService.findStudentDataBySno(userId);
//        boolean isSuccess= false;
//        String content = "";
//        try {
//            System.out.println("try");
//            isSuccess=studentService.isspotRegister(userId);
//            if(isSuccess){
//                Basicinfo basicinfo=studentService.findStudentDataBySno(userId);
//                if (basicinfo == null){
//                    jsonResult.setSuccess(100);
//                    content = "报到成功，但获取学生信息时失败";
//                    jsonResult.setMsg(content);
//                    System.out.println("100");
//
//                }else {
//                    content=ParseBasicInfo.parseBasicinfo(basicinfo);
//                    jsonResult.setSuccess(200);
//                    System.out.println("200");
//                }
//
//            }
//        } catch (StudentException e) {
//            content=e.getMessage(); //报到失败信息
//            jsonResult.setSuccess(-2);
//            jsonResult.setMsg(content);
//            System.out.println("-2");
//        }
////        if(basicinfo==null){
////            logger.info("该用户不存在");
////            return -1;
////        }
//        //将Basicinfo转换成String
//
//        System.out.println("----conttent:" + content);
//        //发送的Json
//        String PostData1=SMessage.STextMsg(userId,ParamesAPI.AgentId_stu,content);
//        System.out.println("PostData" + PostData1);
//        //发送的url
//        String RequestUrl="https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=" + access_token;
//        //发送
//        int result1 = -111;
//        if (jsonResult.getSuccess() == 200){
//            result1 = WeixinUtil.PostMessage(access_token, "POST", RequestUrl,PostData1);
//        }
//        // 打印结果
//        System.out.println("Result:" + result1);
//        if (jsonResult.getSuccess() == 200 && result1 == 0){
//            jsonResult.setSuccess(result1);
//            jsonResult.setMsg("报到成功，并给"+userId+"发送相关消息消息");
//        }else if(jsonResult.getSuccess() == 200 && result1 != 0){
//            jsonResult.setMsg("报到成功，但给"+userId+"发消息时失败");
//        }
//        return jsonResult;
//
//    }

    @Override
    public Boolean postTextMsg(String userId, String content){
//        JsonResult jsonResult=JsonResult.createJsonResult();
        //获取sccess_token
        String access_token = WeixinUtil.getAccessToken(ParamesAPI.corpId,ParamesAPI.secret_stu).getToken();
        //获取学生信息

        System.out.println("----conttent:" + content);
        //发送的Json
        String PostData1=SMessage.STextMsg(userId,ParamesAPI.AgentId_stu,content);
        System.out.println("PostData" + PostData1);
        //发送的url
        String RequestUrl="https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=" + access_token;
        //发送
        int result1 = WeixinUtil.PostMessage(access_token, "POST", RequestUrl,PostData1);
        if (result1 == 0){
            return true;
        }
        return false;
    }


    @Autowired
    AdminloginMapper adminloginMapper;
    @Autowired
    CollegeMapper collegeMapper;
    @Override
    public Boolean iscollege(String fromUserName, String collage)throws StudentException{
        System.out.println("college" + collage);
        Adminlogin adminlogin  = adminloginMapper.selectByPrimaryKey(Integer.parseInt(fromUserName));;
        if (adminlogin == null){
            throw new StudentException();
        }

        String adminCollegeId = adminlogin.getCollege();
        College adminCollege = collegeMapper.selectByPrimaryKey(Integer.parseInt(adminCollegeId));
        String adminCollegeName = adminCollege.getCollegename();
        if (adminCollegeName != null){
            System.out.println("admincollege" + adminCollegeName);
            if (adminCollegeName.trim().equalsIgnoreCase(collage.trim())){
                return true;
            }else {
                return false;
            }
        }else{
            System.out.println("adminCollege is null");
        }
        return false;
    }

    @Override
    public String responseMsg(String userId,String qrto, String fromUserName) throws IOException {
        String respContent = "";
//        Basicinfo basicinfo=studentService.findStudentDataBySno(userId);
        Basicinfo basicinfo = basicinfoMapper.selectByPrimaryKey(userId);

        if (basicinfo != null){
            Boolean iscollage = null; //是否是本学院的学生
            try {
                iscollage = iscollege(fromUserName,basicinfo.getCollege());
            } catch (Exception e) {
                return "你不是本学院的管理员";
            }

            if(!qrto.trim().equalsIgnoreCase("shoudng")){
                String resourcepath="other.properties";
                //缓存类型
                String cacheType=TemplateUtils.getPropertiesdata(resourcepath,"cache.type");

                boolean cacheStatus=cacheType.trim().equalsIgnoreCase("redis");

                boolean status=false;
                String key=CacheGetKeyUtils.getQrKey(userId);
                System.out.println(key);
//                Jedis jedis=null;
                String qrtoken="";

                if(cacheStatus){
//                    jedis=JedisUtils.getJedis();
                    status=jedisClient.exists(key);
                    if(status){
                        qrtoken=jedisClient.get(key);
                        System.out.println(qrtoken);
                        if(!qrtoken.trim().equalsIgnoreCase(qrto)){
//                            jedis.close();
                            Boolean postTextMsg = postTextMsg(userId,"兄弟，报到失败了，您的二维码已经失效了，请获取最新的二维码");
                            return "报到失败，二维码失效或者非法，请让学生获取最新二维码";
                        }
                    }else {
//                        jedis.close();
                        Boolean postTextMsg = postTextMsg(userId,"兄弟，报到失败了，您的二维码已经失效了，请获取最新的二维码");
                        return "报到失败，二维码失效或者非法，请让学生获取最新二维码";
                    }
//                    jedis.close();
                }else {
                    status=CacheUtils.qrconcurrentHashMap.containsKey(key);
                    if(status){
                        qrtoken=CacheUtils.qrconcurrentHashMap.get(key);
                        if(!qrtoken.trim().equals(qrto)){
                            Boolean postTextMsg = postTextMsg(userId,"兄弟，报到失败了，您的二维码已经失效了，请获取最新的二维码");
                            return "报到失败，二维码失效或者非法，请让学生获取最新二维码";
                        }
                    }else {
                        Boolean postTextMsg = postTextMsg(userId,"兄弟，报到失败了，您的二维码已经失效了，请获取最新的二维码");
                        return "报到失败，二维码失效或者非法，请让学生获取最新二维码";
                    }

                }
            }

            if (iscollage != null && iscollage){
                try {
                    Boolean isSuccess=studentService.isspotRegister(userId);//是否已经报到
                    if (isSuccess){ //报到成功
                        String content = ParseBasicInfo.parseBasicinfo(basicinfo);
                        Boolean postTextMsg = postTextMsg(userId,content);
                        if (postTextMsg){//发消息成功
                            respContent = "报到成功，并给"+userId+"发送相关消息消息";
                        }else{
                            respContent = "报到成功，但给" + userId + "发消息时失败";
                        }
                    }
                }catch (StudentException e){
                    respContent = e.getMessage();
                }

            }else {
                respContent = "该用户不是本学院的学生";
                Boolean postTextMsg = postTextMsg(userId,"兄弟，来错地方了");
            }
        }else{
            respContent = "没有该用户的信息";
        }
        return respContent;
    }

}