package com.i1314i.reportsystem.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.i1314i.reportsystem.mapper.*;
import com.i1314i.reportsystem.po.*;
import com.i1314i.reportsystem.service.StudentService;
import com.i1314i.reportsystem.service.exception.StudentException;
import com.i1314i.reportsystem.utils.CacheGetKeyUtils;
import com.i1314i.reportsystem.utils.CacheUtils;
import com.i1314i.reportsystem.utils.Jedis.IJedisClient;
import com.i1314i.reportsystem.utils.Jedis.JedisClusterClient;
import com.i1314i.reportsystem.utils.JsonUtils;
import com.i1314i.reportsystem.utils.TemplateUtils;
import org.omg.CORBA.UserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * @author 平行时空
 * @created 2018-05-12 16:37
 **/
@Service("studentService")
public class StudentServiceImpl implements StudentService {
    private final static Logger logger=LoggerFactory.getLogger(StudentServiceImpl.class);
    @Autowired
    StcompleteinfoMapper stcompleteinfoMapper;
    @Autowired
    BasicinfoMapper basicinfoMapper;
    @Autowired
    ProblemMapper problemMapper;
    @Autowired
    NewsMapper newsMapper;
    @Autowired
    StudentloginMapper studentloginMapper;
    @Autowired
    private IJedisClient jedisClient;

    private String resourcepath="other.properties";
    /**
     * 用户上传信息查询
     *
     * @param request
     * @return JsonResult
     * @throws Exception
     */
    @Override
    public JsonResult getInfoBySnoResult(HttpServletRequest request) throws StudentException, IOException {
        JsonResult jsonResult = JsonResult.createJsonResult();

        String sno = (String) request.getSession().getAttribute("userId");
        if (sno == null) {
            logger.info("该学号不存在");
            throw new StudentException("该学号不存在");
        } else {

            String key= CacheGetKeyUtils.getStudentstcompleteinfo(sno);
            StudentInfo data=new StudentInfo();
            boolean status=jedisClient.exists(key);
            Stcompleteinfo sdata=null;
            //判断缓存
            if(status){
                logger.info("学生信息从redis缓存中获取Stcompleteinfo数据....");
                sdata= JsonUtils.jsonToObject(jedisClient.get(key),Stcompleteinfo.class);
            }else {
                sdata = stcompleteinfoMapper.selectByPrimaryKey(sno);
                if(sdata!=null){
                    jedisClient.set(key,JsonUtils.objectToJson(sdata),60*60*24);
                }

            }

            if (sdata == null) {
                logger.info(sno+" ：服务器中stcompleteinfo表不存在该学生用户,自动插入数据");
               // throw new StudentException("服务器中不存在该学生用户");
                sdata=new Stcompleteinfo();
                sdata.setSno(sno);
                stcompleteinfoMapper.insertSelective(sdata);
            } else {

                Basicinfo basicinfo=null;
                String key2=CacheGetKeyUtils.getStudentbasicinfo(sno);
                if(jedisClient.exists(key2)){
                    logger.info(sno+":从redis缓存获取basicinfo数据....");
                    basicinfo=JsonUtils.jsonToObject(jedisClient.get(key2),Basicinfo.class);
                }else {
                    basicinfo=basicinfoMapper.selectByPrimaryKey(sno);
                    if(basicinfo!=null){
                        jedisClient.set(key2,JsonUtils.objectToJson(basicinfo),60*60*24);
                    }

                }

                if(basicinfo==null){

                    logger.info(sno+" ：服务器中不存在该学生用户的基本信息");
                    throw new StudentException("服务器中不存在该学生用户的基本信息");
                    //System.out.println("base");
                }
                data.setStcompleteinfo(sdata);
                data.setBasicinfo(basicinfo);
                jsonResult.setMsg("success");
                jsonResult.setSuccess(1);
                jsonResult.setData(data);
            }
        }
        return jsonResult;
    }


    @Override
    public Basicinfo findStudentDataBySno(String sno) throws IOException {
        String key=CacheGetKeyUtils.getStudentbasicinfo(sno);
        Basicinfo basicinfo=null;
        if(jedisClient.exists(key)){
            logger.info(sno+": basenfo数据在缓存获取");
            basicinfo=JsonUtils.jsonToObject(jedisClient.get(key),Basicinfo.class);
        }else {
            logger.info(sno+": basenfo数据在数据库获取");
            basicinfo=basicinfoMapper.selectByPrimaryKey(sno);
            if(basicinfo!=null){
                jedisClient.set(key,JsonUtils.objectToJson(basicinfo),60*60*24);
            }

        }
        return basicinfo;
    }

    @Override
    public JsonResult updateStcompleteinfo(Stcompleteinfo stcompleteinfo, HttpServletRequest request) throws StudentException, JsonProcessingException {
        JsonResult jsonResult = JsonResult.createJsonResult();

        String sno = (String) request.getSession().getAttribute("userId");
        if (sno == null) {
            logger.info("该学号不存在");
            throw new StudentException("该学号不存在");
        }

        stcompleteinfo.setSno(sno);
        Integer num=stcompleteinfoMapper.updateByPrimaryKey(stcompleteinfo);
        if (num ==1) {
            logger.info(sno+":更新学生数据成功,更新redis缓存....");
            String key= CacheGetKeyUtils.getStudentstcompleteinfo(sno);
            if(stcompleteinfo!=null){
                jedisClient.set(key,JsonUtils.objectToJson(stcompleteinfo),60*60*24);
            }

            jsonResult.setSuccess(1);
            jsonResult.setMsg("success");
        } else {
            logger.info(sno+":上传数据失败");
            throw new StudentException("上传数据失败");
        }
        return jsonResult;
    }

    @Override
    public JsonResult getStudentStcompleteinfos(HttpServletRequest request) throws StudentException, IOException {
        JsonResult jsonResult = JsonResult.createJsonResult();

        String sno = (String) request.getSession().getAttribute("userId");
        if (sno == null) {
            logger.info("该学号不存在");
            throw new StudentException("该学号不存在");
        } else {

            String key= CacheGetKeyUtils.getStudentstcompleteinfo(sno);
            Stcompleteinfo stcompleteinfo=null;
            if(jedisClient.exists(key)){

                stcompleteinfo= JsonUtils.jsonToObject(jedisClient.get(key),Stcompleteinfo.class);
                logger.info(stcompleteinfo.getSno()+"学生信息从redis缓存中获取....");
            }else {
                stcompleteinfo = stcompleteinfoMapper.selectByPrimaryKey(sno);
                if(stcompleteinfo!=null){
                    jedisClient.set(key,JsonUtils.objectToJson(stcompleteinfo),60*60*24);
                }

            }

            if (stcompleteinfo == null) {
                logger.info(sno+"：该学号不存在");
                throw new StudentException("该学号不存在");
            } else {
                logger.info(sno+"：获取学生信息成功");
                jsonResult.setSuccess(1);
                jsonResult.setMsg("success");
                jsonResult.setData(stcompleteinfo);
            }
            return jsonResult;
        }
    }

    @Override
    public boolean isspotRegister(String userId) throws StudentException {

        Basicinfo basicinfo=basicinfoMapper.selectByPrimaryKey(userId);
        if(basicinfo==null){

            throw new StudentException(userId+":用户不存在");
        }else {
            //用户存在
            if (basicinfo.getSpotregister() != null && "已报到".equalsIgnoreCase(basicinfo.getSpotregister().trim())){
                if("已报到".equalsIgnoreCase(basicinfo.getSpotregister().trim())){
                    throw new StudentException(userId+":该用户之前已经报到了");
                }else{
                    //已经报道
                    basicinfo.setSpotregister("已报到");
                    basicinfoMapper.updateByPrimaryKey(basicinfo);
                    return true;
                }
//            if("已报道".equalsIgnoreCase(basicinfo.getSpotregister().trim())){
////                basicinfo.getSpotregister().trim().equalsIgnoreCase("已报道")
//                System.out.println(userId+":该用户之前已经报到了");
//                throw new StudentException(userId+":该用户之前已经报到了");
            }else {
                basicinfo.setSpotregister("已报到");
                basicinfoMapper.updateByPrimaryKey(basicinfo);
                return true;
            }
        }
    }

    /**
     * 获取结果计算成绩
     * @param problems
     * @return
     */
    @Override
    public JsonResult getGrade(List<Problem> problems) {
        return null;
    }


    /**
     * 修改学生登陆密码
     * @param studentLogin
     * @return
     */
    @Override
    public JsonResult changelogininfo(ChangeStudentLogin studentLogin) {
        Studentlogin login=studentloginMapper.selectByPrimaryKey(studentLogin.getSno());
        JsonResult jsonResult=JsonResult.createJsonResult();
        if(login!=null){
            if(studentLogin.getOldpassword().trim().equals("")||studentLogin.getOldpassword()==null){
                jsonResult.setMsg("旧密码不能为空");
                jsonResult.setSuccess(0);
            }else if (studentLogin.getPassword().trim().equals("")||studentLogin.getPassword()==null){
                jsonResult.setMsg("新密码不能为空");
                jsonResult.setSuccess(0);
            }else if(studentLogin.getPassword1().trim().equals("")||studentLogin.getPassword1()==null){
                jsonResult.setMsg("新密码不能为空");
                jsonResult.setSuccess(0);
            }else if(login.getPassword().trim().equals(studentLogin.getOldpassword())){
                login.setPassword(studentLogin.getPassword());
                int num=studentloginMapper.updateByPrimaryKey(login);
                if(num>0){
                    jsonResult.setSuccess(1);
                    jsonResult.setMsg("修改密码成功");
                }else {
                    jsonResult.setSuccess(0);
                    jsonResult.setMsg("修改密码失败,未知错误");
                }
            }else {
                jsonResult.setMsg("修改密码失败,旧密码输入错误");
                jsonResult.setSuccess(0);
            }

        }else {
            jsonResult.setMsg("修改密码错误,该用户不存在");
            jsonResult.setSuccess(0);
        }



        return jsonResult;
    }

    @Override
    public JsonResult selectStudentIsGetSchool(Basicinfo basicinfo) throws IOException {
        JsonResult jsonResult=JsonResult.createJsonResult();

        List<Basicinfo> basicinfosql=null;
        String key=CacheGetKeyUtils.getListbaseinfobynamess(basicinfo.getName(),basicinfo.getIdnum());
        if(jedisClient.exists(key)){
            basicinfosql=JsonUtils.jsonToList(jedisClient.get(key),Basicinfo.class);
            logger.info(basicinfo.getName()+"相关录取信息从redis缓存中获取");
        }else {
            BasicinfoExample example=new BasicinfoExample();
            BasicinfoExample.Criteria criterion=example.createCriteria();
            criterion.andNameEqualTo(basicinfo.getName());
            criterion.andIdnumEqualTo(basicinfo.getIdnum());
            basicinfosql=  basicinfoMapper.selectByExample(example);
            if(basicinfosql!=null&&basicinfosql.size()>0) {
                logger.info(basicinfo.getName() + "相关录取信息加入redis缓存");
                jedisClient.set(key,JsonUtils.objectToJson(basicinfosql),60*60*24);
            }
        }

        if(basicinfosql==null||basicinfosql.size()==0){
            jsonResult.setSuccess(0);
            jsonResult.setMsg("暂未查询到您的录取信息,请稍后再试");
        }else {
            for(int i=0;i<basicinfosql.size();i++) {
                Basicinfo sqls = basicinfosql.get(i);
                if(sqls.getIdnum()==null){
                    sqls.setIdnum("");
                }
                if (sqls.getIdnum().trim().equals(basicinfo.getIdnum())) {
                    jsonResult.setSuccess(1);
                    basicinfo.setCollege(sqls.getCollege());
                    basicinfo.setMajor(sqls.getMajor());
                    basicinfo.setRemarkOne(sqls.getRemarkOne());
                    basicinfo.setHelpertel(sqls.getHelpertel());
                    jsonResult.setMsg("恭喜您已被录取");
                    jsonResult.setData(basicinfo);
                    return jsonResult;
                }
            }

        }
        jsonResult.setSuccess(0);
        jsonResult.setMsg("暂未查询到您的录取信息,请稍后再试");
        return jsonResult;
    }


    /**
     * 公告查询，返回最新的一条
     * @return
     */
    @Override
    public JsonResult studentPublicInfo(String type) {
        JsonResult jsonResult=JsonResult.createJsonResult();
        if(type==null||type.trim().equals("")){
            jsonResult.setSuccess(0);
            jsonResult.setMsg("500：type is error");
        }else {
            //            NewsExample example=new NewsExample();
            News news=newsMapper.selectPublicInfo(type);

            if(news!=null){
                jsonResult.setSuccess(1);
                jsonResult.setMsg("获取公告成功");
                jsonResult.setData(news);
                news=null;
            }else{
                jsonResult.setSuccess(0);
                jsonResult.setMsg("暂无公告");
            }
        }
        return jsonResult;
    }

}