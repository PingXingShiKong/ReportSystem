package com.i1314i.reportsystem.service.impl;

import com.i1314i.reportsystem.service.IdentityService;
import com.i1314i.reportsystem.service.ScanQRCodeService;
import com.i1314i.reportsystem.service.StudentSideService;
import com.i1314i.reportsystem.utils.weixin.ParamesAPI.util.ParamesAPI;
import com.i1314i.reportsystem.utils.weixin.ParamesAPI.util.WeixinUtil;
import com.i1314i.reportsystem.utils.weixin.contacts.util.MPerson;
import com.i1314i.reportsystem.utils.weixin.msg.Util.SMessage;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service("StudentSideService")
public class StudentSideServiceImpl implements StudentSideService {

    @Autowired
    IdentityService identityService;
    @Autowired
    ScanQRCodeService scanQRCodeService;
    @Override
    public JSONObject getMPerson(String userId, String state) throws IOException {
        MPerson mper=new MPerson();
        String agentid_secret[] = WeixinUtil.getAgentidSecret(state).split("123456");
        String access_token = identityService.getTekon(agentid_secret[0],agentid_secret[1]);
        //String getpersonURL=mper.GET_PERSON_URL.replace("ACCESS_TOKEN", access_token).replace("ID", userId);

        String getpersonURL = "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token="+access_token+"&userid=" + userId;
        JSONObject jsonobject = WeixinUtil.HttpRequest(getpersonURL, "GET", null);
        System.out.println("jsonobject:"+jsonobject);   //输出jsonobject
        return jsonobject;
    }



    @Override
    public String phoneChange(String userId, String mobile, String state) throws IOException{
        String agentid = "txl";
        String secret = ParamesAPI.secret_txl;
//        String secret = "24UimL8sekcyZ9KYQ0gh4pr6TVyuZ97jqtoqnkWI_KFMScmxb5-pZpkM9g6VQlCS";
        String access_token=identityService.getTekon(agentid,secret);
        //下面两个参数判断成不成功 0表示成功 1表示失败 先change后post  用"-"分隔
        String change;
        String post;
        //开始修改用户信息
        //获取用户新手机号
        String newtel=mobile;
        //创建成员对象
        MPerson mperson=new MPerson();
        //拼接url
        String posturl=mperson.UPDATA_URL.replace("ACCESS_TOKEN", access_token);
//        String posturl = "https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token=" + access_token;
//        String servername=request.getServerName();   //获取域名
        String PostData=mperson.UpdataInfo(userId,  newtel);
        System.out.println("修改之后的数据包"+PostData);
        int result = WeixinUtil.PostMessage(access_token, "POST", posturl, PostData);
        // 打印结果
        if(0==result){
            System.out.println("修改信息成功!");
            //创建发送消息对象
            SMessage smsg=new SMessage();
            //拼接url
            String RequestURL=smsg.POST_URL.replace("ACCESS_TOKEN",access_token);
            //Post数据
            String stext="修改信息成功！\n"+"当前手机号为："+newtel;

            Boolean postText = scanQRCodeService.postTextMsg(userId,stext);

//            int result1 = WeixinUtil.PostMessage(access_token, "POST", RequestURL,PostData1);
            // 打印结果
            if (postText) {
                System.out.println("发送消息成功");
                post = "0";
            } else {
                System.out.println("发送消息失败");
                post = "1";
            }
            change = "0";
        }
        else {
//            System.out.println("操作失败");
//            request.setAttribute("msg","修改信息失败！");
//            request.getRequestDispatcher("/WEUI/operate_fail.jsp").forward(request,response);
//            //response.sendRedirect("/sdauweixin/WEUI/operate_fail.jsp");
            change = "1";
            post = "1";
        }
        return change + "-" +post;
    }
}
