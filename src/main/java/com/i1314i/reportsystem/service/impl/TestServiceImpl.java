package com.i1314i.reportsystem.service.impl;

import com.i1314i.reportsystem.service.TestService;
import org.springframework.stereotype.Service;

/**
 * @author 平行时空
 * @created 2018-05-09 21:50
 **/
@Service("testService")
public class TestServiceImpl implements TestService{
}
