package com.i1314i.reportsystem.utils;

/**
 * @author 平行时空
 * @created 2018-06-06 9:19
 **/
public class CacheGetKeyUtils {
    public  static  String getProblemKey(String sno){
        return "problem_"+sno;
    }

    public  static  String getQrKey(String sno){
        return "qr_"+sno;
    }
    public static String getStudentstcompleteinfo(String sno){
        return "stcompleteinfo_"+sno;
    }
    public static String getStudentbasicinfo(String sno){
        return "basicinfo_"+sno;
    }
    public static String getWechatToken(String id){
        return "wechattoken_"+id;
    }

    public static String getListbaseinfobyname(String name){
        return "listbaseinfo_"+name;
    }

    public static String getListbaseinfobynamess(String name,String sid){
        return "listbaseinfo_"+name+"_"+sid;
    }
}
