package com.i1314i.reportsystem.utils;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author 平行时空
 * @created 2018-04-22 16:14
 **/
public class CacheUtils {
    private static Jedis jedis=null;
    private static Map<String,String> concurrentHashMap=new ConcurrentHashMap<String,String>();
    public static Map<String,String> qrconcurrentHashMap=new ConcurrentHashMap<String,String>();
    public static void putData(String key,Object object) throws JsonProcessingException {
         concurrentHashMap.put(key,JsonUtils.objectToJson(object));
    }
    public static <T> T getData(String key,Class<T> type) throws IOException {
        return JsonUtils.jsonToObject(concurrentHashMap.get(key).toString(),type);
    }

    public static <T> List<T> getListDatas(String key, Class<T> type) throws IOException {
        return JSON.parseArray(concurrentHashMap.get(key),type);
    }

    public static <T> List<T> getListData(String key, Class<T> type) throws IOException {
        return JsonUtils.jsonToList(concurrentHashMap.get(key),type);
    }
    public static void removeData(String key){
        concurrentHashMap.remove(key);
    }
    public static boolean containsKey(String key){
        return  concurrentHashMap.containsKey(key);
    }
    public static Integer size(){
        return concurrentHashMap.size();
    }
    public static void clearData(){
        concurrentHashMap.clear();
    }

    public static Map<String, String> getConcurrentHashMap() {
        return concurrentHashMap;
    }

    public static String getKeyName(String id,String yours){
        return yours+"_"+id;
    }

}
