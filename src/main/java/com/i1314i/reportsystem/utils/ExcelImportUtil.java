package com.i1314i.reportsystem.utils;


import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 读取Excel表工具类
 */

/**
 * 返回list，list.get(i）得到第i+1行，list.get(i).get(i)得到第i+1行第i列
 */

public class ExcelImportUtil {
    /**
     * 读取excel返回list
     *
     * @param file
     * @return
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public static List readExcel(MultipartFile file) throws ClassNotFoundException, IOException {
//        List<User> list = new ArrayList<>();
        List<Map<Integer, String>> list = new ArrayList<>();
        InputStream inputStream = file.getInputStream();

        boolean isExcel2003 = true;// 根据文件名判断文件是2003版本还是2007版本
        if (isExcel2007(file.getOriginalFilename())) {
            isExcel2003 = false;
        }
        Workbook book = createWorkboot(inputStream, isExcel2003);
        Sheet sheet = book.getSheetAt(0);
        int firstRow = sheet.getFirstRowNum();
        int lastRow = sheet.getLastRowNum();
//        for (int i = firstRow + 1; i < lastRow + 1; i++) {
//
//            Row row = sheet.getRow(i);
//            int firstCell = row.getFirstCellNum();
//            int lastCell = row.getLastCellNum();
//
//            User user = new User();
//            user.setAge(row.getCell(firstCell).toString());
//            user.setSex(row.getCell(firstCell++).toString());
//            user.setName(row.getCell(firstCell++).toString());
//            list.add(user);
//
//        }

        for (int i = firstRow +1; i < lastRow + 1; i++) {
            Map map = new HashMap();

            Row row = sheet.getRow(i);
            int firstCell = row.getFirstCellNum();
            int lastCell = row.getLastCellNum();

            for (int j = firstRow; j < lastCell; j++) {

                Cell cell2 = sheet.getRow(firstRow +1).getCell(j);
//                String key = cell2.getStringCellValue();

                Cell cell = row.getCell(j);

                if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                }
                String val = cell.getStringCellValue();

//                if (i == firstRow + 1) {
//                    break;
//                } else {
                    map.put(j, val);

//                }
            }
//            if (i != firstRow + 1) {
                list.add(map);
//            }
        }
        return list;
    }

    /**
     * 创建workbook
     *
     * @param
     * @return
     */
    public static Workbook createWorkboot(InputStream in, boolean isExcel2003) throws IOException {
        Workbook wb = null;
        if (isExcel2003) {// 当excel是2003时,创建excel2003
            wb = new HSSFWorkbook(in);
        } else {// 当excel是2007时,创建excel2007
            wb = new XSSFWorkbook(in);
        }
        return wb;
    }

    // @描述：是否是2003的excel，返回true是2003
    public static boolean isExcel2003(String filePath) {
        return filePath.matches("^.+\\.(?i)(xls)$");
    }

    //@描述：是否是2007的excel，返回true是2007
    public static boolean isExcel2007(String filePath) {
        return filePath.matches("^.+\\.(?i)(xlsx)$");
    }
}
