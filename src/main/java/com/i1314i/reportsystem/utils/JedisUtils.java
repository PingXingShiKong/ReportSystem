package com.i1314i.reportsystem.utils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.io.IOException;


/**
 * @author 平行时空
 * @created 2018-04-22 15:26
 * redis操作工具
 **/
public class JedisUtils  {
    private static  JedisPool POOL = null;
    private static  String host=null;
    private static  Integer port=null;

    static {
        try {
            host= TemplateUtils.getPropertiesdata("dao.properties","jedis.host");
            port= Integer.valueOf(TemplateUtils.getPropertiesdata("dao.properties","jedis.port"));
            JedisPoolConfig config=new JedisPoolConfig();
            config.setMaxTotal(50);
            config.setMaxIdle(10);
            int timeout=100000;
            POOL=new JedisPool(config,host,port,timeout);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
 * 返回jedis连接
 * */
    public static Jedis getJedis() throws IOException {
        Jedis jedis=POOL.getResource();
        jedis.auth(TemplateUtils.getPropertiesdata("dao.properties","jedis.password"));
        return jedis;
    }
}
