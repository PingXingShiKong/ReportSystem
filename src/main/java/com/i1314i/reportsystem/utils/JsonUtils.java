package com.i1314i.reportsystem.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

/**
 * json转换插件
 */
public class JsonUtils {


    private static final ObjectMapper objectMapper=new ObjectMapper();

    public static String objectToJson(Object object) throws JsonProcessingException {
        String string =objectMapper.writeValueAsString(object);
        return string;
    }
    public static <T> T jsonToObject(String jsonData,Class<T> type) throws IOException {
        T t=objectMapper.readValue(jsonData,type);
        return t;
    }
    public static <T> List<T> jsonToList(String jsonData, Class<T> beanType) throws IOException {
        JavaType javaType=objectMapper.getTypeFactory().constructParametricType(List.class,beanType);
        List<T> list=objectMapper.readValue(jsonData,javaType);
        return list;
    }
}
