package com.i1314i.reportsystem.utils;


import com.i1314i.reportsystem.utils.Os.Platform;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.*;

/**
 * @author 平行时空
 * @created 2018-06-25 17:02
 **/
public class OSUtils {
    // 获取操作平台信息
    public static String getOsPrefix() {
        String arch = System.getProperty("Os.arch").toLowerCase();
        final String name = System.getProperty("Os.name");
        String osPrefix;
        switch (Platform.getOSType()) {
            case Platform.WINDOWS: {
                if ("i386".equals(arch))
                    arch = "x86";
                osPrefix = "win32-" + arch;
            }
            break;
            case Platform.LINUX: {
                if ("x86".equals(arch)) {
                    arch = "i386";
                } else if ("x86_64".equals(arch)) {
                    arch = "amd64";
                }
                osPrefix = "linux-" + arch;
            }
            break;
            default: {
                osPrefix = name.toLowerCase();
                if ("x86".equals(arch)) {
                    arch = "i386";
                }
                if ("x86_64".equals(arch)) {
                    arch = "amd64";
                }
                int space = osPrefix.indexOf(" ");
                if (space != -1) {
                    osPrefix = osPrefix.substring(0, space);
                }
                osPrefix += "-" + arch;
            }
            break;

        }

        return osPrefix;
    }

    /**
     * 获取系统名称
     *
     * @return
     */
    public static String getOsName() {
        String osName = "";
        String osPrefix = getOsPrefix();
        if (osPrefix.toLowerCase().startsWith("win32-x86")
                || osPrefix.toLowerCase().startsWith("win32-amd64")) {
            osName = "win";
        } else if (osPrefix.toLowerCase().startsWith("linux-i386")
                || osPrefix.toLowerCase().startsWith("linux-amd64")) {
            osName = "linux";
        }

        return osName;
    }

    // 获取加载库
    public static String getLoadLibrary(String library) {
        if (isChecking()) {
            return null;
        }

        String loadLibrary = "";
        String osPrefix = getOsPrefix();
        if (osPrefix.toLowerCase().startsWith("win32-x86")) {
            loadLibrary = "./libs/win32-x86/";
        } else if (osPrefix.toLowerCase().startsWith("win32-amd64")) {
            loadLibrary = "./libs/win32-amd64/";
        } else if (osPrefix.toLowerCase().startsWith("linux-i386")) {
            loadLibrary = "";
        } else if (osPrefix.toLowerCase().startsWith("linux-amd64")) {
            loadLibrary = "";
        }

        return loadLibrary + library;
    }

    private static boolean checking = false;

    public static void setChecking() {
        checking = true;
    }

    public static void clearChecking() {
        checking = false;
    }

    public static boolean isChecking() {
        return checking;
    }




        //通过截取cmd流方式得到计算机的配置信息(不好)
        public static List<String> getIpAddress() {
            Process p = null;
            List<String> address = new ArrayList<String>();
            try {
                p = new ProcessBuilder("ipconfig", "/all").start();
            } catch (Exception e) {
                return address;
            }
            StringBuffer sb = new StringBuffer();
            //读取进程输出值
            InputStream inputStream = p.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            String s = "";
            try {
                while ((s = br.readLine()) != null) {
                    sb.append(s + "\n");
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            System.out.println(sb);
            return address;
        }


          //获取用户名
        public static String getComputerUserName(){
            Map<String, String> map = System.getenv();
            return  map.get("USERNAME");//获取用户名
        }

          //获取计算机名
        public static String getComputerName(){
            Map<String, String> map = System.getenv();
            return map.get("COMPUTERNAME");//获取计算机名
        }
          //获取计算机域名
        public static String getComputermianUrl(){
            Map<String, String> map = System.getenv();
            return map.get("USERDOMAIN");//获取计算机域名
        }

        //得到计算机的ip地址和mac地址
        public static void getConfig() {
            try {
                InetAddress address = InetAddress.getLocalHost();
                NetworkInterface ni = NetworkInterface.getByInetAddress(address);
                //ni.getInetAddresses().nextElement().getAddress();
                byte[] mac = ni.getHardwareAddress();
                String sIP = address.getHostAddress();
                String sMAC = "";
                Formatter formatter = new Formatter();
                for (int i = 0; i < mac.length; i++) {
                    sMAC = formatter.format(Locale.getDefault(), "%02X%s", mac[i],
                            (i < mac.length - 1) ? "-" : "").toString();

                }
                System.out.println("IP：" + sIP);
                System.out.println("MAC：" + sMAC);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //得到计算机的ip,名称,操作系统名称,操作系统版本
        public static void Config() {
            try {
                InetAddress addr = InetAddress.getLocalHost();
                String ip = addr.getHostAddress().toString(); //获取本机ip
                String hostName = addr.getHostName().toString(); //获取本机计算机名称
                System.out.println("本机IP：" + ip + "本机名称:" + hostName);
                Properties props = System.getProperties();
                System.out.println(getOsName());
                System.out.println("操作系统的名称：" + props.getProperty("Os.name"));
                System.out.println("操作系统的版本：" + props.getProperty("Os.version"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }




}

