package com.i1314i.reportsystem.utils.Thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.ScanParams;
import redis.clients.jedis.ScanResult;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

/**
 * @author 平行时空
 * @created 2018-08-30 19:20
 **/
public class CacheBodyThread implements Callable<Long> {
    private final static Logger LOGGER = LoggerFactory
            .getLogger(CacheBodyThread.class);

    String threadName;
    Jedis jedis;
    Pipeline pipeline;
    boolean isDel;// isDel 清除 TRUE 统计FALSE
    ScanParams params;
    CountDownLatch countDownLatch;

    public CacheBodyThread(String threadName, Jedis jedis, Pipeline pipeline,
                           boolean isDel, ScanParams params, CountDownLatch countDownLatch) {
        super();
        this.threadName = threadName;
        this.jedis = jedis;
        this.pipeline = pipeline;
        this.isDel = isDel;
        this.params = params;
        this.countDownLatch = countDownLatch;
    }

    @SuppressWarnings("deprecation")
    @Override
    public Long call() throws Exception {
        long sum =0;
        LOGGER.info("【" + threadName + "】线程,统计缓存thread正在执行中========");

//		List<String> keys = null;
//		keys = new ArrayList<String>(); // 待处理key集合

        String scanRet = "0";// 起始游标
        ScanResult<String> scanResult = null;

        do {
            scanResult = jedis.scan(scanRet, params);
            scanRet = scanResult.getStringCursor();
            sum += scanResult.getResult().size();
            if (isDel) {
                pipeline = jedis.pipelined();// 批量操作
                for (String key_i : scanResult.getResult()) {
                    pipeline.del(key_i);
                }
                pipeline.sync();// 执行
            }
        }
        while (0 != scanResult.getCursor());/** 未来版本此方法会删除 **/
        if (null != pipeline) {
            try {
                pipeline.close();
                LOGGER.info("pipeline管道连接已关闭===============");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (null != jedis) {
            jedis.close();// 关闭连接
            LOGGER.info("jedis连接已关闭===============");
        }
        countDownLatch.countDown();
        LOGGER.info("【" + threadName + "】线程执行完毕========");
        return sum;
    }

}

