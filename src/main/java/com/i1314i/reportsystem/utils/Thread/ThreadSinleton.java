package com.i1314i.reportsystem.utils.Thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 线程池
 * @author 平行时空
 * @created 2018-08-30 19:07
 **/
public class ThreadSinleton {
    private final static Logger LOGGER = LoggerFactory.getLogger(ThreadSinleton.class);

    private static class SingletonHolder {
        static ExecutorService es = null;
        static {
            es = Executors.newFixedThreadPool(Runtime.getRuntime()
                    .availableProcessors());
            LOGGER.error("线程池初始化完成！！！！");

        }
        private static final ThreadSinleton sinleton = new ThreadSinleton();
    }

    private ThreadSinleton() {
    }

    public static final ThreadSinleton getSinleton() {
        return SingletonHolder.sinleton;
    }

    public static final ExecutorService getExecutorService() {
        return SingletonHolder.es;
    }
}

