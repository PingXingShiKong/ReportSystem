package com.i1314i.reportsystem.utils.weixin.ScanQRCode.util;

import com.i1314i.reportsystem.po.Basicinfo;

public class ParseBasicInfo {

    public static String parseBasicinfo(Basicinfo basicinfo){
        String sno = basicinfo.getSno();
        String name = basicinfo.getName();
        String major = basicinfo.getMajor();
        String classnum = basicinfo.getClassnum();
        String dormnum = basicinfo.getDormnum();
        String responseText = "报到成功" + "\n学号：" + sno +"\n姓名：" + name + "\n班级：" + classnum
                + "\n专业：" + major + "\n宿舍号：" + dormnum;
        return responseText;
    }
}
