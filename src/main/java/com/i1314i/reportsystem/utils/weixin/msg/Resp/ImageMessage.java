package com.i1314i.reportsystem.utils.weixin.msg.Resp;

/**
 *
 * @author Engineer.Jsp
 * @date 2014.10.08*
 */
public class ImageMessage extends BaseMessage {

	private Image Image;

	public Image getImage() {
		return Image;
	}

	public void setImage(Image image) {
		Image = image;
	}
}
