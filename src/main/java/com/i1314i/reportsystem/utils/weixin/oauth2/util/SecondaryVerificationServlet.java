package com.i1314i.reportsystem.utils.weixin.oauth2.util;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import com.i1314i.reportsystem.utils.weixin.ParamesAPI.util.ParamesAPI;
import com.i1314i.reportsystem.utils.weixin.ParamesAPI.util.WeixinUtil;
import net.sf.json.JSONObject;
/**二次验证的servlet
 * Created by ThinkPad on 2017/4/20 0020.
 */
public class SecondaryVerificationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        // 将请求、响应的编码均设置为UTF-8（防止中文乱码）
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        String code = request.getParameter("code");

        if (!"authdeny".equals(code)) {

            // 调取凭证
            String access_token = WeixinUtil.getAccessToken(ParamesAPI.corpId, ParamesAPI.secret_stu).getToken();

            // 打印凭证
            out.print("凭证:"+access_token);

            // agentid 跳转链接时所在的企业应用ID 管理员须拥有agent的使用权限；agentid必须和跳转链接时所在的企业应用ID相同
           // String UserID = GOauth2Core.GetUserID(access_token, code, "0");

            // 二次验证地址
           // String RequestURL = ParamesAPI.REDIRECT_URI.replace("ACCESS_TOKEN",access_token).replace("USERID", UserID);

            // 二次验证返回数据
            //JSONObject jsonObject = WeixinUtil.HttpRequest(RequestURL, "GET", null);

            // 二次验证结果
            //out.print("二次验证返回结果：\n"+jsonObject);

            // 传值
           // request.setAttribute("jsonObject", jsonObject);
        }
        else{
            out.print("Code获取失败，至于为什么，自己找原因。。。");
        }
    }
}
