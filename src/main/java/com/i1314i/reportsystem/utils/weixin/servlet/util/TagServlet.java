package com.i1314i.reportsystem.utils.weixin.servlet.util;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import com.i1314i.reportsystem.utils.weixin.ParamesAPI.util.ParamesAPI;
import com.i1314i.reportsystem.utils.weixin.ParamesAPI.util.WeixinUtil;
import com.i1314i.reportsystem.utils.weixin.contacts.util.MTag;

/**
 * Created by Administrator on 2017/4/24.
 */
@WebServlet(name = "TagServlet")
public class TagServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      //调取凭证
      String access_token = WeixinUtil.getAccessToken(ParamesAPI.corpId, ParamesAPI.secret_stu).getToken();
      //创建标签对象
      MTag mtag=new MTag();
      //接收数据
      String tag=request.getParameter("addtag");
      String person=request.getParameter("tagperson");
      String TagPost=mtag.Create_Tag("辍学");
      String addTagPersonPost=mtag.Create_Tag("赵四");
      //提交数据，获取结果
        int tagresult = WeixinUtil.PostMessage(access_token, "POST", mtag.CREATE_TAG_URL, TagPost );
        int personresult=WeixinUtil.PostMessage(access_token,"POST",mtag.ADD_TAG_PERSON,addTagPersonPost);
        // 打印结果
        if(0==tagresult&&0==personresult){
            System.out.println("操作成功");
        }
        else {
            System.out.println("操作失败");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
