package com.i1314i.reportsystem.utils.weixin.test;

import com.i1314i.reportsystem.utils.weixin.ParamesAPI.util.ParamesAPI;
import com.i1314i.reportsystem.utils.weixin.ParamesAPI.util.WeixinUtil;

import com.i1314i.reportsystem.utils.weixin.msg.Util.SMessage;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "TestServlet")
public class TestServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("进入此servlet");
        String access_token = WeixinUtil.getAccessToken(ParamesAPI.corpId, ParamesAPI.secret_stu).getToken();
        SMessage sMessage = new SMessage();
        String URL = sMessage.POST_URL.replace("ACCESS_TOKEN",access_token);
////        //String Data = sMessage.STextMsg("20164117|20155619",null,null,"1000010","hello");
////        System.out.println(Data);
////        int result1 = WeixinUtil.PostMessage(access_token, "POST", URL,Data);
//        // 打印结果
//        if (0 == result1) {
//            System.out.println("发送消息成功");
//        } else {
//            System.out.println("发送消息失败");
//        }
    }
}
