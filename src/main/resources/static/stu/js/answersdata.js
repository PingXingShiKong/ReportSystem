layui.config({
    base:'/js/'
}).use(['layer','form','element'], function() {
    var layer = layui.layer;
    var form = layui.form;
    var element = layui.element;
    var size=0;
    var problems=$("#answers");
    $(function () {
        var loading=null;
        //加载层-默认风格
        layer.ready(function(){
            loading=layer.load();
        });
        $.ajax({
            type:'post',
            url:geturl()+'answer/getanswer',
            contentType:'application/json; charset=utf-8',
            dataType:'json',
            cache:false,
            success:function(data){//返回json结果
                layer.close(loading);
                if(data.success==1){
                    problems.empty();
                    $.each(data.data, function (n, value) {

                        var result = '<div class="weui-form-preview">'
                             +'<div class="weui-form-preview__hd">'
                                +'<label class="weui-form-preview__label">题目'+(n+1)+'答案：</label>'
                                    +'<em class="weui-form-preview__value" style="color: red;">'+value.answer+'</em>'
                            +'</div>'
                            +' <div class="weui-form-preview__bd">'
                               +'<div class="" style="text-align: center; color: black;">'
                                  +value.problemtext
                                +' </div>'
                             +'</div>'
                        +'</div>'
                        +' <br/>'
                        // console.log(result);
                        problems.append(result);

                    });

                    resta='<a href="javascript:;" class="weui-btn weui-btn_primary" id="trigger">答案已完结</a>'
                    problems.append(resta);

                }else {
                    problems.empty();
                    var result='<div class="weui-form-preview">'
                        +data.msg
                        +'</div>'
                    problems.append(result);
                }

            },
            error:function(error){
                layer.close(loading);

                if(isWeiXin()){
                    window.location.href=geturl()+  'weixin/fail';
                }else{
                    layer.msg('信息加载错误'+error.status);
                }
            },
        });
        setTimeout(function(){
            layer.close(loading);
        }, 2000);
    });


    $(document.body).pullToRefresh(function() {
        setTimeout(function() {
            layer.ready(function(){
                loading=layer.load();
            });
            $.ajax({
                type:'post',
                url:geturl()+'answer/getanswer',
                contentType:'application/json; charset=utf-8',
                dataType:'json',
                cache:false,
                success:function(data) {//返回json结果
                    layer.close(loading);
                    if (data.success == 1) {
                        problems.empty();
                        $.each(data.data, function (n, value) {

                            var result = '<div class="weui-form-preview">'
                                + '<div class="weui-form-preview__hd">'
                                + '<label class="weui-form-preview__label">题目' + (n + 1) + '答案：</label>'
                                + '<em class="weui-form-preview__value" style="color: red;">' + value.answer + '</em>'
                                + '</div>'
                                + ' <div class="weui-form-preview__bd">'
                                + '<div class="" style="text-align: center; color: black;">'
                                + value.problemtext
                                + ' </div>'
                                + '</div>'
                                + '</div>'
                                + ' <br/>'
                            // console.log(result);
                            problems.append(result);

                        });

                        resta = '<button id="show-login" class="weui-btn weui-btn_primary">刷新答案</button>'
                        problems.append(resta);

                    } else {
                        problems.empty();
                        var result = '<div class="weui-form-preview">'
                            + data.msg
                            + '</div>'
                        problems.append(result);
                    }
                }
            });
            $(document.body).pullToRefreshDone();

        }, 1000);
        setTimeout(function(){
            layer.close(loading);
        }, 2000);
    });

    $("#trigger").click(function () {
        $(document.body).pullToRefresh('triggerPullToRefresh')
    })


});

