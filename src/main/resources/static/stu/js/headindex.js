layui.config({
    base:'/js/'
}).use(['element','layer'], function() {
    var element = layui.element;
    var layer = layui.layer;

    $("#changeinfo").click(function () {
        layer.open({
            type: 2,
            title:'修改密码',
            area: ['500px', '300px'],
            content: [geturl()+'student/changeinfo', 'no'] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    });

    $("#changewechatphone").click(function () {
        layer.open({
            type: 2,
            title:'修改微信绑定手机号',
            area: ['500px', '300px'],
            content: [geturl()+'student/changephone', 'no'] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
    });

    $("#joinus").click(function () {
        layer.open({
            title:'加入我们',
            content:'如果您想加入我们并符合以下要求中的任意一条,欢迎您发送您的相关信息给我们<br/>'+'邮箱：1604030622@qq.com<br/>'+'<br/><p>1.前端：<br/>(1)了解并会使用jquey layui/bootstrap <br/>(2)熟悉并熟练使用jsp/thymeleaf<br/>(3)如果您之前有过前后端分离的经验 会使用（angular.js/vue.js）等技术那您将优先得到考虑</p><br/>(4)安卓端 如果你有安卓或者ios的开发经验(原生app)<br/>或者您有过非原生app（h5）的开发经验(mui)'+'<br/>2.后端：<br/>（1）熟悉并会使用ssm、springboot、shiro等流行框架，会使用redis(集群)、mysql等关系型和非关系型数据库'+'<br/>（2）了解并会使用json'+"<br/>（3）或者您如果有过微信平台（企业号）的相关开发经验（优先）<br/>(4)熟练使用git<br/>(5)其他 Dubbo Docker...(欢迎)<br/>如果您加入我们，您将会在这获得让你难以忘记的经历和技术，so..欢迎加入我们,期待您的邮件"
            // ,area: '500px'
            ,btn: ['已阅读']
            ,yes: function(index, layero){

                layer.close(index)

            }
            ,cancel: function(){
                //右上角关闭回调
                // window.location.href=geturl()+  'student/index';
                //return false 开启该代码可禁止点击该按钮关闭
            }
        });
    });

});


