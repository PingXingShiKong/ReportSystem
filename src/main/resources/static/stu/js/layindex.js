layui.config({
    base:'/js/'
}).use(['element'], function(){
    var element = layui.element;

    if(isPC()==false){
        window.location.href=geturl()+'student/test';
    }


    $(function () {
        $.ajax({
            type:'post',
            url:geturl()+'student/getstudentdatas',
            contentType:'application/json; charset=utf-8',
            dataType:'json',
            cache:false,
            success:function(data){//返回json结果
                if(data.success==1){
                    $("#myname").html('<img src="/images/getpicture" class="layui-nav-img"/>'+data.data.basicinfo.name);
                }else{
                    layer.open({
                        title:'错误信息',
                        content:data.msg
                        ,btn: ['确定']
                        ,yes: function(index, layero){
                            window.location.href=geturl()+ 'student/index';
                        }
                        ,cancel: function(){
                            //右上角关闭回调
                            window.location.href=geturl()+  'student/index';
                        }
                    });

                }
            },
            error:function(error){
                layer.msg('信息加载错误'+error.status);
            },
        });



        $("#findself").click(function () {
            window.location.href=geturl()+"student/info";
        });

        $("#uploadself").click(function () {
            window.location.href=geturl()+"student/uploadself";
        });
    });

});





