
layui.config({
	base : "/js/"
}).use(['form','layer','jquery'],function(){
	var form = layui.form(),
		layer = parent.layer === undefined ? layui.layer : parent.layer,
		$ = layui.jquery;
	//video背景
	$(window).resize(function(){
		if($(".video-player").width() > $(window).width()){
			$(".video-player").css({"height":$(window).height(),"width":"auto","left":-($(".video-player").width()-$(window).width())/2});
		}else{
			$(".video-player").css({"width":$(window).width(),"height":"auto","left":-($(".video-player").width()-$(window).width())/2});
		}
	}).resize();
    form.verify({
        code: function(value){
            if(value.length != 4){
               // layer.msg("验证码长度不正确");
                return '验证码长度不正确';
            }
        }
       // , sno:[/^[0-9]*$/,'学号必须为数字']
    });

    form.on('submit(login)',function(data){

        var datas=data.field;
       // alert(JSON.stringify(datas));11



        $.ajax({
            type:'post',
            url:geturl()+'login/studentlogincheck',
            contentType:'application/json; charset=utf-8',
            dataType:'json',
            cache:false,
            data:$("#yanzhengForm").toJson(),
            success:function(data){//返回json结果
                if(data.success==1){

                    if(isPC()){
                        window.location.href=geturl()+'student/index';
                    }else {
                        window.location.href=geturl()+'student/test';
                    }

                }else if(data.success==2){
                    window.location.href=geturl()+'xueyuan/collegeindex';//学院管理员
                }else if(data.success==3){
                    window.location.href=geturl()+'school/index';//超级管理员
                }else {
                    layer.msg(data.msg);
                }
            },
            error:function(error){
                layer.msg(error.status+'：未知错误');
            },
        });
        return false;
    });


});



