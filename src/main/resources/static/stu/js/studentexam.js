layui.config({
    base:'/js/'
}).use(['layer','form','element'], function() {
    var layer = layui.layer;
    var form = layui.form;
    var element = layui.element;
    var size=0;

    form.verify({

        rado: function(value,item){

            var Materielcategory = $(':radio[name='+item.name+']:checked').val();
            if (Materielcategory == null) {
                return "您的题目还未做完";
            }
            /*
            var s=$("#updateproblemForm").serialize();
            var list=s.split("&");
            if(list.length<size){
                return "题目未做完";
            }
            */
        }


    });



    form.on('submit(*)',function(data){
        $.ajax({
            type:'post',
            url:geturl()+'student/updateExam',
            contentType:'application/json; charset=utf-8',
            dataType:'json',
            cache:false,
            data: $("#updateproblemForm").toJson(),
            success:function(data){//返回json结果
                var problems=$("#problem");

                if(data.success==1){
                    var result ="<div class='tsd'>"
                        + "<button class='layui-btn layui-btn-fluid'>考试结果</button>"
                        + "<div class='tsdscore'>"+data.data+"分</div>"
                        + "<div class='tsds'><p>恭喜您已经通过考试</p></div>"
                        + "<div class='psbtn'><div id='successone' class='layui-btn layui-btn-radius layui-btn-normal' onclick='successone();'>查看报到信息</div></div>"
                     +"</div>";
                    problems.empty();
                    problems.append(result);

                    // layer.msg(data.msg+data.data);
                }else {

                    var result ="<div class='tsd'>"
                        + "<button class='layui-btn layui-btn-fluid'>考试结果</button>"
                        + "<div class='tsdscore'>"+data.data+"分</div>"
                        + "<div class='tsds'><p>很抱歉您未通过本次考试</p></div>"
                        + "<div class='psbtn'><div id='againone' class='layui-btn layui-btn-radius layui-btn-normal' onclick='againone();'>重来一次</div></div>"
                        +"</div>";
                    problems.empty();
                    problems.append(result);


                    // layer.msg(data.msg+data.data);
                }
            },
            error:function(error){
                layer.msg(error.status+'：未知错误');
            },
        });
        return false;
    });


    $(function () {
        var loading=null;
        var problems=$("#problem");
        //加载层-默认风格
        layer.ready(function(){
            loading=layer.load();
        });
        $.ajax({
            type:'post',
            url:geturl()+'student/getExam',
            contentType:'application/json; charset=utf-8',
            dataType:'json',
            cache:false,
            success:function(data){//返回json结果
                layer.close(loading);
                if(data.success==0){
                    problems.empty();
                    $.each(data.data, function (n, value) {

                        var result =
                            ' <div class="layui-card" style="width: 500px;">'
                            +'<blockquote class="layui-elem-quote">'
                            +'<h3>题目'+(n+1)+'：</h3>'
                            +value.problemtext
                            +'</blockquote>'
                            +'<div class="layui-card-body">'
                            +'<div class="layui-form-item">'
                            +'<div class="layui-input-block">'
                            +'<input type="radio" name="'+value.problemid+'" value="A" title="A: '+value.a+'" lay-verify="rado" ><br/>'
                            +'<input type="radio" name="'+value.problemid+'" value="B" title="B: '+value.b+'" ><br/>'
                            +'<input type="radio" name="'+value.problemid+'" value="C" title="C: '+value.c+'" ><br/>'
                            +'<input type="radio" name="'+value.problemid+'" value="D" title="D: '+value.d+'" ><br/>'
                            +'</div>'
                            +'</div>'
                            + '</div>'
                            +'</div>';
                        // console.log(result);
                        problems.append(result);

                    });
                    var subdiv="<br/>"
                        + "<div class='layui-form-item'>"
                        +"<button class='layui-btn' lay-submit lay-filter='*'>提交试题</button>"

                        +"</div>"
                        +"<br/>"
                        +"<br/>";
                    problems.append(subdiv);
                    form.render();
                }

            },
            error:function(error){
                layer.close(loading);

                if(isWeiXin()){
                    window.location.href=geturl()+  'weixin/fail';
                }else{
                    layer.msg('信息加载错误'+error.status);
                }
            },
        });
        setTimeout(function(){
            layer.close(loading);
        }, 2000);
    });




});


function againone() {
    window.location.href=geturl()+ 'student/problem';
}
function successone() {
    window.location.href=geturl()+ 'student/info';
}

$("#successone").click(function () {
    window.location.href=geturl()+ 'student/info';
});
$("#againone").click(function () {
    window.location.href=geturl()+ 'student/problem';
});


