
layui.config({
    base:'/js/'
}).use(['element','layer'], function() {
    var element = layui.element;
    var layer = layui.layer;


$(function () {
    var loading=null;
    //加载层-默认风格
    layer.ready(function(){
        loading=layer.load();
    });
    $.ajax({
        type:'post',
        url:geturl()+'student/getstudentdatas',
        contentType:'application/json; charset=utf-8',
        dataType:'json',
        cache:false,
        success:function(data){//返回json结果
            layer.close(loading);
            if(data.success==1){
                $("#sno").text(data.data.basicinfo.sno);
                $("#myname").html('<img src="/images/getpicture" class="layui-nav-img"/>'+data.data.basicinfo.name);



                if(data.data.basicinfo.name==null||data.data.basicinfo.name=='null'||data.data.basicinfo.name==''){
                    data.data.basicinfo.name='未知';
                }
                if(data.data.basicinfo.sex==null||data.data.basicinfo.sex=='null'||data.data.basicinfo.sex==''){
                    data.data.basicinfo.sex='未知';
                }
                if(data.data.basicinfo.examnum==null||data.data.basicinfo.examnum=='null'||data.data.basicinfo.examnum==''){
                    data.data.basicinfo.examnum='未知';
                }

                if(data.data.basicinfo.idnum==null||data.data.basicinfo.idnum=='null'||data.data.basicinfo.idnum==''){
                    data.data.basicinfo.idnum='未知';
                }
                if(data.data.basicinfo.campus==null||data.data.basicinfo.campus=='null'||data.data.basicinfo.campus==''){
                    data.data.basicinfo.campus='未知';
                }

                if(data.data.basicinfo.college==null||data.data.basicinfo.college=='null'||data.data.basicinfo.college==''){
                    data.data.basicinfo.college='未知';
                }

                if(data.data.basicinfo.major==null||data.data.basicinfo.major=='null'||data.data.basicinfo.major==''){
                    data.data.basicinfo.major='未知';
                }

                if(data.data.basicinfo.classnum==null||data.data.basicinfo.classnum=='null'||data.data.basicinfo.classnum==''){
                    data.data.basicinfo.classnum='未知';
                }


                if(data.data.basicinfo.dormnum==null||data.data.basicinfo.dormnum=='null'||data.data.basicinfo.dormnum==''){
                    data.data.basicinfo.dormnum='未知';
                }

                if(data.data.basicinfo.bankcardid==null||data.data.basicinfo.bankcardid=='null'||data.data.basicinfo.bankcardid==''){
                    data.data.basicinfo.bankcardid='未知';
                }

                if(data.data.basicinfo.paysta==null||data.data.basicinfo.paysta=='null'||data.data.basicinfo.paysta==''){
                    data.data.basicinfo.paysta='未知';
                }

                if(data.data.basicinfo.collegetel==null||data.data.basicinfo.collegetel=='null'||data.data.basicinfo.collegetel==''){
                    data.data.basicinfo.collegetel='未知';
                }

                if(data.data.basicinfo.schooltel==null||data.data.basicinfo.schooltel=='null'||data.data.basicinfo.schooltel==''){
                    data.data.basicinfo.schooltel='未知';
                }

                if(data.data.basicinfo.mastername==null||data.data.basicinfo.mastername=='null'||data.data.basicinfo.mastername==''){
                    data.data.basicinfo.mastername='未知';
                }

                if(data.data.basicinfo.mastertel==null||data.data.basicinfo.mastertel=='null'||data.data.basicinfo.mastertel==''){
                    data.data.basicinfo.mastertel='未知';
                }

                if(data.data.basicinfo.brother==null||data.data.basicinfo.brother=='null'||data.data.basicinfo.brother==''){
                    data.data.basicinfo.brother='未知';
                }

                if(data.data.basicinfo.brothertel==null||data.data.basicinfo.brothertel=='null'||data.data.basicinfo.brothertel==''){
                    data.data.basicinfo.brothertel='未知';
                }

                if(data.data.basicinfo.helper==null||data.data.basicinfo.helper=='null'||data.data.basicinfo.helper==''){
                    data.data.basicinfo.helper='未知';
                }

                if(data.data.basicinfo.helpertel==null||data.data.basicinfo.helpertel=='null'||data.data.basicinfo.helpertel==''){
                    data.data.basicinfo.helpertel='未知';
                }
                // $("#myname").text(data.data.basicinfo.name);
                $("#name").text(data.data.basicinfo.name);
                $("#sex").text(data.data.basicinfo.sex);
                $("#examnum").text(data.data.basicinfo.examnum);
                $("#idnum").text(data.data.basicinfo.idnum);
                $("#campus").text(data.data.basicinfo.campus);
                $("#college").text(data.data.basicinfo.college);
                $("#major").text(data.data.basicinfo.major);
                $("#classnum").text(data.data.basicinfo.classnum);
                $("#dormnum").text(data.data.basicinfo.dormnum);
                $("#bankcardid").text(data.data.basicinfo.bankcardid);
                $("#paysta").text(data.data.basicinfo.paysta);
                $("#collegetel").text(data.data.basicinfo.collegetel);
                $("#schooltel").text(data.data.basicinfo.schooltel);
                $("#mastername").text(data.data.basicinfo.mastername);
                $("#mastertel").text(data.data.basicinfo.mastertel);
                $("#brother").text(data.data.basicinfo.brother);
                $("#brothertel").text(data.data.basicinfo.brothertel);
                $("#helper").text(data.data.basicinfo.helper);
                $("#helpertel").text(data.data.basicinfo.helpertel);
                //window.location.href="/stu/index";
            }else{
                //layer.msg(data.msg);
//eg2
                if(isWeiXin()){
                    window.location.href=geturl()+  'weixin/fail';
                }else if(isPC()){
                    layer.open({
                        title:'错误信息',
                        content:data.msg
                        ,btn: ['确定']
                        ,yes: function(index, layero){
                            //按钮【按钮一】的回调
                            window.location.href=geturl()+ 'student/index';
                        }
                        ,cancel: function(){
                            //右上角关闭回调
                            window.location.href=geturl()+  'student/index';
                            //return false 开启该代码可禁止点击该按钮关闭
                        }
                    });
                }else {
                    window.location.href=geturl()+  'weixin/fail';
                }


            }
        },
        error:function(error){
            layer.close(loading);

            if(isWeiXin()){
                window.location.href=geturl()+  'weixin/fail';
            }else{
                layer.msg('信息加载错误'+error.status);
            }
        },
    });
    setTimeout(function(){
        layer.close(loading);
    }, 2000);
});

    $("#uploadself").click(function () {
        window.location.href=geturl()+"student/uploadself";
    });


});