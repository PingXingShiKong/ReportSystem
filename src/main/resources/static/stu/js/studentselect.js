layui.config({
    base:'/js/'
}).use(['layer','form','element'], function() {
    var layer = layui.layer;
    var form = layui.form;
    var element = layui.element;


    $(function () {
        var type="1";
        $.ajax({
            type:'post',
            url:geturl()+'getpublicinfo',
            contentType:'application/json; charset=utf-8',
            data:'{"type":"1"}',
            dataType:'json',
            cache:false,
            success:function(data) {//返回json结果
                if(data.success==1){
                    if(data.data.content!=null){
                        layer.open({
                            title:data.data.title,
                            content:'<h3>发布日期:'+data.data.inputtime+'</h3><br/>'+data.data.content
                            // ,area: '500px'
                            ,btn: ['已阅读']
                            ,yes: function(index, layero){

                                layer.close(index)

                            }
                            ,cancel: function(){
                                //右上角关闭回调
                                // window.location.href=geturl()+  'student/index';
                                //return false 开启该代码可禁止点击该按钮关闭
                            }
                        });
                    }
                }
            },
            error:function(error){
                layer.msg(error.status+'：未知错误');
            }

        })
    })

    form.on('submit(updates)',function(data){
        var problems=$("#card");
        $.ajax({
            type:'post',
            url:geturl()+'getStudentluqu',
            contentType:'application/json; charset=utf-8',
            dataType:'json',
            cache:false,
            data: $("#selectForm").toJson(),
            success:function(data){//返回json结果
                if(data.success==1){
                    if(data.data.college==null||data.data.college=='null'||data.data.college==''){
                        data.data.college="暂无";
                    }
                    if(data.data.major==null||data.data.major=='null'||data.data.major==''){
                        data.data.major="暂无";
                    }
                    if(data.data.helpertel!=null&&data.data.helpertel!='null'||data.data.helpertel!=''){
                        data.data.remarkOne="通知书已寄发";
                    }
                    if(data.data.helpertel==null||data.data.helpertel=='null'||data.data.helpertel==''){
                        data.data.remarkOne="通知书还未寄发";
                    }
                    var result ='<div class="tsd">'
                        + '<button class="layui-btn layui-btn-fluid">录取结果</button>'
                        + '<i class="layui-icon layui-icon-face-smile" style= " font-size:100px; color: #1E9FFF;"></i>'
                        + '<div class="tsdscore" style=" font-size:22px; color:red;">恭喜您,已经被录取</div>'
                        + '<p style="font-size:18px; margin-top: 10px;">学院：'+data.data.college+'</p>'
                        + '<p style="font-size:18px; margin-top: 10px;">专业：'+data.data.major+'</p>';
                    if(data.data.helpertel==null||data.data.helpertel=='null'||data.data.helpertel==''){
                        result+='<p style="font-size:18px; margin-top: 10px;">通知书邮寄进程：'+data.data.remarkOne+'</p>'
                            +'</div>';

                    }else{
                        $(".card").css("height","340");
                        result+='<p style="font-size:18px; margin-top: 10px;">通知书邮寄进程：'+data.data.remarkOne+'</p>'
                       + '<p style="font-size:18px; margin-top: 10px;">EMS订单号：'+data.data.helpertel+'</p>'
                            +'</div>';
                    }
                        // + '<p style="font-size:18px; margin-top: 10px;">通知书邮寄进程：'+data.data.remarkOne+'</p>'
                        // +'</div>';
                    problems.empty();
                    problems.append(result);
                }else {
                    var result=   '<button class="layui-btn layui-btn-fluid">录取结果</button>'
                        + '<div style="font-size: 20px; color: red; margin-top: 40px;">' +
                        '<i class="layui-icon layui-icon-face-surprised" style= " font-size:100px; color: #1E9FFF;"></i> ' +
                        '<p style="margin-top: 20px;">' +
                        data.msg
                        +
                        '</p>' +
                        '</div>';
                    problems.empty();
                    problems.append(result);
                    // layer.msg(data.msg);
                }
            },
            error:function(error){
                layer.msg(error.status+'：未知错误');
            }
        });
        return false;
    });
});