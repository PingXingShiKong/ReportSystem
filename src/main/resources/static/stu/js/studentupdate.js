layui.config({
    base:'/js/'
}).use(['layer','form','element','laydate'], function() {
    var layer = layui.layer;
    var form = layui.form;
    var element = layui.element;
    var laydate = layui.laydate;

    //执行一个laydate实例

    if(!isWeiXin()){
        laydate.render({
            elem: '#registertime', //指定元素
            format: 'yyyy-MM-dd', //可任意组合
            type: 'date'
            // ,min: '2018-5-13'
            // ,max: '2018-5-31'
            // range: true //或 range: '~' 来自定义分割字符
        });
    }



    //自定义验证规则
    form.verify({
        phonenum: function(value){
            if(value.length==0){
                // layer.msg("验证码长度不正确");
                return '联系方式不能为空';
            }
        },
        registertime: function(value){
            var reg = /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/;
            if(!reg.test(value)){
                return '报到时间格式不正确';
            }
            if(value.length==0){
                // layer.msg("验证码长度不正确");
                return '报到时间不能为空';
            }
        }

    });
    form.on('submit(updates)',function(data){
        $.ajax({
            type:'post',
            url:geturl()+'student/updateStcompleteinfo',
            contentType:'application/json; charset=utf-8',
            dataType:'json',
            cache:false,
            data: $("#updateForm").toJson(),
            success:function(data){//返回json结果
                if(data.success==1){
                    layer.msg("上传成功");

                    setTimeout(function(){
                        if(isWeiXin()){
                            window.location.href=geturl()+'weixin/success';
                        }else {
                            window.location.href=geturl()+'student/index';
                        }
                    }, 1000);


                }else {
                    layer.msg(data.msg);
                }
            },
            error:function(error){
                layer.msg(error.status+'：未知错误');
            },
        });
        return false;
    });

    $(function () {
        var loading=null;
        //加载层-默认风格
        layer.ready(function(){
            loading=layer.load();
        });
        $.ajax({
            type:'post',
            url:geturl()+'student/getStcompleteinfo',
            contentType:'application/json; charset=utf-8',
            dataType:'json',
            cache:false,
            success:function(data){//返回json结果
                layer.close(loading);
                if(data.success==1){
                    $("#myname").html('<img src="/images/getpicture" class="layui-nav-img"/>'+data.data.basicinfo.name);
                    $("#phonenum").attr('value',data.data.stcompleteinfo.phonenum);



                        $("input[name='bedding']").each(function(){
                            if($(this).val().trim()==data.data.stcompleteinfo.bedding.trim()){
                                $(this).prop( "checked", 'true');
                            }
                        });

                    $("input[name='militaryclothing']").each(function(){
                        if($(this).val().trim()==data.data.stcompleteinfo.militaryclothing.trim()){
                            $(this).prop( "checked", 'true');
                        }
                    });


                    $("#shoenum").val(data.data.stcompleteinfo.shoenum);


                    $("#height").val(data.data.stcompleteinfo.height);
                    $("#weight").val(data.data.stcompleteinfo.weight);

                    var count=$("#trafficway").get(0).options.length;
                    for(var i=0;i<count;i++){
                        if($("#trafficway").get(0).options[i].text == data.data.stcompleteinfo.trafficway.trim())
                        {
                            $("#trafficway").get(0).options[i].selected = true;
                            break;
                        }
                    }

                    $("#parentname").val(data.data.stcompleteinfo.parentname);

                    $("#parentphonenum").val(data.data.stcompleteinfo.parentphonenum);


                    $("#homeaddr").val(data.data.stcompleteinfo.homeaddr);

                    $("#studentpho").val(".");
                    $("input[name='loan']").each(function(){
                        if($(this).val().trim()==data.data.stcompleteinfo.loan.trim()){
                            $(this).prop( "checked", 'true');
                        }
                    });


                    $("#company").val(data.data.stcompleteinfo.company);
                    $("#registertime").val(data.data.stcompleteinfo.registertime);
                    //window.location.href="/stu/index";
                    form.render();
                }else{
                    //layer.msg(data.msg);
//eg2
                    if(isWeiXin()){
                        window.location.href=geturl()+  'weixin/fail';
                    }else {
                        layer.open({
                            title:'错误信息',
                            content:data.msg
                            ,btn: ['确定']
                            ,yes: function(index, layero){
                                //按钮【按钮一】的回调
                                window.location.href=geturl()+ 'student/index';
                            }
                            ,cancel: function(){
                                //右上角关闭回调
                                window.location.href=geturl()+  'student/index';
                                //return false 开启该代码可禁止点击该按钮关闭
                            }
                        });
                    }


                }
            },
            error:function(error){
                layer.close(loading);

                if(isWeiXin()){
                    window.location.href=geturl()+  'weixin/fail';
                }else{
                    layer.msg('信息加载错误'+error.status);
                }
            },
        });
        setTimeout(function(){
            layer.close(loading);
        }, 2000);
    });



});