$(function () {
    $.ajax({
        type:'post',
        url:geturl()+'student/getexamstatus',
        contentType:'application/json; charset=utf-8',
        dataType:'json',
        cache:false,
        // data: $("#updateproblemForm").toJson(),
        success:function(data){//返回json结果
            if(data.success==0){

                if(isWeiXin()){
                    //微信端
                    window.location.href=geturl()+"student/questions";
                }else if(isPC()){
                    window.location.href=geturl()+"student/problem";
                }else{
                    //手机端
                    window.location.href=geturl()+"student/questions";
                }
            }
        },
        error:function(error){
            layer.msg(error.status+'：未知错误');
        },
    });
});
