layui.config({
    base:'/js/'
}).use(['element','layer'], function() {
    var element = layui.element;
    var layer = layui.layer;

    $(function () {
        $.ajax({
            type:'post',
            url:geturl()+'student/getexamstatus',
            contentType:'application/json; charset=utf-8',
            dataType:'json',
            cache:false,
            // data: $("#updateproblemForm").toJson(),
            success:function(data){//返回json结果
                var problems=$("#problem");
                if(data.success==1){
                    if(isWeiXin()){
                        window.location.href=geturl()+"student/test";
                    }else if(isPC()){
                        var result ="<div class='tsd'>"
                            + "<button class='layui-btn layui-btn-fluid'>考试结果</button>"
                            + "<div class='tsdscore1'>您已经通过考试</div>"
                            + "<div class='psbtn'><div id='successoney' onclick='successoney();' class='layui-btn layui-btn-radius layui-btn-normal'>查看报到信息</div></div>"
                            +"</div>";
                        problems.empty();
                        problems.append(result);
                    }else {
                        window.location.href=geturl()+"student/test";
                    }
                }else{

                    layer.open({
                        title:'提示信息',
                        content:'您只有完成并且通过本次测验才能获得<p style="color:red;">查看个人信息并且完成新生报到</p> 的机会，'+'否则您将无法进行新生报到'+'<p style="color:green;">点击龙猫可查看部分答案提示</p>'
                        ,btn: ['确定']
                        ,yes: function(index, layero){

                            layer.close(index)

                        }
                        ,cancel: function(){
                            //右上角关闭回调
                            // window.location.href=geturl()+  'student/index';
                            //return false 开启该代码可禁止点击该按钮关闭
                        }
                    });
                }
            },
            error:function(error){
                layer.msg(error.status+'：未知错误');
            }
        });
    });
});

function  successoney() {
    window.location.href=geturl()+  'student/info';
}





