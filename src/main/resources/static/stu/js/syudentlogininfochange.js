
layui.config({
    base:'/js/'
}).use(['layer','form','element','laydate'], function() {
    var layer = layui.layer;
    var form = layui.form;
    var element = layui.element;
    var laydate = layui.laydate;

    //自定义验证规则
    form.verify({
        oldpassword: function(value){
            if(value==""){
                // layer.msg("验证码长度不正确");
                return '旧密码不能为空';
            }

        },
        newpassword: function(value){
            if(value==""){
                // layer.msg("验证码长度不正确");
                return '新密码不能为空';
            }else if(value.length<6){
                return '新密码至少为6位';
            } else {
                var password1=$("#newpassword").val();
                var password2=$("#newpassword1").val();
                if (password1!=password2){
                    return '两次输入新密码不同';
                }
            }
        }
    });

    form.on('submit(changeinfo)',function(data){
        $.ajax({
            type:'post',
            url:geturl()+'student/changestudentlogin',
            contentType:'application/json; charset=utf-8',
            dataType:'json',
            cache:false,
            data: $("#changeForm").toJson(),
            success:function(data){//返回json结果
                if(data.success==1){
                    layer.msg(data.msg);
                }else {
                    layer.msg(data.msg);
                }
            },
            error:function(error){
                layer.msg(error.status+'：未知错误');
            },
        });
        return false;
    });

});