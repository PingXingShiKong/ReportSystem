var isWeixin = isWeiXin();
var winHeight = typeof window.innerHeight != 'undefined' ? window.innerHeight : document.documentElement.clientHeight;
console.log(winHeight);
function loadHtml(){
    var div = document.createElement('div');
    div.id = 'weixin-tip';
    div.innerHTML = '<p><img src="/images/wechatanswer/wechatqr.jpg" alt="微信打开" style="margin-top:100px;max-width: 100%; height: auto; width:200px;height:200px;"/></p><p style="color:red; font-size:18px;">关注微信公众号获取答案</p>';
    document.body.appendChild(div);
}

function loadStyleText(cssText) {
    var style = document.createElement('style');
    style.rel = 'stylesheet';
    style.type = 'text/css';
    try {
        style.appendChild(document.createTextNode(cssText));
    } catch (e) {
        style.styleSheet.cssText = cssText; //ie9以下
    }
    var head=document.getElementsByTagName("head")[0]; //head标签之间加上style样式
    head.appendChild(style);
}
var cssText = "#weixin-tip{position: fixed; left:0; top:0; background: black; filter:alpha(opacity=80); width: 100%; height:100%; z-index: 9999;} #weixin-tip p{text-align: center; margin-top: 10%; padding:0 5%;}";
if(!isWeixin){
    loadHtml();
    loadStyleText(cssText);
}