
layui.config({
    base:'/js/'
}).use(['element','layer'], function() {
    var element = layui.element;
    var layer = layui.layer;

    layer.ready(function(){
        layer.open({
            title:'提示信息',
            content:'您只有完成并且通过本次测验(80分)才能获得<p style="color:red;">查看个人信息并且完成新生报到</p> 的机会，'+'否则您将无法进行新生报到'+'<p style="color:green;">关注威信公众号:</p> <p style="color:red;"> 山东农大南校学生圈</p> 可查看部分答案提示'
            ,btn: ['确定']
            ,yes: function(index, layero){

                layer.close(index)

            }
            ,cancel: function(){
                //右上角关闭回调
                // window.location.href=geturl()+  'student/index';
                //return false 开启该代码可禁止点击该按钮关闭
            }
        });
    });


    $(document).ready(function(){
        var loading=null;
        //加载层-默认风格
        layer.ready(function(){
            loading=layer.load();
        });
        $.post(geturl()+"student/getExam",{},function(data, status){

            var problems = data.data;

            $("#n").text(1);
            $("#ti").text(1);
            $("#length").text(problems.length);

            var problem1 = problems[0];
            var answers = {};
            var problrm1id = problem1.problemid;
            $("#content").html(problem1.problemtext);
            $("#A").html("A."+problem1.a);
            $("#B").html("B."+problem1.b);
            $("#C").html("C."+problem1.c);
            $("#D").html("D."+problem1.d);

            layer.close(loading);

            $(".weui-btn-area a").click(function () {
                //var answer = $("input[type='radio']:checked").parent().prev().text().trim();
                var answer = $("input[type='radio']:checked").parent().prev().children("p").attr("id");
                $("input[type='radio']:checked").attr("checked",false);
                if (answer == null){
                    alert("本题您还未选择");
                    return ;
                }

                answers[problrm1id]=answer;
                var strn = $("#n").text();
                var n = parseInt(strn);
                if (n == problems.length-1){
                    //  $("a").text("提交");

                }
                if (n < problems.length){
                    var problem = problems[n];
                    problrm1id = problem.problemid;
                    $("#content").html(problem.problemtext);
                    $("#A").html("A."+problem.a);
                    $("#B").html("B."+problem.b);
                    $("#C").html("C."+problem.c);
                    $("#D").html("D."+problem.d);
                    $("#n").text(n+1);
                    $("#ti").text(n+1);
                } else {
                    //$.post();
                    //   alert("提交");
                    var result = JSON.stringify(answers);

                    layer.ready(function(){
                        loading=layer.load();
                    });
                    $.ajax({
                        type:'post',
                        url:geturl()+'student/updateExam',
                        contentType:'application/json; charset=utf-8',
                        dataType:'json',
                        cache:false,
                        data:result,
                        success:function(data) {//返回json结果

                            if (data.success == 1){
                                var reult="<div class='weui-panel weui-panel_access'>"
                                    + "<div class=''>您的成绩</div>"
                                    + "<div class='weui-panel__bd'>"
                                    + "<div class='weui-media-box'>"
                                    +  "<h1 class='' style='font-size:70px;color:red;margin-top:70px;margin-bottom:50px;'>"+data.data+"分</h1>"
                                    +  "</div>"
                                    +  " </div>"
                                    +  "<h1 class='' style='font-size:20px;color:red;margin-top:20px;margin-bottom:20px;'>恭喜您已经通过此次考试，请查看个人信息</h1>"
                                    +  " </div>"
                                    +  " <div class='weui-btn-area'>"
                                    +  " <a class='weui-btn weui-btn_primary' href='javascript:' style='margin-top:50px;' id='succesones'>查看信息</a>"
                                    +  "</div>";
                                $("#question").empty();
                                $("#question").html(reult);
                                $("#succesones").click(function () {
                                    window.location.href=geturl()+"student/test";
                                });

                            }else {
                                var reult="<div class='weui-panel weui-panel_access'>"
                                    + "<div class=''>您的成绩</div>"
                                    + "<div class='weui-panel__bd'>"
                                    + "<div class='weui-media-box'>"
                                    +  "<h1 class='' style='font-size:70px;color:red;margin-top:70px;margin-bottom:50px;'>"+data.data+"分</h1>"
                                    +  "</div>"
                                    +  " </div>"
                                    +  "<h1 class='' style='font-size:20px;color:red;margin-top:20px;margin-bottom:20px;'>您未通过此次考试，请再来一次</h1>"
                                    +  " </div>"
                                    +  " <div class='weui-btn-area'>"
                                    +  " <a class='weui-btn weui-btn_primary' href='javascript:' style='margin-top:50px;' id='agains'>重新考试</a>"
                                    +  "</div>";
                                $("#question").empty();
                                $("#question").html(reult);
                                $("#agains").click(function () {
                                    window.location.href=geturl()+"student/questions";
                                });

                            }
                        }
                    }); //提交数据
                    setTimeout(function(){
                        layer.close(loading);
                    }, 2000);

                }


            });//a click

        });//获取数据
        setTimeout(function(){
            layer.close(loading);
        }, 2000);

    }); //加载html



})




