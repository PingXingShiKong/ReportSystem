layui.config({
    base : "/js/"
}).use(['form','layer','jquery','laypage'],function(){
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage,
        $ = layui.jquery;

    //加载页面数据
    var newsData = '';
    var url=geturl()+"colandsch/baodaolv?"+"time="+new Date();
    $.get(url, function(data){
        var newArray = [];
        newsData = data;
        if(window.sessionStorage.getItem("addNews")){
            var addNews = window.sessionStorage.getItem("addNews");
            newsData = JSON.parse(addNews).concat(newsData);
        }
        //执行加载数据的方法
        newsList();
    })

    $("body").on("click",".news_edit",function(){
        var id = $(this).parents("tr").children("td:nth-child(2)").text();
        var url="/colandsch/bjbaodaolvs?id="+id+"&time="+new Date();
        var index = layui.layer.open({
            title : "专业报到率",
            type : 2,
            content:url,
            success : function(layero, index){
                setTimeout(function(){
                    layui.layer.tips('点击此处返查看其他专业报到率', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
        //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
        $(window).resize(function(){
            layui.layer.full(index);
        })
        layui.layer.full(index);
    })


    function newsList(that){
        //渲染数据
        function renderDate(data,curr){
            var dataHtml = '';
            if(!that){
                currData = newsData.concat().splice(curr*nums-nums, nums);
            }else{
                currData = that.concat().splice(curr*nums-nums, nums);
            }
            if(currData.length != 0){
                var sum=0;
                var hdcount=0;
                var bdcount=0;
                for(var i=0;i<currData.length;i++){
                    if (isWeiXin()) {
                        dataHtml +=    '<a class="weui-cell weui-cell_access" href="javascript:;" title= '+currData[i].majorname +'>' +
                            '<div class="weui-cell__bd">'+
                            '<p>' + currData[i].majorname +'</p>'+
                            '</div>'+
                            '<div class="weui-cell__ft">' + currData[i].register +'</div>' +
                            '</a>';
                        sum+=currData[i].register;
                        hdcount+=currData[i].headcount;
                        bdcount+=currData[i].bdcount;
                    }else {
                        dataHtml += '<tr>'
                            + '<td >' + currData[i].majorid + '</td>'
                            + '<td align="left">' + currData[i].majorname + '</td>'
                        ;
                        dataHtml += '<td>' + currData[i].headcount + '</td>';
                        dataHtml += '<td>' + currData[i].bdcount + '</td>'
                            + '<td>' + currData[i].register + "%" + '</td>'
                            + '<td>'
                            //+ '<a class="layui-btn layui-btn-mini news_edit" href="/xueyuan/bjbaodaolv.html?id='+currData[i].majorname+'" ><i class="iconfont icon-edit"></i> 查看</a>'
                            + '<a class="layui-btn layui-btn-mini news_edit"><i class="iconfont icon-edit"></i> 查看</a>'
                            + '</td>'
                            + '</tr>';
                        sum += currData[i].register;
                    }
                }
                if (isWeiXin()){
                    var rate = bdcount/hdcount;
                    rate = rate.toFixed(5)*100 + '%';
                    $("#sum").append(rate);
                    $("#bdcount").text(bdcount);
                    $("#hdcount").text(hdcount);
                }
                $("#all").text(sum+"%");
            }else{
                if (isWeiXin()){
                    dataHtml = '<div class="weui-loadmore weui-loadmore_line">' +
                        '<span class="weui-loadmore__tips">暂无数据</span>' +
                        '</div>';
                } else {
                    dataHtml = '<tr><td colspan="8">暂无数据</td></tr>';
                }
            }
            return dataHtml;
        }

        //分页
        var nums = 13; //每页出现的数据量
        if(that){
            newsData = that;
        }
        laypage({
            cont : "page",
            pages : Math.ceil(newsData.length/nums),
            jump : function(obj){
                $(".news_content").html(renderDate(newsData,obj.curr));
                $('.news_list thead input[type="checkbox"]').prop("checked",false);
                form.render();
                $("a.weui-cell_access").click(function(){
                    var id = $(this).attr("title");
                    var url = geturl() + "weixin/classRate?id="+id+"&time="+new Date();
                    window.location.href = url;
                });
            }
        })
    }
})