layui.config({
    base : "/js/"
}).use(['form','layer','jquery','laypage'],function(){
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage,
        $ = layui.jquery;


    $("body").on("click",".news_select",function(){
        var s= $("#sno").val();
        var url="/xueyuan/unselects?sno="+s+"&time="+new Date();
        var index = layui.layer.open({
            title : "查询结果",
            type : 2,
            content:url,
            success : function(layero, index){
                setTimeout(function(){
                    layui.layer.tips('点击此处返回', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
        //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
        $(window).resize(function(){
            layui.layer.full(index);
        })
        layui.layer.full(index);
    })

    //加载页面数据
    var newsData = '';
    var url=geturl()+"colandsch/unreported"+"&time="+new Date();
    $.get(url, function(data){
        var newArray = [];
        newsData = data;
        if(window.sessionStorage.getItem("addNews")){
            var addNews = window.sessionStorage.getItem("addNews");
            newsData = JSON.parse(addNews).concat(newsData);
        }
        //执行加载数据的方法
        newsList();
    })

    $("body").on("click",".news_edit",function(){
        var id = $(this).parents("tr").children("td:nth-child(2)").text();
        layer.prompt({
            formType: 2,
            title: '未报到原因',
            area: ['800px', '350px'] //自定义文本域宽高
        }, function(value, index, elem){
            var url="/xueyuan/addreason?id="+id+"&context="+value+"&time="+new Date();
            window.location.href=url;
            layer.close(index);
        });
    })

    function newsList(that){
        //渲染数据
        function renderDate(data,curr){
            var dataHtml = '';
            if(!that){
                currData = newsData.concat().splice(curr*nums-nums, nums);
            }else{
                currData = that.concat().splice(curr*nums-nums, nums);
            }
            if(currData.length != 0){
                for(var i=0;i<currData.length;i++){
                    if (isWeiXin()){

                        dataHtml +=   '<tr>'
                            +'<td>' + currData[i].sno + '</td>'
                            +'<td>' + currData[i].name + '</td>'
                            +'<td>' + currData[i].reason + '</td>'
                            +'</tr>';
                    }else {
                        dataHtml += '<tr>'
                            + '<td >' + (i + 1) + '</td>'
                            + '<td>' + currData[i].sno + '</td>'
                        ;
                        dataHtml += '<td>' + currData[i].reason + '</td>';
                        dataHtml += '<td>'
                            + '<a class="layui-btn layui-btn-mini news_edit"><i class="iconfont icon-edit"></i> 备注未报到原因</a>'
                            + '</td>'
                            + '</tr>';
                    }
                }
            }else{
                if (isWeiXin()){
                    dataHtml = '<div class="weui-loadmore weui-loadmore_line">' +
                        '<span class="weui-loadmore__tips">暂无数据</span>' +
                        '</div>';
                } else {
                    dataHtml = '<tr><td colspan="8">暂无数据</td></tr>';
                }
            }
            return dataHtml;
        }


        //分页
        var nums =3; //每页出现的数据量
        if(that){
            newsData = that;
        }
        laypage({
            cont : "page",
            pages : Math.ceil(newsData.length/nums),
            jump : function(obj){
                $(".news_content").html(renderDate(newsData,obj.curr));
                $('.news_list thead input[type="checkbox"]').prop("checked",false);
                form.render();
            }
        })
    }
})