layui.config({
    base : "/js/"
}).use(['form','layer','jquery','laypage'],function(){
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage,
        $ = layui.jquery;

    //加载页面数据
    var newsData = '';
    var sno=$("#sno").val();

    if(sno!="") {
        var  url="/xueyuan/unselect?sno="+sno+"&time="+new Date();
    }
    else {
        var  url = location.search;
        var theRequest = new Object();
        if (url.indexOf("?") != -1) {
            var str = url.substr(1);
            strs = str.split("&");
        }
        url = "/xueyuan/unselect?" + strs[0]+"&time="+new Date();
    }
    $.get(url, function(data){
        var newArray = [];
        newsData = data;
        if(window.sessionStorage.getItem("addNews")){
            var addNews = window.sessionStorage.getItem("addNews");
            newsData = JSON.parse(addNews).concat(newsData);
        }
        //执行加载数据的方法
        newsList();
    })

    $("body").on("click",".news_edit",function(){
        var id = $(this).parents("tr").children("td:nth-child(2)").text();
        layer.prompt({
            formType: 2,
            title: '未报到原因',
            area: ['800px', '350px'] //自定义文本域宽高
        }, function(value, index, elem){
            var url="/xueyuan/addreason?id="+id+"&context="+value+"&f=1"+"&time="+new Date();
            window.location.href=url;
            layer.close(index);
        });
    })

    function newsList(that){
        //渲染数据
        function renderDate(data,curr){
            var dataHtml = '';
            if(!that){
                currData = newsData.concat().splice(curr*nums-nums, nums);
            }else{
                currData = that.concat().splice(curr*nums-nums, nums);
            }
            if(currData.length != 0){
                for(var i=0;i<currData.length;i++){
                    dataHtml += '<tr>'
                        +'<td >'+(i+1)+'</td>'
                        +'<td>'+currData[i].sno+'</td>'
                    ;
                    dataHtml += '<td>'+currData[i].reason+'</td>';
                    dataHtml += '<td>'
                        +'<a class="layui-btn layui-btn-mini news_edit"><i class="iconfont icon-edit"></i> 备注未报到原因</a>'
                        +'</td>'
                        +'</tr>';
                }
            }else{
                dataHtml = '<tr><td colspan="8">暂无数据</td></tr>';
            }
            return dataHtml;
        }


        //分页
        var nums =2; //每页出现的数据量
        if(that){
            newsData = that;
        }
        laypage({
            cont : "page",
            pages : Math.ceil(newsData.length/nums),
            jump : function(obj){
                $(".news_content").html(renderDate(newsData,obj.curr));
                $('.news_list thead input[type="checkbox"]').prop("checked",false);
                form.render();
            }
        })
    }
})